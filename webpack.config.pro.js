var path = require('path');
var webpack = require('webpack');

// console.log('process', process);

module.exports = {
  devtool: 'source-map',
  entry: './scripts/index',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ],
  resolve: {
    extensions: [ '', '.js', '.scss', '.css' ],
    alias: {
        config: path.join(__dirname, 'config', 'pro')
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['react-hot', 'babel-loader'],
        include: [path.join(__dirname, 'scripts'), path.join(__dirname, 'node_modules/react-router/modules')],
      },
      {
        test: /\.(scss|sass)$/,
        loaders: ["style", "css", "sass", 'autoprefixer'],
        include: [path.join(__dirname, 'styles')]
      },
      {
        test: /\.css?$/,
        loaders: ["style", "css", ],
        include: [path.join(__dirname, 'styles')]
      },
      { test: /\.png$/, loader: "url-loader?limit=100000" },
      { test: /\.jpg$/, loader: "file-loader" },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
    ]
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, "./node_modules/compass-mixins/lib")]
  }
};
