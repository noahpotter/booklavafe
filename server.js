var localServer = require('./server.local');
var devServer = require('./server.dev');
var proServer = require('./server.pro');

switch(process.env.NODE_ENV) {
  case 'local':
    localServer();
    break;
  case 'dev':
    devServer();
    break;
  case 'pro':
    proServer();
    break;
}