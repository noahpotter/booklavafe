var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: 'mocha!./tests/index.js',
  output: {
    filename: 'test.build.js',
    path: 'tests/',
    publicPath: 'http://' + 'localhost' + ':' + 3002 + '/tests'
  },
  resolve: {
    extensions: [ '', '.js', '.scss', '.css' ],
    alias: {
      config: path.join(__dirname, 'config', 'dev')
    }
  },
  module: {
    loaders: [
      {
        test: /\.jsx|\.js$/,
        exclude: /node_modules/,
        loaders: ['babel']
      },
      {
        test: /(\.css|\.scss|\.less)$/,
        loader: 'null-loader',
        exclude: [
            /build/
        ]
      },
      {
        test: /(\.jpg|\.jpeg|\.png|\.gif)$/,
        loader: 'null-loader'
      }
    ]
  },
  devServer: {
    host: 'localhost',
    port: 3002
  },
  node: {
    fs: "empty"
  }
};