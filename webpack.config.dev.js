var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');

// console.log('process', process);

module.exports = {
  devtool: 'source-map',
  entry: './scripts/index',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ],
  resolve: {
    extensions: [ '', '.js', '.scss', '.css' ],
    alias: {
        config: path.join(__dirname, 'config', 'dev')
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel'],
        include: [path.join(__dirname, 'scripts'), path.join(__dirname, 'node_modules/react-router/modules')],
      },
      {
        test: /\.css?$/,
        loaders: ['style', 'css', 'postcss'],
        include: [path.join(__dirname, 'styles')]
      },
      { test: /\.png$/, loader: 'url-loader?limit=100000' },
      { test: /\.jpg$/, loader: 'file-loader' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&minetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' },
      {
        test: /\.md$/,
        loader: 'raw'
      },
    ]
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/compass-mixins/lib')]
  },
  // postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
  postcss: function () {
    return {
      plugins: [require('postcss-import')({path: './styles/modules'}), require('precss')(
        {
          mixins: {
            mixinsDir: path.join(__dirname, 'styles', 'mixins'),
            mixinsFiles: [path.join(__dirname, 'styles', 'font-awesome', '_mixins.css')]
          }
        }), 
        autoprefixer, require('postcss-font-awesome')],
      syntax: require('postcss-scss')
    };
  }
};
