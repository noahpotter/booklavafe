var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');

// console.log('process', process.env.NODE_ENV);

module.exports = {
  entry: [
    // 'webpack-dev-server/client?http://0.0.0.0:5000',
    'react-hot-loader/patch',
    'webpack-hot-middleware/client?http://localhost:5000/',
    'webpack/hot/only-dev-server',
    './scripts/index'
  ],
  devtool: 'eval-source-map',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    extensions: [ '', '.js', '.scss', '.css' ],
    alias: {
        config: path.join(__dirname, 'config', 'local')
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel'],
        include: [path.join(__dirname, 'scripts'), path.join(__dirname, 'node_modules/react-router/modules')],
      },
      {
        test: /\.css?$/,
        loaders: ['style', 'css', 'postcss'],
        include: [path.join(__dirname, 'styles')]
      },
      { test: /\.png$/, loader: 'url-loader?limit=100000' },
      { test: /\.jpg$/, loader: 'file-loader' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&minetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' },
      {
        test: /\.md$/,
        loader: 'raw'
      },
    ]
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/compass-mixins/lib')]
  },
  postcss: function (webpack) {
    return {
      plugins: [
        require('postcss-import')(
          {
            path: ['./styles/extends', './styles/modules', './styles/mixins'],
            addDependencyTo: webpack
          }
        ), 
        require('postcss-pseudoelements'), 
        require('postcss-mixins')(
          {
            mixinsDir: path.join(__dirname, 'styles', 'mixins'),
            mixinsFiles: [path.join(__dirname, 'styles', 'font-awesome', '_mixins.css')]
          }
        ), 
        require('postcss-simple-vars')( 
          {
            // variables() {
            //   const file = './styles/modules/variables.js';

            //   delete require.cache[path.join(__dirname, file)];

            //   return require(file);
            // },
            silent: true
          }
        ), 
        require('postcss-color-function'), 
        require('postcss-nesting'), 
        require('postcss-extend'),
        require('postcss-nested'), 
        autoprefixer, 
        require('postcss-font-awesome')],
      syntax: require('postcss-scss')
    };
  }
};
