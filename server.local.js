var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.local');

function run() {
  var app = express();
  var compiler = webpack(config);
  
  app.set('port', (process.env.PORT || 3000));

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    hot: true,
    publicPath: config.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000,
  }));

  app.use(express.static(__dirname + '/static'));

  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
  });

  app.listen(app.get('port'), 'localhost', function(err) {
    if (err) {
      console.log(err);
      return;
    }
  });
}


module.exports = run;