var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.pro');

function run() {
  var app = express();
  var compiler = webpack(config);

  app.set('port', (process.env.PORT || 3000));
  
  app.use(express.static(__dirname + '/dist'));

  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'dist', 'index.html'));
  });

  app.listen(app.get('port'), function(err) {
    if (err) {
      console.log(err);
      return;
    }
  });
}


module.exports = run;