import expect from 'expect'
import {reducers} from '../../scripts/reducers'
import * as actions from '../../scripts/actions'
import Immutable from 'immutable'
import localStorage from 'localStorage'
import store from 'store'

describe('loggedUserDetails reducers', () => {
  describe('initial state', () => {
    it('logged out user', () => {
      store.remove('userId')
      expect(
        reducers.loggedUserDetails(undefined, {}).toJS()
      ).toEqual(
        Immutable.fromJS({
          id: null,
          apiKey: '',
        }).toJS()
      )
    })

    it('logged in user', () => {
      store.set('userId', 0)
      store.set('apiKey', '123')
      expect(
        reducers.loggedUserDetails(undefined, {}).toJS()
      ).toEqual(
        Immutable.fromJS({
          id: 0,
          apiKey: '123',
        }).toJS()
      )
    })

    it('"logs out" the user if the apiKey is missing', () => {
      store.set('userId', 0)
      store.set('apiKey', '')
      expect(
        reducers.loggedUserDetails(undefined, {}).toJS()
      ).toEqual(
        Immutable.fromJS({
          id: null,
          apiKey: '',
        }).toJS()
      )
    })
  })

  it('should handle LOGIN_SUCCESS', () => {
    expect(
      reducers.loggedUserDetails(undefined, {
        type: actions.LOGIN_SUCCESS,
        response: {token: '123', id: 0}
      }).toJS()
    ).toEqual(
      Immutable.fromJS({
        id: 0,
        apiKey: '123'
      }).toJS()
    )
  })

  it ('should put the id and apiKey in localstorage for future use', () => {
    reducers.loggedUserDetails(undefined, {
      type: actions.LOGIN_SUCCESS,
      response: {token: '123', id: 0}
    }).toJS()

    expect(
      store.get('userId')
    ).toEqual(0)

    expect(
      store.get('apiKey')
    ).toEqual('123')
  })

  it('should handle LOGOUT_SUCCESS', () => {
    expect(
      reducers.loggedUserDetails(undefined, {
        type: actions.LOGOUT_SUCCESS
      }).toJS()
    ).toEqual(
      Immutable.fromJS({
        id: null,
        apiKey: ''
      }).toJS()
    )
  })

  it('should remove the id from localstorage', () => {
    store.set('userId', 0)

    reducers.loggedUserDetails(undefined, {
      type: actions.LOGOUT_SUCCESS
    }).toJS()

    expect(
      store.get('userId')
    ).toEqual(null)
  })
})