import expect from 'expect'
import {reducers} from '../../scripts/reducers'
import * as actions from '../../scripts/actions'
import Immutable from 'immutable'
import localStorage from 'localStorage'
import store from 'store'

describe('searchDetails reducers', () => {
  describe('initial state', () => {
    it('', () => {
      expect(
        reducers.searchDetails(undefined, {}).toJS()
      ).toEqual(
        Immutable.fromJS({
          query: '',
          type: '',
          selectedId: null,
          books: [],
          clubs: [],
          users: []
        }).toJS()
      )
    })
  })

  describe('searching clubs', () => {
    it('should handle SEARCH_SUCCESS', () => {
      expect(
        reducers.searchDetails(undefined, {
          type: actions.SEARCH_SUCCESS,
          response: {entities: {clubs: {0: {id: 0}}}},
          previousState: { searchDetails: Immutable.fromJS({type: 'clubs'}) }
        }).toJS()
      ).toEqual(
        Immutable.fromJS({
          query: '',
          type: '',
          selectedId: null,
          books: [],
          clubs: [0],
          users: []
        }).toJS()
      )
    })
  });
})