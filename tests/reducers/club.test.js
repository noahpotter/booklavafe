import expect from 'expect'
import {reducers} from '../../scripts/reducers'
import * as actions from '../../scripts/actions'
import Immutable from 'immutable'
import localStorage from 'localStorage'
import store from 'store'

describe('club reducers', () => {
  it('adds to ownedClubs on CREATE_CLUB_SUCCESS', () => {
    expect(
      reducers.entities(Immutable.fromJS({users: {0: {id: 0, ownedClubs: []}}}), {
        type: actions.CREATE_CLUB_SUCCESS,
        previousState: {loggedUserDetails: Immutable.fromJS({id: 0})},
        response: {entities: {clubs: {0: {id: 0}}}}
      }).toJS()
    ).toEqual(
      Immutable.fromJS({
        users: {
          0: {
            id: 0,
            ownedClubs: [0]
          }
        },
        clubs: {
          0: {
            id: 0
          }
        }
      }).toJS()
    )
  })

  it('adds to subscribedClubs on JOIN_CLUB_SUCCESS', () => {
    expect(
      reducers.entities(Immutable.fromJS({users: {0: {id: 0, subscribedClubs: []}}}), {
        type: actions.JOIN_CLUB_SUCCESS,
        previousState: {loggedUserDetails: Immutable.fromJS({id: 0})},
        response: {entities: {clubs: {0: {id: 0}}}}
      }).toJS()
    ).toEqual(
      Immutable.fromJS({
        users: {
          0: {
            id: 0,
            subscribedClubs: [0]
          }
        },
        clubs: {
          0: {
            id: 0
          }
        }
      }).toJS()
    )
  })

  // it('adds to subscribedClubs on JOIN_CLUB_SUCCESS', () => {
  //   expect(
  //     reducers.loggedUserDetails(undefined, {
  //       type: actions.LOGIN_SUCCESS,
  //       response: {token: '123', id: 0}
  //     }).toJS()
  //   ).toEqual(
  //     Immutable.fromJS({
  //       id: 0,
  //       apiKey: '123'
  //     }).toJS()
  //   )
  // })
})