import expect from 'expect'
import * as actions from '../../scripts/actions'
import authorize from '../../scripts/middleware/authorize'
import Immutable from 'immutable'
import { pushState, HISTORY_API } from 'redux-router'

const createFakeStore = fakeData => ({
  lastAction: null,
  getState() {
    return fakeData
  },
  dispatch(action) {
    lastAction = action;
    return action
  }
})

const dispatchWithStoreOf = (storeData, action) => {
  let dispatched = null
  const store = createFakeStore(storeData)
  const dispatch = authorize(store)(actionAttempt => dispatched = actionAttempt)
  dispatch(action)
  return dispatched
};

describe('authorize middleware', () => {
  describe('logged in user', () => {
    const store = {
      loggedUserDetails: Immutable.fromJS({
        id: '1',
        apiKey: '123'
      })
    }

    describe('allowed route', () => {

      const action = pushState(null, '/users', null);

      it('doesnt change the route', () => {
        expect(
          dispatchWithStoreOf(store, action).payload.args[1]
        ).toEqual('/users')
      })
    })

    describe('unauthorized routes', () => {

      const action = pushState(null, '/login', null);

      it('changes the route', () => {
        expect(
          dispatchWithStoreOf(store, action).payload.args[1]
        ).toEqual('/')
      })
    })
  })

  describe('guest user', () => {

    const store = {
      loggedUserDetails: Immutable.fromJS({
        id: '',
        apiKey: ''
      })
    }

    describe('allowed route', () => {

      const action = pushState(null, '/login', null);

      it('doesnt change the route', () => {
        expect(
          dispatchWithStoreOf(store, action).payload.args[1]
        ).toEqual('/login')
      })
    })

    describe('unauthorized routes', () => {

      const action = pushState(null, '/users', null);

      it('changes the route', () => {
        expect(
          dispatchWithStoreOf(store, action).payload.args[1]
        ).toEqual('/login')
      })
    })
  })
})