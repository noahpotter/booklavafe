import expect from 'expect'
import * as actions from '../../scripts/actions'

describe('actions', () => {
  it('should create an action to start a reply', () => {
    const postId = '0';
    const expectedAction = {
      type: actions.START_REPLY,
      postId
    }
    expect(actions.startReply(postId)).toEqual(expectedAction)
  })
})