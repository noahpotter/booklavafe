import expect from 'expect'
import { ROUTER_DID_CHANGE, HISTORY_API } from 'redux-router/lib/constants';
import * as actions from '../../scripts/actions'
import 'isomorphic-fetch';
import fetchMock from 'fetch-mock'
import { setFetch } from '../../scripts/middleware/api'
import Immutable from 'immutable'
import { mockStore, matchUrl } from './utils'

describe('club async actions', () => {

  beforeEach(() => {
    fetchMock.unregisterRoute();
    fetchMock.restore();
  })

  it('creates a club', (done) => {
    var club = { id: 0, name: 'test' }
    var clubDetails = { name: 'test' }
    fetchMock.registerRoute([
      {
        name: 'createClub',
        matcher: matchUrl.bind(this, 'http://localhost:8000/clubs/', clubDetails),
        method: 'post', 
        response: {body: club}
      }
    ]);
    fetchMock.mock();
    setFetch(fetch);

    // Flags:
    // typeOnly: only checks type
    const expectedActions = [
      { type: actions.CREATE_CLUB },
      { type: actions.CREATE_CLUB_SUCCESS, response: { rawData: club, entities: { clubs: { 0: club}}, result: 0} },
    ]
    const store = mockStore({ loggedUserDetails: Immutable.fromJS({id: '0', apiKey: '123' }), clubDetails: Immutable.fromJS({creatingClub: clubDetails})}, expectedActions, done)
    store.dispatch(actions.createClub())
  })

  // it('joins a club', (done) => {
  //   var club = { id: 0, name: 'test' }
  //   var clubDetails = { name: 'test' }
  //   fetchMock.registerRoute([
  //     {
  //       name: 'joinClub',
  //       matcher: matchUrl.bind(this, 'http://localhost:8000/users/0/clubs/0/', clubDetails),
  //       method: 'post', 
  //       response: {body: club}
  //     }
  //   ]);
  //   fetchMock.mock();
  //   setFetch(fetch);

  //   const expectedActions = [
  //     { type: actions.JOIN_CLUB },
  //     { type: actions.JOIN_CLUB_SUCCESS, response: { rawData: club, entities: { clubs: { 0: club}}, result: 0} },
  //   ]
  //   const store = mockStore({ entities: Immutable.fromJS({users: {0: {id: 0, subscribedClubs: []}}}), loggedUserDetails: Immutable.fromJS({id: '0', apiKey: '123' })})}, expectedActions, done)
  //   store.dispatch(actions.createClub())
  // })
})