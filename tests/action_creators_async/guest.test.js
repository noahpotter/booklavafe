import expect from 'expect'
import { ROUTER_DID_CHANGE, HISTORY_API } from 'redux-router/lib/constants';
import * as actions from '../../scripts/actions'
import 'isomorphic-fetch';
import fetchMock from 'fetch-mock'
import { setFetch } from '../../scripts/middleware/api'
import Immutable from 'immutable'
import { mockStore, matchUrl } from './utils'

describe('guest async actions', () => {

  beforeEach(() => {
    fetchMock.unregisterRoute();
    fetchMock.restore();
  })

  it('creates and logins in a user', (done) => {
    var user = { id: 0, username: 'test' }
    var userDetails = { email: 'test@test.com', username: 'test', password: 'test', password_confirmation: 'test' }
    fetchMock.registerRoute([
      {
        name: 'createUser',
        matcher: matchUrl.bind(this, 'http://localhost:8000/users/', userDetails),
        method: 'post', 
        response: {body: user}
      },
      {
        name: 'login',
        matcher: matchUrl.bind(this, 'http://localhost:8000/api-token-auth/', {email: userDetails.email, password: userDetails.password}),
        method: 'post', 
        response: {body: {token: '123', id: user.id}}
      },
      {
        name: 'fetchUser',
        matcher: matchUrl.bind(this, 'http://localhost:8000/users/0/', null),
        method: 'get', 
        response: {body: user}
      }
    ]);
    fetchMock.mock();
    setFetch(fetch);

    // Flags:
    // typeOnly: only checks type
    const expectedActions = [
      { type: actions.CREATE_USER },
      { type: actions.CREATE_USER_SUCCESS, response: { rawData: {id: 0, username: 'test'}, entities: { users: { 0: { id: 0, username: 'test'}}}, result: 0} },
      { type: actions.LOGIN },
      { type: actions.LOGIN_SUCCESS, response: {token: '123', id: '0'} },
      { type: actions.FETCH_USER, typeOnly: true },
      { type: actions.FETCH_USER_SUCCESS, typeOnly: true },
      { type: HISTORY_API, typeOnly: true },
    ]
    const store = mockStore({ loggedUserDetails: Immutable.fromJS({id: user.id, apiKey: '123' })}, expectedActions, done)
    store.dispatch(actions.createUser(userDetails))
  })

  it('logs in a user', (done) => {
    var user = { id: 0, username: 'test', email: 'test@test.com' }
    var loginDetails = { email: 'test@test.com', password: 'test' }
    fetchMock.registerRoute([
      {
        name: 'login',
        matcher: matchUrl.bind(this, 'http://localhost:8000/api-token-auth/', {email: loginDetails.email, password: loginDetails.password}),
        method: 'post', 
        response: {body: {token: '123', id: user.id}}
      },
      {
        name: 'fetchUser',
        matcher: matchUrl.bind(this, 'http://localhost:8000/users/0/', null),
        method: 'get', 
        response: {body: user}
      }
    ]);
    fetchMock.mock();
    setFetch(fetch);

    const expectedActions = [
      { type: actions.LOGIN },
      { type: actions.LOGIN_SUCCESS, response: {token: '123', id: '0'} },
      { type: actions.FETCH_USER },
      { type: actions.FETCH_USER_SUCCESS, response: { rawData: user, entities: { users: { 0: user}}, result: 0} },
      { type: HISTORY_API, typeOnly: true },
    ]
    const store = mockStore({ loggedUserDetails: Immutable.fromJS({id: user.id, apiKey: '123' })}, expectedActions, done)
    store.dispatch(actions.login(loginDetails))
  })
  
  describe('when page loads', () => {
    it('fetches a logged in user', (done) => {
      var user = { id: 0, username: 'test', email: 'test@test.com' }
      var loginDetails = { email: 'test@test.com', password: 'test' }
      fetchMock.registerRoute([
        {
          name: 'fetchUser',
          matcher: matchUrl.bind(this, 'http://localhost:8000/users/0/', null),
          method: 'get',
          response: {body: user}
        }
      ]);
      fetchMock.mock();
      setFetch(fetch);

      const expectedActions = [
        { type: actions.INITIAL_STARTUP },
        { type: actions.FETCH_USER },
        { type: actions.FETCH_USER_SUCCESS, response: { rawData: user, entities: { users: { 0: user}}, result: 0} }
      ]
      const store = mockStore({ loggedUserDetails: Immutable.fromJS({id: user.id, apiKey: '123' })}, expectedActions, done)
      store.dispatch(actions.loadPage())
    })
  })

  it('logs out a user', (done) => {
    fetchMock.registerRoute([
      {
        name: 'logout',
        matcher: matchUrl.bind(this, 'http://localhost:8000/api-token-auth/', {id: '0'}),
        method: 'delete', 
        response: {}
      }
    ]);
    fetchMock.mock();
    setFetch(fetch);

    const expectedActions = [
      { type: actions.LOGOUT },
      { type: actions.LOGOUT_SUCCESS, typeOnly: true },
      { type: HISTORY_API, typeOnly: true }
    ]
    const store = mockStore({ loggedUserDetails: Immutable.fromJS({id: '0'}) }, expectedActions, done)
    store.dispatch(actions.logout())
  })
})