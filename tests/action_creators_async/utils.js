import expect from 'expect'
import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import api, { CALL_API } from '../../scripts/middleware/api'
import Immutable from 'immutable'

const middlewares = [ thunk, api ]

/**
 * Creates a mock of Redux store with middleware.
 */
export function mockStore(getState, expectedActions, done) {
  if (!Array.isArray(expectedActions)) {
    throw new Error('expectedActions should be an array of expected actions.')
  }
  if (typeof done !== 'undefined' && typeof done !== 'function') {
    throw new Error('done should either be undefined or function.')
  }

  function mockStoreWithoutMiddleware() {
    return {
      getState() {
        return typeof getState === 'function' ?
          getState() :
          getState
      },

      dispatch(action) {
        console.log('processing action ' + action.type);
        if (action[CALL_API]) {
          if (done && !expectedActions.length) {
            done()
          }
          return action
        } else {
          const expectedAction = expectedActions.shift()

          try {
            if (expectedAction.typeOnly) {
              expect(action.type).toEqual(expectedAction.type);
            } else {
              expect(Immutable.fromJS(action).remove('previousState').toJS()).toEqual(expectedAction);
            }

            if (done && !expectedActions.length) {
              done()
            }
            return action
          } catch (e) {
            done(e)
          }
        }
      }
    }
  }

  const mockStoreWithMiddleware = applyMiddleware(
    ...middlewares
  )(mockStoreWithoutMiddleware)

  return mockStoreWithMiddleware()
}

export function matchUrl(targetUrl, targetData, url, request) {
  var requestData;
  if (request.body) {
    requestData = JSON.parse(request.body);
  }

  if (url == targetUrl) {
    if (requestData) {
      if (Immutable.fromJS(requestData).isSuperset(Immutable.fromJS(targetData))) {
        return true;
      } else {
        console.log(`Matched ${url}, but target options were different.`, requestData, targetData)
      }
    } else if (targetData) {
      console.log('Got target data but no request data.', targetData)
    } else {
      // No target and request data so we assume this was purposeful
      return true;
    }
  }

  return false;
}