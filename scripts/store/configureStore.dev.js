import { createStore, applyMiddleware, compose } from 'redux';
import { reduxReactRouter } from 'redux-router';
import { devTools } from 'redux-devtools';
import { createHistory } from 'history';
import routes from '../routes';
import thunk from 'redux-thunk';
import api from '../middleware/api';
import authorize from '../middleware/authorize';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';
import * as actions from '../actions';

const finalCreateStore = compose(
  applyMiddleware(thunk, authorize, api),
  reduxReactRouter({ routes, createHistory }),
  // applyMiddleware(createLogger()),
  // devTools()
)(createStore);

export default function configureStore(initialState) {
  const store = finalCreateStore(rootReducer, initialState);
  store.dispatch(actions.loadPage())

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      // const nextRootReducer = require('../reducers');
      // store.replaceReducer(nextRootReducer);
      store.replaceReducer(require('../reducers').default)
    });
  }

  return store;
}

