import React from 'react';
import { Route, IndexRoute } from 'react-router'

import App from './App';
import Base from './Base';
import GuestPage from './pages/GuestPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage';
import ResetPasswordPage from './pages/ResetPasswordPage';
import DefaultPage from './pages/DefaultPage';
import SearchPage from './pages/SearchPage';
import ClubPage from './pages/ClubPage';
import MyClubsPage from './pages/MyClubsPage';
import ClubBookDiscussionPage from './pages/ClubBookDiscussionPage';
import ClubDiscussionPage from './pages/ClubDiscussionPage';
import ProfilePage from './pages/ProfilePage';
import AboutPage from './pages/AboutPage';

export default (
  <Route name='root' path='/' component={Base}>
    <IndexRoute component={DefaultPage} />
    <Route component={GuestPage}>
      <Route name='login' path='/login' component={LoginPage} />
      <Route name='register' path='/register' component={RegisterPage} />
      <Route name='forgot_password' path='/passwords/forgot' component={ForgotPasswordPage} />
      <Route name='reset_password' path='/passwords/reset' component={ResetPasswordPage} />
    </Route>
    <Route name='app' component={App}>
      <Route name='discuss_club_book' path='/club_book_discussions/:id' component={ClubBookDiscussionPage} />
      <Route name='discuss_club' path='/club_discussions/:id' component={ClubDiscussionPage} />
      <Route name='search' path='/search' component={SearchPage} />
      <Route name='club' path='/clubs/:id' component={ClubPage} />
      <Route name='my_clubs' path='/users/:id/clubs' component={MyClubsPage} />
      <Route name='profile' path='/users/:id' component={ProfilePage} />
      <Route name='about' path='/about' component={AboutPage} />
    </Route>
  </Route>
);
