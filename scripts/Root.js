import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router'
import { store, history } from './store/store';
import routes from './routes';

export default function Root() {
  return (
    <Provider store={store}>
      <Router history={history} children={routes} />
    </Provider>
  )
}