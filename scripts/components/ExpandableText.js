import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/dedupe';

// https://github.com/littlebits/react-popover


export default class ExpandableText extends Component {
  static propTypes = {
    fontSize: PropTypes.number,
    height: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.toggleOpen = this.toggleOpen.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.setClasses = this.setClasses.bind(this);

    this.contentVisible = false;
    this.contentIsSmaller = false;

    this.state = {
      open: false
    }
  }

  handleResize(e) {
    this.forceUpdate();
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    this.setClasses();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  componentDidUpdate() {
    this.setClasses();
  }

  setClasses() {
    var content = ReactDOM.findDOMNode(this.refs.content);
    var contentContainer = ReactDOM.findDOMNode(this.refs.mask);

    var ele = ReactDOM.findDOMNode(this.refs.bottomRuler);
    if (ele.isVisible()) {
      this.contentVisible = true;
      contentContainer.className = classNames(contentContainer.className, {'content-visible': true, 'content-covered': false}); 
    } else {
      this.contentVisible = false;
      contentContainer.className = classNames(contentContainer.className, {'content-visible': false, 'content-covered': true}); 
    }
    
    var contentHeight = getComputedStyle(content).getPropertyValue('height');
    var contentContainerHeight = getComputedStyle(contentContainer).getPropertyValue('height');

    if (parseInt(contentHeight) < parseInt(contentContainerHeight)) {
      this.contentIsSmaller = true;
      contentContainer.className = classNames(contentContainer.className, {'content-smaller': true});
    } else {
      this.contentIsSmaller = false;
      contentContainer.className = classNames(contentContainer.className, {'content-smaller': false});
    }
  }

  toggleOpen() {
    if (this.state.open) {
      this.setState({open: false});
    } else if (!this.contentIsSmaller) {
      this.setState({open: true});
    }
  }

  render() {
    var maskStyle = {};
    if (!this.state.open && this.props.height) {
      maskStyle.height = this.props.height + 'px';
    }

    return (
      <div className={classNames('expandable-text-container', {open: this.state.open, close: !this.state.open})} style={{fontSize: this.props.fontSize}} onClick={this.toggleOpen} >
        <div className='mask' style={maskStyle} ref='mask'>
          <div className='content' ref='content'>
            {this.props.children}
            <div className='bottom-ruler' ref='bottomRuler' />
          </div>
          <div className='ellipsis' />
        </div>
      </div>
    );
  }
}
