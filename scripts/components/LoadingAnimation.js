

import ReactPopover from 'react-popover';
import React, { Component, PropTypes, findDOMNode } from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import classnames from 'classnames';

import '../../styles/modules/loading_animation';

// https://github.com/littlebits/react-popover

export default class LoadingAnimation extends Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="bookshelf_wrapper">
        <ul className="books_list">
          <li className="book_item first"></li>
          <li className="book_item second"></li>
          <li className="book_item third"></li>
          <li className="book_item fourth"></li>
          <li className="book_item fifth"></li>
          <li className="book_item sixth"></li>
        </ul>
        <div className="shelf"></div>
      </div>
    );
  }
}
