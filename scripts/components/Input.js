import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import shouldPureComponentUpdate from 'react-pure-render/function';
import classNames from 'classnames';
import Popover from './Popover';

import '../../styles/modules/input';

export default class Input extends Component {
  static propTypes = {
    rowType: PropTypes.bool, // Tells if the title should be displayed on the row
    colType: PropTypes.bool, // Tells if the title should be displayed in the col
    label: PropTypes.string,
    labelWidth: PropTypes.number,
    inputWidth: PropTypes.number,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    errors: PropTypes.object,
    errorMessage: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    preventTabFrom: PropTypes.bool,
    inverted: PropTypes.bool,
    focusOnMount: PropTypes.bool,
    divider: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]), // Should add space below to separate from other elements
    contClassName: PropTypes.string,
    inputFieldClassName: PropTypes.string,
    inputClassName: PropTypes.string,
    info: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.getInputValue = this.getInputValue.bind(this);
  }

  componentDidMount() {
    if (this.props.focusOnMount) {
      ReactDOM.findDOMNode(this.refs.input).focus();
      try {
        ReactDOM.findDOMNode(this.refs.input).setSelectionRange(10000, 10000);
      } catch (err) {

      }
    }
  }

  onKeyDown(event) {
    if (this.props.preventTabFrom && event.keyCode == 9 && !event.ctrlKey) {
      event.preventDefault();
    }
  }

  render() {
    var errorStr = null;

    if (this.props.errors) {
      if (this.props.errorMessage) {
        errorStr = this.props.errorMessage;
      } else {
        errorStr = this.props.errors.toJS()[0];
      }
    }

    var labelStyle = {};

    if (this.props.labelWidth) {
      labelStyle.width = this.props.labelWidth + 'px';
    }

    var popoverInfo = null;
    if (this.props.info) {
      popoverInfo = 
        <Popover activateOnHover={true} body={this.props.info}>
          <div className='popover-info' />
        </Popover>
    }

    var label = null;

    if (this.props.label) {
      label = 
        <div className={classNames('label', {'fill-width': this.props.inputWidth})} style={labelStyle}>
          {this.props.label}
          {popoverInfo}
        </div>
    }

    var style = {};
    if (typeof this.props.divider == 'number') {
      style.marginBottom = this.props.divider + 'px';
    } else if (this.props.divider) {
      style.marginBottom = '5px';
    }

    var inputStyle = {};

    if (this.props.inputWidth) {
      inputStyle.width = this.props.inputWidth + 'px';
    }

    return (
      <div className={classNames('input-cont', this.props.contClassName, {'row': this.props.rowType, 'col': this.props.colType})} style={style}>
        {label}
        <div className={classNames('input-field fixed-height', this.props.inputFieldClassName, {'error': errorStr, 'fill-width': !this.props.inputWidth})} style={inputStyle}>
          <div className='validation'>
            <Popover 
              activateOnHover={true} 
              body={errorStr}>
              <div className='popover-info' />
            </Popover>
          </div>
          <input 
            className={classNames(this.props.inputClassName, {'inverted': this.props.inverted})}
            type={this.props.type}
            autoCapitalize='off'
            autoCorrect='off'
            autoComplete='off'
            size='45'
            ref='input'
            placeholder={this.props.placeholder}
            onChange={this.handleOnChange}
            onKeyUp={this.props.onKeyUp}
            onKeyDown={this.onKeyDown}
            value={this.props.value} />
        </div>
      </div>
    );
  }

  handleOnChange() {
    this.props.onChange(this.getInputValue());
  }

  getInputValue() {
    return ReactDOM.findDOMNode(this.refs.input).value;
  }
}
