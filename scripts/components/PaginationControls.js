import ReactPopover from 'react-popover';
import React, { Component, PropTypes, findDOMNode } from 'react';
import classNames from 'classnames';

import '../../styles/modules/pagination_controls';

// https://github.com/littlebits/react-popover

export default class Popover extends Component {
  static propTypes = {
    onFirstPageClick: PropTypes.func,
    onPreviousPageClick: PropTypes.func,
    onNextPageClick: React.PropTypes.func,
    onLastPageClick: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  render() {
    var firstButton, previousButton, nextButton, lastButton;
    if (this.props.onFirstPageClick) {
      firstButton = 
        <div className='pagination-button first-page enabled' onClick={this.props.onFirstPageClick} />
    } else {
      firstButton = 
        <div className='pagination-button first-page' />
    }
    if (this.props.onPreviousPageClick) {
      previousButton = 
        <div className='pagination-button previous-page enabled' onClick={this.props.onPreviousPageClick} />
    } else {
      previousButton = 
        <div className='pagination-button previous-page' />
    }
    if (this.props.onNextPageClick) {
      nextButton = 
        <div className='pagination-button next-page enabled' onClick={this.props.onNextPageClick} />
    } else {
      nextButton = 
        <div className='pagination-button next-page' />
    }
    if (this.props.onLastPageClick) {
      lastButton = 
        <div className='pagination-button last-page enabled' onClick={this.props.onLastPageClick} />
    } else {
      lastButton = 
        <div className='pagination-button last-page' />
    }

    return (
      <div className='pagination-area'>
        {firstButton}
        {previousButton}
        {nextButton}
        {lastButton}
      </div>
    );
  }
}
