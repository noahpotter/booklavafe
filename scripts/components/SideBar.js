import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { push } from 'react-router-redux'
import { logout, fetchClubs, showPopupForm } from '../actions'
import { getLoggedUser, hasLoggedUser, sortAscAlpha } from '../utils'
import Immutable from 'immutable'
import Popover from './Popover';

function defaultWindowDetails() {
  return {
    show: false,
    timeout: null,
    clickOpened: false
  }
}

function removeTimeouts(windows) {
  for (var w in windows) {
    if (windows[w].timeout) {
      window.clearTimeout(windows[w].timeout);
    }

    windows[w].timeout = null;
  }
  return windows;
}

function resetWindowsState(windows) {
  windows = removeTimeouts(windows);

  return {
    home: defaultWindowDetails(),
    profile: defaultWindowDetails(),
    search: defaultWindowDetails(),
    clubs: defaultWindowDetails(),
    feedback: defaultWindowDetails(),
    about: defaultWindowDetails()
  }
}

class SideBar extends Component {
  static propTypes = {
    // value: PropTypes.string.isRequired,
    // fieldName: PropTypes.string,
    // errors: PropTypes.array,
    // onChange: PropTypes.func.isRequired,
    // preventTabFrom: PropTypes.bool
    pathname: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.hideDisplay = this.hideDisplay.bind(this);
    this.hideDisplays = this.hideDisplays.bind(this);
    this.showDisplay = this.showDisplay.bind(this);
    this.toggleDisplay = this.toggleDisplay.bind(this);
    this.logout = this.logout.bind(this);
    this.renderClub = this.renderClub.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.setMaxWindowSize = this.setMaxWindowSize.bind(this);
    this.handleMouseEnterWindowIcon = this.handleMouseEnterWindowIcon.bind(this);
    this.handleMouseExitWindowIcon = this.handleMouseExitWindowIcon.bind(this);
    this.showPopupForm = this.showPopupForm.bind(this);
    this.searchNavigate = this.searchNavigate.bind(this);
    this.isShowingWindow = this.isShowingWindow.bind(this);
    this.state = {
      testing: false,
      windows: {
        home: defaultWindowDetails(),
        profile: defaultWindowDetails(),
        search: defaultWindowDetails(),
        clubs: defaultWindowDetails(),
        feedback: defaultWindowDetails(),
        about: defaultWindowDetails()
      }
    }
  }

  handleResize(e) {
    this.forceUpdate();
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    this.setMaxWindowSize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  componentWillUpdate() {
    if (this.state.windows.clubs.show) {
      var sidebarWindow = ReactDOM.findDOMNode(this.refs.clubsWindow);

      sidebarWindow.style.height = null;
      sidebarWindow.style.maxHeight = null;
    }
  }

  componentDidUpdate() {
    this.setMaxWindowSize();
  }

  handleMouseEnterWindowIcon(display) {
    this.hideDisplays();
    this.showDisplay(display);
    // // Setup a timeout that triggers the window to open
    // var state = this.state;
    // if (!state.windows[display].show) {
    //   state.windows = removeTimeouts(state.windows);

    //   var timeout = window.setTimeout(() => {
    //     // this.toggleDisplay(display);
    //     this.clickToggleDisplay(display);
    //   }, 500);
    //   state.windows[display].timeout = timeout;
    //   this.setState(state)
    // }
  }

  handleMouseExitWindowIcon(display) {
    this.hideDisplay(display);
    // var state = this.state;
    // if (!state.windows[display].show) {
    //   window.clearTimeout(this.state.windows[display].timeout);
    //   state.windows[display].timeout = null;
    //   this.setState(state);
    // } else if (!state.windows[display].clickOpened) {
    //   state.windows = resetWindowsState(state.windows);
    //   this.setState(state);
    // }
  }

  isShowingWindow() {
    for (var display in this.state.windows) {
      if (this.state.windows[display].show) {
        return true;
      }
    }
  }

  setMaxWindowSize() {
    if (this.state.windows.clubs.show) {
      var sidebarWindow = ReactDOM.findDOMNode(this.refs.clubsWindow);
      var sidebarWindowRect = sidebarWindow.getBoundingClientRect();
      var windowHeight = window.innerHeight;

      var maxHeight = windowHeight - sidebarWindowRect.top;
      sidebarWindow.style.height = Math.min(sidebarWindowRect.height, maxHeight) + 'px';
      sidebarWindow.style.maxHeight = Math.min(sidebarWindowRect.height, maxHeight) + 'px';
    }
  }

  showDisplay(display) {
    if (!this.state.testing) {
      var state = this.state;
      state.windows = resetWindowsState(state.windows);
      state.windows[display].show = true;
      this.setState(state);

      if (display == 'clubs' && state.windows[display].show) {
        this.props.fetchClubs(getLoggedUser().get('id'));
      }
    }
  }

  hideDisplay(display) {
    if (!this.state.testing) {
      var state = this.state;
      state.windows = resetWindowsState(state.windows);
      this.setState(state);
    }
  }

  clickToggleDisplay(display) {
    if (!this.state.testing) {
      var state = this.state;
      if (state.windows[display].show && !state.windows[display].clickOpened) {
        // just set clicked open to true
        state.windows[display].clickOpened = true;
        this.setState(state);
      } else {
        var toggledDisplay = !state.windows[display].show;
        state.windows = resetWindowsState(state.windows);
        state.windows[display].show = toggledDisplay;
        if (toggledDisplay) {
          state.windows[display].clickOpened = true;
        }

        this.setState(state);
        
        if (display == 'clubs' && state.windows[display].show) {
          this.props.fetchClubs(getLoggedUser().get('id'));
        }
      }
    }
  }

  toggleDisplay(display) {
    if (!this.state.testing) {
      var state = this.state;
      var toggledDisplay = !state.windows[display].show;
      state.windows = resetWindowsState(state.windows);
      state.windows[display].show = toggledDisplay;

      this.setState(state);
      
      if (display == 'clubs' && state.windows[display].show) {
        this.props.fetchClubs(getLoggedUser().get('id'));
      }
    }
  }

  hideDisplays() {
    if (!this.state.testing) {
      var newState = this.state;
      newState.windows = resetWindowsState(newState.windows);
      this.setState(newState);
    }
  }

  searchNavigate(query, event) {
    if (this.props.pathname == '/search' || this.props.pathname == '/search/') {
      // Do nothing, window will close
    } else {
      this.navigateFromWindow('/search', query, event);
    }
  }

  navigate(pathname, query) {
    this.props.push({pathname, query});
    // this.hideDisplays();
  }

  navigateFromWindow(pathname, query, event) {
    this.props.push({pathname, query});
    this.hideDisplays(); 
    event.preventDefault();
    event.stopPropagation();
  }

  showPopupForm(form) {
    this.props.showPopupForm(form);
    this.hideDisplays();
  }

  logout() {
    this.props.logout();
  }

  renderClub(club) {
    return(
      <div key={club.get('id')} className='sidebar-club-button' onClick={this.navigateFromWindow.bind(this, `/clubs/${club.get('id')}`, null)}>
        {club.get('name')}
      </div>
    );
  }

  render() {
    const user = getLoggedUser();

    if (user) {
      const userDetail = this.props.entities.getIn(['userDetails', user.get('userDetail').toString()]);

      var profileView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.profile.show})}>
          <div className='window-title' onClick={this.navigateFromWindow.bind(this, `/users/${user.get('id')}/`, null)}>
            Profile
          </div>
          <div className='window-content'>
            <div className='profile-area'>
              <div className='pic-area'>
                <div className='pic-background' />
                <img className='pic' src={userDetail.get('image')} onClick={this.navigateFromWindow.bind(this, `/users/${user.get('id')}/`, null)} />
              </div>
              <div className='profile-info-area'>
                <div className='profile-info' onClick={this.navigateFromWindow.bind(this, `/users/${user.get('id')}/`, null)}>
                  {user.get('username')}
                </div>
              </div>
            </div>
            <div className='logout-button' onClick={this.logout.bind(this)}>
              Logout
            </div>
          </div>
        </div>;
      var searchView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.search.show})}>
          <div className='window-title' onClick={this.searchNavigate.bind(this, null)}>
            Search
          </div>
          <div className='window-content no-padding'>
            <div className='sidebar-search-button' onClick={this.searchNavigate.bind(this, {type: 'books'})}>
              Books
            </div>
            <div className='sidebar-search-button' onClick={this.searchNavigate.bind(this, {type: 'clubs'})}>
              Clubs
            </div>
          </div>
        </div>;

      var clubs = this.props.entities.get('clubs').filter(club => user.get('subscribedClubs').contains(club.get('id')) || user.get('ownedClubs').contains(club.get('id')));


      var ownedClubs,
          ownedClubsSection,
          subscribedClubs,
          subscribedClubsSection;

      ownedClubs = this.props.entities.get('clubs').filter(club => user.get('ownedClubs').includes(club.get('id')))
      subscribedClubs = this.props.entities.get('clubs').filter(club => user.get('subscribedClubs').includes(club.get('id')) && !user.get('ownedClubs').includes(club.get('id')))

      if (ownedClubs.size > 0) {
        ownedClubsSection = sortAscAlpha(ownedClubs, 'name').map(club => {
          return this.renderClub(club)
        }).toArray()
      } else {
        ownedClubsSection = 
          <div className='sidebar-window-has-no-clubs'>
            No owned clubs.
          </div>
      }

      if (subscribedClubs.size > 0) {
        subscribedClubsSection = sortAscAlpha(subscribedClubs, 'name').map(club => {
          return this.renderClub(club)
        }).toArray()
      } else {
        subscribedClubsSection = 
          <div className='sidebar-window-has-no-clubs'>
            No subscribed clubs.
          </div>
      }

      var clubsView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.clubs.show})} ref='clubsWindow' >
          <div className='window-title' onClick={this.navigateFromWindow.bind(this, `/users/${user.get('id')}/clubs/`, null)}>
            Clubs
          </div>
          <div className='window-content no-padding'>
              <div className='window-section-heading'>
                Owned Clubs
              </div>
              {ownedClubsSection}
              <div className='window-section-heading'>
                Subscribed Clubs
              </div>
              {subscribedClubsSection}
          </div>
        </div>;

      var aboutView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.about.show})}>
          <div className='window-title' onClick={this.navigateFromWindow.bind(this, 'about', null)}>
            About
          </div>
        </div>;

      var feedbackView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.feedback.show})}>
          <div className='window-title' onClick={this.showPopupForm.bind(this, 'feedback')}>
            Feedback
          </div>
        </div>;

      var homepageView = 
        <div className={classNames('sidebar-window', {'show': this.state.windows.home.show})}>
          <div className='window-title' onClick={this.showPopupForm.bind(this, 'home')}>
            Booklava Home
          </div>
        </div>;

      return (
        <div className='sidebar'>
          <div className='buttons'>

            <div className='sidebar-button icon home-icon' onMouseEnter={this.showDisplay.bind(this, 'home')} onClick={this.navigate.bind(this, '/', null)}>
              
            </div>
            <div className={classNames('sidebar-button icon profile-icon', {'active': this.state.windows.profile.show})} onMouseEnter={this.handleMouseEnterWindowIcon.bind(this, 'profile')} >
              
            </div>
            <div className={classNames('sidebar-button icon search-icon', {'active': this.state.windows.search.show})} onMouseEnter={this.handleMouseEnterWindowIcon.bind(this, 'search')}  >
            </div>
            <div className={classNames('sidebar-button icon clubs-icon', {'active': this.state.windows.clubs.show})} onMouseEnter={this.handleMouseEnterWindowIcon.bind(this, 'clubs')} >
              
            </div>
            <div className='spacer' />
            <div className='sidebar-button icon question-icon' onMouseEnter={this.showDisplay.bind(this, 'feedback')} onClick={this.showPopupForm.bind(this, 'feedback')}>
              
            </div>
            <div className='sidebar-button icon about-icon' onMouseEnter={this.showDisplay.bind(this, 'about')}  onClick={this.navigate.bind(this, '/about', null)}>
            </div>
          </div>
          <div className={classNames('sidebar-content', {'show': this.isShowingWindow()})}>
            <div className='window'>
              {homepageView}
              {profileView}
              {searchView}
              {clubsView}
              {feedbackView}
              {aboutView}
            </div>
            <div className='backdrop' onClick={this.hideDisplays}>
            </div>
          </div>
        </div>
      );
    } else if (hasLoggedUser(this.props.loggedUserDetails)) {
      return (
        <div className='sidebar'>
          <div className='buttons'>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  const { entities, loggedUserDetails } = state;

  return {
    entities,
    loggedUserDetails
  };
}

export default connect(mapStateToProps, {
  logout,
  push,
  showPopupForm,
  fetchClubs
})(SideBar);