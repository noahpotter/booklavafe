import React, { Component, PropTypes, findDOMNode } from 'react';
import classNames from 'classnames';
import ReactDropzone from 'react-dropzone';
import Popover from './Popover';

import '../../styles/modules/dropzone';

export default class Dropzone extends Component {
  static propTypes = {
    rowType: PropTypes.bool, // Tells if the title should be displayed on the row
    colType: PropTypes.bool, // Tells if the title should be displayed in the col
    label: PropTypes.string,
    labelWidth: PropTypes.number,
    placeholder: PropTypes.string,
    errors: PropTypes.object,
    errorMessage: PropTypes.string,
    inverted: PropTypes.bool,
    divider: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
    onChange: PropTypes.func.isRequired,
    infoText: PropTypes.string,
    // existingFileUrls: PropTypes
  };

  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
    this.state = {
      files: []
    }
  }

  onDrop(files) {
    this.setState({
      files: files
    }, () => { this.props.onChange(this.state.files) });
  }

  render() {
    var errorStr = null;

    if (this.props.errors) {
      if (this.props.errorMessage) {
        errorStr = this.props.errorMessage;
      } else {
        errorStr = this.props.errors.toJS()[0];
      }
    }

    var labelStyle = {};
    if (this.props.labelWidth) {
      labelStyle.width = this.props.labelWidth + 'px';
    }


    var label = null;

    if (this.props.label) {
      label = 
        <div className='label' style={labelStyle}>
          {this.props.label}
        </div>
    }

    var style = {};
    if (typeof this.props.divider == 'number') {
      style.marginBottom = this.props.divider + 'px';
    } else {
      style.marginBottom = '5px';
    }

    var info = null;
    if (this.props.infoText) {
      info = 
        <Popover activateOnHover={true} body={this.props.infoText}>
          <div className='popover-info' />
        </Popover>
    }

    var content = null;
    var hasFiles = this.state.files.length > 0 || this.props.existingFileUrls && this.props.existingFileUrls.length > 0;

    if (this.state.files.length == 0 && this.props.existingFileUrls && this.props.existingFileUrls.length > 0) {
      content = 
        <div className='dropzone-previews'>
          { this.props.existingFileUrls.map(file => <img key={file} className='dropzone-preview' src={file} />) }
        </div>
    } else if (hasFiles) {
      content = 
        <div className='dropzone-previews'>
          { this.state.files.map(file => <img key={file.preview} className='dropzone-preview' src={file.preview} />) }
        </div>
    } else {
      content = 
        <div className='dropzone-placeholder'>{this.props.placeholder}</div>
    }

    return (
      <div className={classNames('input-cont', {'row': this.props.rowType, 'col': this.props.colType})} style={style}>
        {label}
        {info}
        <div className={classNames('input-field', {'expand': !hasFiles, 'error': errorStr})}>
          <div className='validation'>
            <Popover 
              activateOnHover={true} 
              body={errorStr}>
              <div className='popover-info' />
            </Popover>
          </div>
          <ReactDropzone className={classNames('dropzone', this.props.className, {'inverted': this.props.inverted})} accept='image/*' onDrop={this.onDrop}>
            <div className='dropzone-internal'>
              {content}
            </div>
          </ReactDropzone>
        </div>
      </div>
    );
  }
}
