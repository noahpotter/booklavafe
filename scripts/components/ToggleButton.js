import React, { Component, PropTypes, findDOMNode } from 'react';
import classnames from 'classnames';
import ReactToggle from 'react-toggle';

import '../../styles/modules/toggle_button';

export default class ToggleButton extends Component {
  static propTypes = {
    rowType: PropTypes.bool, // Tells if the title should be displayed on the row
    colType: PropTypes.bool, // Tells if the title should be displayed in the col
    label: PropTypes.string,
    oppositeValue: PropTypes.bool, // If true, it displays and returns the opposite values of the toggle
    value: PropTypes.bool.isRequired,
    fieldName: PropTypes.string,
    errors: PropTypes.array,
    onChange: PropTypes.func.isRequired,
    divider: PropTypes.bool // Should add space below to separate from other elements
  };

  constructor(props) {
    super(props);
    this.handleToggleChange = this.handleToggleChange.bind(this);
  }

  handleToggleChange(event) {
    if (this.props.oppositeValue) {
      this.props.onChange(!event.target.checked);
    } else {
      this.props.onChange(event.target.checked);
    }
  }

  render() {
    var errorStr = null;

    if (this.props.errors) {
      errorStr = `${this.props.fieldName} ${this.props.errors[0]}`;
    }

    var labelStyle = {};

    if (this.props.labelWidth) {
      labelStyle.width = this.props.labelWidth + 'px';
    }

    var label = null;

    if (this.props.label) {
      label = 
        <div className='label' style={labelStyle}>
          {this.props.label}
        </div>
    }

    var style = {};
    if (typeof this.props.divider == 'number') {
      style.marginBottom = this.props.divider + 'px';
    } else if (this.props.divider) {
      style.marginBottom = '5px';
    }

    return (
      <div className={classnames('input-cont', {'row': this.props.rowType, 'col': this.props.colType})} style={style} >
        {label}
        <div className={classnames('input-field toggle', {'error': errorStr})}>
          <ReactToggle
            onChange={this.handleToggleChange}
            defaultChecked={this.props.oppositeValue ? !this.props.value: this.props.value} />
          <div className='validation'>
            {errorStr}
          </div>
        </div>
      </div>
    );
  }
}

