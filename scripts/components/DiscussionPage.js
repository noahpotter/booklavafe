import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { startCreatingThread, pauseCreatingThread, cancelCreatingThread,
         deselectThreadItem, startCreatingPost, editCreatingPost, cancelCreatingPost, createPost,
         fetchPosts, toggleActiveThreadItem, deleteThread, fetchUser, toggleDiscussionDetails,
         showHierarchy, hideHierarchy, activate, deactivate, startEditingPost, editEditingPost, cancelEditingPost, 
         editCreatingThread, editPost, deletePost, fetchNewThreads, fetchNewPosts, reportPost } from '../actions';
import { sortAscDate, getActiveEntity, isElementInVisibleInParent } from '../utils';
import classNames from 'classnames/dedupe';
import Textarea from '../components/Textarea';
import Popover from '../components/Popover';

import '../../styles/modules/discussion';

var SCROLL_RATE = 2;
var SCROLL_DELTA = 3;

class DiscussionPage extends Component {
  static propTypes = {
    discussionType: PropTypes.string, // clubBookDiscussion, clubDiscussion
    discussionId: PropTypes.string,
    fetchDiscussions: PropTypes.func,
    fetchDiscussionThreads: PropTypes.func,
    renderDetails: PropTypes.func,
    createThreadFunc: PropTypes.func,
    parentId: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.pauseCreatingThread = this.pauseCreatingThread.bind(this);
    this.renderChildrenPosts = this.renderChildrenPosts.bind(this);
    this.clickThreadTopic = this.clickThreadTopic.bind(this);
    this.activatePost = this.activatePost.bind(this);
    this.activateOriginalPost = this.activateOriginalPost.bind(this);
    this.renderVisualizationArea = this.renderVisualizationArea.bind(this);
    this.renderVisualization = this.renderVisualization.bind(this);
    this.renderThread = this.renderThread.bind(this);
    this.deleteThread = this.deleteThread.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.onWheelPosts = this.onWheelPosts.bind(this);
    this.startScrollingLeft = this.startScrollingLeft.bind(this);
    this.stopScrollingLeft = this.stopScrollingLeft.bind(this);
    this.scrollLeft = this.scrollLeft.bind(this);
    this.startScrollingRight = this.startScrollingRight.bind(this);
    this.stopScrollingRight = this.stopScrollingRight.bind(this);
    this.scrollRight = this.scrollRight.bind(this);
    this.handleScrollAreaVisibility = this.handleScrollAreaVisibility.bind(this);
    this.startPanning = this.startPanning.bind(this);
    this.pan = this.pan.bind(this);
    this.stopPanning = this.stopPanning.bind(this);
    this.fetchNewEntities = this.fetchNewEntities.bind(this);
    this.getPostContent = this.getPostContent.bind(this);

    this.state = {
      scrollingTimeout: null,
      startLeft: null,
      startX: null,
      panning: false,
      fetchNewEntitiesInterval: null
    }
  }

  componentWillMount() {
    this.props.fetchDiscussions();
    this.props.fetchDiscussionThreads();
  }

  componentDidMount() {
    this.handleScrollAreaVisibility();
    var interval = window.setInterval(this.fetchNewEntities, 10000);
    this.setState({fetchNewEntitiesInterval: interval});
  }

  componentWillUnmount() {
    window.clearInterval(this.state.fetchNewEntitiesInterval);
    this.setState({fetchNewEntitiesInterval: null});
  }

  componentWillUpdate(nextProps) {
    if (!this.props.loggedUserDetails.get('id') && nextProps.loggedUserDetails.get('id')) {
      this.props.fetchDiscussions();
      this.props.fetchDiscussionThreads();
    }
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.threadDetails.get('showCreatingThread') && this.props.threadDetails.get('showCreatingThread')) {
      this.refs.threadInput.focus();
    }

    var previousCreatingPost = prevProps.postDetails.get('creatingPost');
    var creatingPost = this.props.postDetails.get('creatingPost');

    if ((!previousCreatingPost && creatingPost) || (creatingPost && previousCreatingPost.get('id') != creatingPost.get('id'))) {
      ReactDOM.findDOMNode(this.refs['post'+creatingPost.get('parentPostId')]).scrollIntoView(false);
    }

    this.props.threadDetails.get('showHierarchy').map(showThreadHierarchy => {
      const prevShowHierarchy = prevProps.threadDetails.getIn(['showHierarchy', showThreadHierarchy.get('thread').toString()])
      if (!prevShowHierarchy || prevShowHierarchy.get('post') != showThreadHierarchy.get('post')) {
        this.refs['post'+showThreadHierarchy.get('post')].scrollIntoView();
      }
    })

    this.handleScrollAreaVisibility();
  }

  getPostContent(post) {
    if (post.get('deleted')) {
      return '<deleted>';
    } else {
      return post.get('content');
    }
  }

  fetchNewEntities() {
    var threads = this.props.entities.get('threads').filter(thread => {
      return thread.get(this.props.discussionType) == this.props.discussionId;
    }).map(thread => {
      return thread.get('id')
    }).toArray();

    this.props.fetchNewThreads(this.props.discussionType, this.props.discussionId, threads);

    this.props.threadDetails.get('activeThreads').forEach(activeThread => {
      var posts = this.props.entities.get('posts').filter(post => {
        return post.get('thread') == activeThread.get('id');
      }).map(post => {
        return post.get('id')
      }).toArray();
      this.props.fetchNewPosts(activeThread.get('id'), posts);
    })
  }

  handleScrollAreaVisibility() {
    var ele = ReactDOM.findDOMNode(this.refs.threads);
    var position = ele.scrollLeft;
    var contentHeight = ele.scrollWidth - ele.clientWidth;
    var percentScrolled = position / contentHeight;

    if (this.refs.scrollLeftArea && this.refs.scrollRightArea) {
      var scrollLeftAreaEle = ReactDOM.findDOMNode(this.refs.scrollLeftArea);
      var scrollRightAreaEle = ReactDOM.findDOMNode(this.refs.scrollRightArea);

      if (isNaN(percentScrolled)) {
        scrollLeftAreaEle.className = classNames(scrollLeftAreaEle.className, {'hidden': true});
        scrollRightAreaEle.className = classNames(scrollRightAreaEle.className, {'hidden': true});
      } else if (percentScrolled == 0) {
        scrollLeftAreaEle.className = classNames(scrollLeftAreaEle.className, {'hidden': true});
        scrollRightAreaEle.className = classNames(scrollRightAreaEle.className, {'hidden': false});
      } else if (percentScrolled == 1) {
        scrollLeftAreaEle.className = classNames(scrollLeftAreaEle.className, {'hidden': false});
        scrollRightAreaEle.className = classNames(scrollRightAreaEle.className, {'hidden': true});
      } else {
        scrollLeftAreaEle.className = classNames(scrollLeftAreaEle.className, {'hidden': false});
        scrollRightAreaEle.className = classNames(scrollRightAreaEle.className, {'hidden': false});
      }
    }
  }

  stopMiddleClick(func, event) {
    if (event.button == 1) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      func();
    }
  }

  scrollLeft() {
    ReactDOM.findDOMNode(this.refs.threads).scrollLeft -= SCROLL_RATE;
    this.handleScrollAreaVisibility();
  }

  startScrollingLeft(event) {
    var timeout = window.setInterval(this.scrollLeft, SCROLL_DELTA);
    var state = this.state;
    state.scrollingTimeout = timeout;
    this.setState(state);
  }

  stopScrollingLeft(event) {
    window.clearInterval(this.state.scrollingTimeout);
    var state = this.state;
    state.scrollingTimeout = null;
    this.setState(state);
  }

  scrollRight(el) {
    ReactDOM.findDOMNode(this.refs.threads).scrollLeft += SCROLL_RATE;
    this.handleScrollAreaVisibility();
  }

  startScrollingRight(event) {
    var timeout = window.setInterval(this.scrollRight, SCROLL_DELTA);
    var state = this.state;
    state.scrollingTimeout = timeout;
    this.setState(state);
  }

  stopScrollingRight(event) {
    window.clearInterval(this.state.scrollingTimeout);
    var state = this.state;
    state.scrollingTimeout = null;
    this.setState(state);
  }

  startPanning(event) {
    // Check if middle mouse is held
    if (event.button == 1 && !this.state.panning) {
      var state = this.state;
      state.panning = true;
      state.startLeft = ReactDOM.findDOMNode(this.refs.threads).scrollLeft;
      state.startX = event.pageX;
      this.setState(state);
      event.stopPropagation();
    }
  }

  pan(event) {
    if (this.state.panning) {
      ReactDOM.findDOMNode(this.refs.threads).scrollLeft = this.state.startLeft + (this.state.startX - event.pageX);
    }
  }

  stopPanning(event) {
    // Check if middle mouse was let go
    if (event.button == 1 && this.state.panning) {
      var state = this.state;
      state.panning = false;
      this.setState(state);
      event.stopPropagation();
    }
  }

  onWheelPosts(event) {
    // Only stop propagation if the child post length hasn't been scrolled to the bottom yet
    var postsElement = event.currentTarget;
    var position = postsElement.scrollTop;
    var contentHeight = postsElement.scrollHeight - postsElement.clientHeight;
    var percentScrolled = position / contentHeight;

    var content = ReactDOM.findDOMNode(this.refs.content);
    var contentContainer = ReactDOM.findDOMNode(this.refs.mask);

    var ele = event.currentTarget.childNodes[0];

    // Only scroll the posts if fully visible, and we have more space to scroll in the scroll direction
    var isVisible = isElementInVisibleInParent(postsElement.parentNode);
    if (isVisible) { // We can scroll the posts now
      if (event.deltaY > 0 && percentScrolled < 0.99) { // We can scroll down
        // Do the normal scroll
        event.stopPropagation(); // Don't scroll sideways
      } else if (event.deltaY < 0 && percentScrolled > 0.01) {
        // Do the normal scroll
        event.stopPropagation(); // Don't scroll sideways
      } else { // We reached the extents of the scroll
      }
    } else {
      // We can't fully see the posts, don't scroll them and scroll sideways until we can see them
      event.preventDefault();
    }
  }

  deleteThread(threadId) {
    if (confirm('Delete this thread?')) {
      this.props.deleteThread(threadId);
    }
  }

  deletePost(threadId, postId) {
    if (confirm('Delete this post?')) {
      if (getActiveEntity(this.props.entities, this.props.interactionDetails, 'post')) {
        this.props.hideHierarchy(threadId);
        this.props.deactivate({post: true});
      }
      this.props.deletePost(postId);
    }
  }

  clickThreadTopic(thread) {
    this.props.fetchPosts(thread.get('id'))
    this.props.toggleActiveThreadItem(thread.get('id'));
  }

  pauseCreatingThread() {
    var pauseTypingNewThreadFunc = this.props.pauseCreatingThread;
    setTimeout(function() {pauseTypingNewThreadFunc();}, 200)
  }

  activatePost(post) {
    this.props.activate({post: post.get('id')});
    this.props.showHierarchy(post.get('thread'), post.get('id'));
    this.props.fetchUser(post.get('user'));
  }

  activateOriginalPost(post) {
    this.props.activate({post: post.get('id')});
    this.props.fetchUser(post.get('user'));
  }

  renderPostActions(post) {
    var activePost = getActiveEntity(this.props.entities, this.props.interactionDetails, 'post');
    if (activePost && activePost.get('id') == post.get('id')) {
      if (activePost.get('user') == this.props.loggedUserDetails.get('id')) { 
        var deletePostFunc = null;
        if (activePost.get('parentPost')) {
          deletePostFunc = this.deletePost.bind(this, post.get('thread'), post.get('id'));
        } else {
          deletePostFunc = this.deleteThread.bind(this, post.get('thread'));
        }

        var editingPost = this.props.postDetails.get('editingPost');

        if (editingPost && editingPost.get('id') == post.get('id')) {
          return (
              <div className='post-actions actions'>
                <div className='action inverted icon save-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.editPost.bind(this))} />
                <div className='action inverted icon cancel-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.cancelEditingPost)} />
              </div>
          );
        } else {
          return (
            <div className='post-actions actions'>
              <div className='action inverted icon reply-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.startCreatingPost.bind(this, {threadId: post.get('thread'), parentPostId: post.get('id')}))} />
              <div className='action inverted icon edit-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.startEditingPost.bind(this, post))} />
              <div className='action inverted icon remove-icon color-hover' onClick={this.stopMiddleClick.bind(this, deletePostFunc)} />
            </div>
          );
        }
      } else {
        var user = this.props.entities.getIn(['users', activePost.get('user').toString()]);
        var username = '';
        if (user) {
          username = user.get('username');
        }

        return (
          <div className='actions-area'>
            <div className='user-info'>
              {'- ' + username}
            </div>
            <div className='post-actions actions'>
              <div className='action inverted icon reply-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.startCreatingPost.bind(this, {threadId: post.get('thread'), parentPostId: post.get('id')}))} />
              <div className='action inverted icon report-icon color-hover' onClick={this.stopMiddleClick.bind(this, this.props.reportPost.bind(this, post.get('id')))} />
            </div>
          </div>
        );
      }
    } else {
      return (
        <div className='more-actions actions row'>
          <div key='more-icon' className='action inverted icon more-icon' onClick={this.stopMiddleClick.bind(this, this.props.activate.bind(this, {post: post.get('id')}))} />
        </div>
      )
    }
  }

  renderChildrenPosts(parentPost, isReplyingToThread, postReplyingTo) {
    var childPosts = this.props.entities.get('posts').filter(post => post.get('parentPost') == parentPost.get('id'));
    var childrenComponents = [];
    childPosts.forEach(post => {
      childrenComponents.push(this.renderChildrenPosts(post, isReplyingToThread, postReplyingTo));
    });

    var replying = '';
    if (isReplyingToThread) {
      replying = 'not-replying';

      if (postReplyingTo.get('id') == parentPost.get('id')) {
        replying = 'replying';
      }
    }

    var nestedLevel = parentPost.get('nestedLevel') >= 4 ? '4' : parentPost.get('nestedLevel');

    var editingPost = this.props.postDetails.get('editingPost');
    var content = null;

    if (editingPost && editingPost.get('id') == parentPost.get('id')) {
      content = 
        <Textarea 
          value={editingPost.get('content')}
          errors={this.props.statusDetails.getIn(['errors', `post_${editingPost.get('id')}`, 'content'])}
          onChange={this.props.editEditingPost.bind(this, 'content')}
          colType={true}
          inverted={true}
          autosize={true}
          focusOnMount={true}
          preventScrollBubble={true}
        />
    } else {
      content = 
        <div className='text' onClick={this.stopMiddleClick.bind(this, this.activatePost.bind(this, parentPost))} >
          {this.getPostContent(parentPost)}
        </div>
    }

    var myPostIndicator = null;

    if (parentPost.get('user') == this.props.loggedUserDetails.get('id')) {
      myPostIndicator = 
        <div className='my-post-indicator' />
    }

    return (
      <div key={'parentPost'+parentPost.get('id')}>
        <div className={classNames('post', `level${nestedLevel}`, replying, {'form-disabled': this.props.statusDetails.getIn(['progress', 'deletePost']) == parentPost.get('id')})} ref={`post${parentPost.get('id')}`}>
          {content}
          {this.renderPostActions(parentPost)}
          {myPostIndicator}
        </div>
        <div className='replies'>
          {childrenComponents}
        </div>
      </div>
    );
  }

  renderLeafPostHierarchy(leafPostId) {
    var leafPost = this.props.entities.get('posts').find(post => post.get('id') == leafPostId);
    var postsInHierarchy = [leafPost];
    var postsInHierarchyComponents = [];
    var currentPost = leafPost;
    while(currentPost.get('parentPost')) {
      var nextPost = this.props.entities.get('posts').find(post => post.get('id') == currentPost.get('parentPost'));

      if (nextPost.get('parentPost')) {
        postsInHierarchy.push(nextPost);
      }

      currentPost = nextPost;
    }

    var editingPost = this.props.postDetails.get('editingPost');

    postsInHierarchy.reverse();
    postsInHierarchy.forEach(post => {
      var myPostIndicator = null;

      if (post.get('user') == this.props.loggedUserDetails.get('id')) {
        myPostIndicator = 
          <div className='my-post-indicator' />
      }

      var content = null;

      if (editingPost && editingPost.get('id') == post.get('id')) {
        content = 
          <Textarea 
            value={editingPost.get('content')}
            errors={this.props.statusDetails.getIn(['errors', `post_${editingPost.get('id')}`, 'content'])}
            onChange={this.props.editEditingPost.bind(this, 'content')}
            colType={true}
            inverted={true}
            autosize={true}
            focusOnMount={true}
            preventScrollBubble={true}
          />
      } else {
        content = 
          <div className='text' onClick={this.stopMiddleClick.bind(this, this.props.hideHierarchy.bind(this, post.get('thread')))}>
            {this.getPostContent(post)}
          </div>
      }

      postsInHierarchyComponents.push(
        <div key={'post'+post.get('id')} ref={`post${post.get('id')}`}>
          <div className={classNames('post', 'level0', {replying: post.get('id') == leafPost.get('id'), 'form-disabled': this.props.statusDetails.getIn(['progress', 'deletePost']) == leafPost.get('id')})}>
            {content}
            {myPostIndicator}
            {this.renderPostActions(post)}
          </div>
        </div>
      );
    });

    return postsInHierarchyComponents;
  }

  renderThreadTopic(thread) {
    var originalPost = this.props.entities.get('posts').find(post => post.get('id') == thread.get('originalPost'));
    var isActive = this.props.threadDetails.get('activeThreads').has(thread.get('id').toString())

    return (
      <div className={`thread-topic ${isActive ? 'active' : ''}`}>
        <div className='thread-topic-text' onClick={this.clickThreadTopic.bind(this, thread)}>
          {this.getPostContent(originalPost)}
        </div>
        <div className='thread-topic-actions'>
          <div className={classNames('thread-topic-action icon color-hover', {'close-thread-icon': isActive, 'open-thread-icon': !isActive})} onClick={isActive ? this.props.deselectThreadItem.bind(this, thread.get('id')) : this.clickThreadTopic.bind(this, thread)}>
          </div>
          {/*<div className={classNames('thread-topic-action icon color-hover favorite-thread-icon')}>
                    </div>*/}
        </div>
        <div className='active-area' style={{background: this.props.discussionDetails.getIn(['colors', parseInt(this.props.threadDetails.getIn(['activeThreads', thread.get('id').toString(), 'color']))])}} />
      </div>
    );
  }

  renderThread(thread) {
    if (this.props.threadDetails.get('activeThreads').has(thread.get('id').toString())) {

      var originalPost = this.props.entities.get('posts').find(post => post.get('id') == thread.get('originalPost'))
      var replyingToPostId = this.props.postDetails.getIn(['creatingPost', 'parentPostId']);
      var postReplyingTo = null;

      if (replyingToPostId) {
        postReplyingTo = this.props.entities.get('posts').find(post => post.get('id') == replyingToPostId);
      }

      var isReplyingToThread = postReplyingTo && thread.get('id') == postReplyingTo.get('thread');

      var editingPost = this.props.postDetails.get('editingPost');
      var content = null;

      if (editingPost && editingPost.get('id') == originalPost.get('id')) {
        content = 
          <Textarea 
            value={editingPost.get('content')}
            errors={this.props.statusDetails.getIn(['errors', `post_${editingPost.get('id')}`, 'content'])}
            onChange={this.props.editEditingPost.bind(this, 'content')}
            colType={true}
            inverted={true}
            autosize={true}
            focusOnMount={true}
            preventScrollBubble={true}
          />
      } else {
        content = 
          <div className='text' onClick={this.stopMiddleClick.bind(this, this.activateOriginalPost.bind(this, originalPost))} >
            {this.getPostContent(originalPost)}
          </div>
      }

      var myPostIndicator = null;

      if (originalPost.get('user') == this.props.loggedUserDetails.get('id')) {
        myPostIndicator = 
          <div className='my-post-indicator' />
      }


      var originalPostComponent = 
        <div className={classNames('post', 'original-post', {replying: postReplyingTo && originalPost.get('id') == postReplyingTo.get('id'), 'form-disabled': this.props.statusDetails.getIn(['progress', 'deletePost']) == originalPost.get('id')})}
        >
          {content}
          {myPostIndicator}
          {this.renderPostActions(originalPost)}
        </div>

      var cancelHierarchy = null;
      var hierarchyInfo = null;
      var replies = [];

      if (this.props.threadDetails.get('showHierarchy').has(thread.get('id').toString())) {
        var postId = this.props.threadDetails.getIn(['showHierarchy', thread.get('id').toString(), 'post']);
        hierarchyInfo = 
          <Popover
            activateOnHover={true}
            activateOnClick={true}
            activateAfterTime={0.5}
            maxWidth={200}
            body={<div><div>Currently displaying the parent posts that led to the selected post.</div><br /><div>Click the "x" icon or the post again to cancel this view.</div></div>}
            preferPlace='right'
          >
            <div className='popover-info' />
          </Popover>
        cancelHierarchy = <div key='cancel-hierarchy' className='cancel-hierarchy' onClick={this.stopMiddleClick.bind(this, this.props.hideHierarchy.bind(this, thread.get('id')))} />
        replies = this.renderLeafPostHierarchy(postId);
      } else {
        var posts = this.props.entities.get('posts').filter(post => post.get('parentPost') == originalPost.get('id'));
        sortAscDate(posts).forEach(post => {
          replies.push(
            this.renderChildrenPosts(post, isReplyingToThread, postReplyingTo)
          );
        });
      }

      var replyInput = null;
      if (isReplyingToThread) {
        replyInput = 
          <div className={classNames('input-area active', {'form-disabled': this.props.statusDetails.getIn(['progress', 'replyPost']) == postReplyingTo.get('thread')})} key={postReplyingTo.get('id')}>
            <Textarea
              value={this.props.postDetails.getIn(['creatingPost', 'content'])}
              placeholder='Type here...'
              inverted={true}
              inputFieldClassName='fill-width'
              errors={this.props.statusDetails.getIn(['errors', `thread_${thread.get('id')}`, 'content'])}
              autosize={true}
              maxHeight={100}
              minRows={3}
              focusOnMount={true}
              ref={`replyInput${postReplyingTo.get('id')}`}
              preventScrollBubble={true}
              onChange={this.props.editCreatingPost.bind(this, 'content')} />
            <div className='actions'>
              <div className='action inverted cancel-button' onClick={this.stopMiddleClick.bind(this, this.props.cancelCreatingPost)}>
                Cancel
              </div>
              <div className='action submit-button' onClick={this.stopMiddleClick.bind(this, this.props.createPost)}>
                Send
              </div>
            </div>
          </div>
      }

      return (
        <div className={classNames('thread', {'form-disabled': this.props.statusDetails.getIn(['progress', 'deleteThread']) == thread.get('id')})}>
          <div className='thread-header' style={{ background: this.props.discussionDetails.getIn(['colors', parseInt(this.props.threadDetails.getIn(['activeThreads', thread.get('id').toString(), 'color']))]) }}>
            <div className='close-thread-button' onClick={this.stopMiddleClick.bind(this, this.props.deselectThreadItem.bind(this, thread.get('id')))} />
          </div>
          {originalPostComponent}
          <div className='hierarchy-actions'>
            {hierarchyInfo}
            {cancelHierarchy}
          </div>
          <div className='posts' /*onWheel={this.onWheelPosts}*/>
            {replies}
          </div>
          {replyInput}
        </div>
      );
    }
  }

  renderVisualizationArea(discussion) {
    return (
      <div className='visualization-area'>
        <Popover
          activateOnHover={true}
          activateAfterTime={0.5}
          body='Back to club'
          preferPlace='right'
        >
          <div className={classNames({'discussion-details-button': this.props.parentId, 'discussion-details-button-placeholder': !this.props.parentId})} onClick={this.props.push.bind(this, {pathname: `/clubs/${this.props.parentId}`})}>
            <div className='discussion-details-button-back' />
          </div>
        </Popover>
        {this.renderVisualization()}
        <Popover
          activateOnHover={true}
          activateAfterTime={0.5}
          body='Discussion details'
          preferPlace='right'
        >
          <div className={classNames('discussion-details-button', {'open': this.props.discussionDetails.get('detailsOpen')})} onClick={this.props.toggleDiscussionDetails}>
            <div className='discussion-details-button-circle' />
            <div className='discussion-details-button-circle' />
            <div className='discussion-details-button-circle' />
          </div>
        </Popover>
      </div>
    );
  }

  renderVisualization() {
    return (
      <div className='visualization'>
      </div>
    );
  }

  render() {
    var threads = this.props.entities.get('threads').filter(thread => thread.get(this.props.discussionType) == this.props.discussionId);
    var discussion = this.props.entities.getIn([this.props.discussionType + 's', this.props.discussionId.toString()]);

    var threadTopicArea = null;

    if (this.props.discussionDetails.get('detailsOpen')) {
      threadTopicArea =
        <div className='thread-topic-area'>
          {this.renderVisualizationArea(discussion)}
          {this.props.renderDetails()}
        </div>
    } else {
      var threadPreviewComponents;
      var activeThreadComponents;

      if (discussion) {
        threadPreviewComponents = sortAscDate(threads, 'date').map(thread => this.renderThreadTopic(thread));
        activeThreadComponents = sortAscDate(threads, 'date').map(thread => this.renderThread(thread));
      }

      var inputArea = null;
      if (this.props.threadDetails.get('showCreatingThread')) {
        inputArea =
          <div className={classNames('input-area active', {'form-disabled': this.props.statusDetails.getIn(['progress', 'createThread'])})}>
            <Textarea
              value={this.props.threadDetails.getIn(['creatingThread', 'content'])}
              placeholder='Type here...'
              inverted={true}
              inputFieldClassName='fill-width'
              errors={this.props.statusDetails.getIn(['errors', 'thread', 'content'])}
              autosize={true}
              maxHeight={100}
              minRows={3}
              focusOnMount={true}
              onChange={this.props.editCreatingThread.bind(this, 'content')}
              ref='threadInput' />
            <div className='actions'>
              <div className='action inverted cancel-button' onClick={this.props.cancelCreatingThread}>
                Cancel
              </div>
              <div className='action submit-button' onClick={this.props.createThreadFunc}>
                Send
              </div>
            </div>
          </div>
      }

      threadTopicArea =
        <div className='thread-topic-area'>
          {this.renderVisualizationArea(discussion)}
          <div className='thread-topics-actions'>
            <div className='create-thread-topic inverted primary-button' onClick={this.props.startCreatingThread.bind(this, this.props.discussionType, this.props.discussionId)} />
          </div>
          <div className='thread-topics'>
            {threadPreviewComponents}
          </div>
          {inputArea}
        </div>
    }

    var scrollLeftArea, scrollRightArea;
    if (!this.state.panning) {
      scrollLeftArea =
        <div className='scroll-left-area' ref='scrollLeftArea' onMouseEnter={this.startScrollingLeft} onMouseLeave={this.stopScrollingLeft} />
      scrollRightArea =
        <div className='scroll-right-area' ref='scrollRightArea' onMouseEnter={this.startScrollingRight} onMouseLeave={this.stopScrollingRight} />
    }

    return (
      <DocumentTitle title={'Discuss'}>
        <div className='discussion-area' onMouseMove={this.pan} onMouseLeave={this.stopPanning} onMouseUp={this.stopPanning}>
          {threadTopicArea}
          <div className='thread-area' ref='threadArea'>
            {scrollLeftArea}
            <div className='threads' ref='threads' onMouseDown={this.startPanning} onMouseUp={this.stopPanning}>
              {activeThreadComponents}
            </div>
            {scrollRightArea}
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

function mapStateToProps(state) {
  const { entities,
          discussionDetails, 
          loggedUserDetails, 
          interactionDetails, 
          threadDetails,
          postDetails, 
          statusDetails } = state;

  return {
    entities,
    discussionDetails,
    loggedUserDetails,
    interactionDetails,
    threadDetails,
    postDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  startCreatingThread, 
  cancelCreatingThread,
  pauseCreatingThread, 
  deleteThread,
  deselectThreadItem, 
  startCreatingPost, 
  editCreatingPost, 
  cancelCreatingPost, 
  createPost,
  fetchPosts,
  showHierarchy, 
  hideHierarchy, 
  editCreatingThread,
  toggleDiscussionDetails,
  fetchUser,
  activate, 
  deactivate, 
  fetchNewPosts,
  fetchNewThreads,
  reportPost,
  toggleActiveThreadItem,
  startEditingPost, 
  editEditingPost, 
  cancelEditingPost, 
  editPost, 
  deletePost,
  push 
})(DiscussionPage);