import ReactPopover from 'react-popover';
import React, { Component, PropTypes, findDOMNode } from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import classNames from 'classnames';

import '../../styles/modules/input';

// https://github.com/littlebits/react-popover

export default class Popover extends Component {
  static propTypes = {
    activateOnHover: PropTypes.bool,
    activateOnClick: PropTypes.bool,
    activateAfterTime: PropTypes.number,
    forceOpen: PropTypes.bool,
    children: React.PropTypes.element.isRequired,
    body: React.PropTypes.string,
    preferPlace: React.PropTypes.string,
    place: React.PropTypes.string,
    maxWidth: React.PropTypes.number
  };

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);

    this.state = {
      isOpen: props.forceOpen,
      currentTimeout: null
    }
  }

  componentDidMount() {
    if (this.refs.popover.bodyEl) {
      // this.refs.popover.bodyEl.style.padding = '200px';
    }
  }

  toggle (toState = null) {
    this.setState({ isOpen: toState === null ? !this.state.isOpen : toState })
  }

  mouseOver() {
    if (this.props.activateAfterTime) {
      var timeout = setTimeout(this.toggle.bind(this, true), this.props.activateAfterTime*1000);
      this.setState({currentTimeout: timeout});
    } else {
      this.toggle(true);
    }
  }

  mouseOut() {
    if (this.props.activateAfterTime) {
      clearTimeout(this.state.currentTimeout);
    }
    this.toggle(false);
  }

  onClick() {
    if (this.state.currentTimeout) {
      clearTimeout(this.state.currentTimeout);
    }

    this.toggle();
  }

  render() {
    var child = this.props.children;

    var methods = {};

    methods.onMouseOut = () => { this.mouseOut() }

    if (this.props.activateOnHover) {
      methods.onMouseOver = () => { this.mouseOver() }
    }

    if (this.props.activateOnClick) {
      methods.onClick = () => { this.onClick() }
    }
    
    child = React.cloneElement(child, methods); 

    var style = {};
    if (this.props.maxWidth) {
      style.width = `${this.props.maxWidth}px`;
    }

    var body =
      <div className='popover-body' style={style}>
        {this.props.body}
      </div>

    /*
      Typical children: (I don't make this permanent in case the popover should have different icons)
      <div className='popover-info' />
    */

    return (
      <ReactPopover {...this.props} body={body} ref='popover' isOpen={this.state.isOpen}>
        {child}
      </ReactPopover>
    );
  }
}
