import React, { Component, PropTypes, findDOMNode } from 'react';
import classnames from 'classnames';

import '../../styles/modules/popup';

export default class Popup extends Component {
  static propTypes = {
    visible: PropTypes.bool,
    title: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    onCancel: PropTypes.func,
    cancelText: PropTypes.string,
    onAccept: PropTypes.func,
    acceptText: PropTypes.string
  };

  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.visible) {
      var style = {};
      if (this.props.width) {
        style.width = width;
      }

      if (this.props.height) {
        style.height = height;
      }

      return (
        <div className='popup-background' >
          <div className='popup' >
            <div className='popup-title' >
              <div className='popup-title-text' >
                {this.props.title}
              </div>
              <div className='popup-title-close icon cancel-icon' onClick={this.props.onCancel} />
            </div>
            <div className='popup-body' >
              {this.props.children}
            </div>
            <div className='popup-actions' >
              <div className='popup-action button cancel-button' onClick={this.props.onCancel} >
                {this.props.cancelText || 'Cancel'}
              </div>
              <div className='popup-action button primary-button' onClick={this.props.onAccept} >
                {this.props.acceptText || 'Accept'}
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

