import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import AutosizeTextarea from 'react-textarea-autosize';
import classNames from 'classnames';
import Popover from './Popover';

import '../../styles/modules/input';

export default class Textarea extends Component {
  static propTypes = {
    rowType: PropTypes.bool, // Tells if the title should be displayed on the row
    colType: PropTypes.bool, // Tells if the title should be displayed in the col
    label: PropTypes.string,
    value: PropTypes.string.isRequired,
    fieldName: PropTypes.string,
    errors: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    preventTabFrom: PropTypes.bool,
    inverted: PropTypes.bool,
    maxHeight: PropTypes.number,
    divider: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]), // Should add space below to separate from other elements
    focusOnMount: PropTypes.bool,
    contClassName: PropTypes.string,
    inputFieldClassName: PropTypes.string,
    inputClassName: PropTypes.string,
    autosize: PropTypes.bool,
    minRows: PropTypes.number,
    info: PropTypes.string,
    placeholder: PropTypes.string,
    preventScrollBubble: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.getInputValue = this.getInputValue.bind(this);
    this.focus = this.focus.bind(this);
    this.onWheel = this.onWheel.bind(this);
  }

  onKeyDown(event) {
    if (this.props.preventTabFrom && event.keyCode == 9 && !event.ctrlKey) {
      event.preventDefault();
    }
  }

  componentDidMount() {
    if (this.props.focusOnMount) {
      ReactDOM.findDOMNode(this.refs.input).focus();
      ReactDOM.findDOMNode(this.refs.input).setSelectionRange(10000, 10000);
    }
  }

  focus() {
    this.refs.input.focus();
  }

  onWheel(event) {
    if (this.props.preventScrollBubble) {
      event.stopPropagation();
    }
  }

  render() {
    var errorStr = null;

    if (this.props.errors) {
      errorStr = this.props.errors.toJS()[0];
    }
    
    var labelStyle = {};

    if (this.props.labelWidth) {
      labelStyle.width = this.props.labelWidth + 'px';
    }

    var popoverInfo = null;
    if (this.props.info) {
      popoverInfo = 
        <Popover activateOnHover={true} body={this.props.info}>
          <div className='popover-info' />
        </Popover>
    }

    var label = null;

    if (this.props.label) {
      label = 
        <div className={classNames('label', {'fill-width': this.props.inputWidth})} style={labelStyle}>
          {this.props.label}
          {popoverInfo}
        </div>
    }

    var style = {};
    if (typeof this.props.divider == 'number') {
      style.marginBottom = this.props.divider + 'px';
    } else if (this.props.divider) {
      style.marginBottom = '5px';
    }

    var textareaStyle = {};
    if (this.props.maxHeight) {
      textareaStyle.maxHeight = this.props.maxHeight + 'px';
    }

    var selectedTextArea = null;
    if (this.props.autosize) {
      selectedTextArea =
        <AutosizeTextarea
          className={classNames('autosize', this.props.className, {'inverted': this.props.inverted})}
          autoCapitalize='off'
          autoCorrect='off'
          autoComplete='off'
          ref='input'
          onChange={this.handleOnChange}
          onKeyDown={this.onKeyDown}
          value={this.props.value}
          style={textareaStyle}
          placeholder={this.props.placeholder}
          onWheel={this.onWheel}
          minRows={this.props.minRows} />
    } else {
      selectedTextArea =
        <textarea
          className={classNames(this.props.className, {'inverted': this.props.inverted})}
          autoCapitalize='off'
          autoCorrect='off'
          autoComplete='off'
          ref='input'
          style={textareaStyle}
          onChange={this.handleOnChange}
          onKeyDown={this.onKeyDown}
          placeholder={this.props.placeholder}
          onWheel={this.onWheel}
          value={this.props.value} />
    }

    var errorPopover = null;
    if (errorStr) {
      errorPopover = 
        <Popover 
          activateOnHover={true} 
          body={errorStr}>
          <div className='popover-info' />
        </Popover>
    }

    return (
      <div className={classNames('input-cont', this.props.contClassName, {'row': this.props.rowType, 'col': this.props.colType, 'expand-height': !this.props.autosize})} style={style}>
        {label}
        <div className={classNames('input-field text-area expand-height', this.props.inputFieldClassName, {'error': errorStr})}>
          <div className='validation'>
            {errorPopover}
          </div>
          {selectedTextArea}
        </div>
      </div>
    );
  }

  handleOnChange() {
    this.props.onChange(this.getInputValue());
  }

  getInputValue() {
    return ReactDOM.findDOMNode(this.refs.input).value;
  }
}
