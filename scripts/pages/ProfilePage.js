import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { fetchUser, startEditingUser, cancelEditingUser, editEditingUser, editUser,
         startEditingUserDetail, cancelEditingUserDetail, editEditingUserDetail, editUserDetail } from '../actions';
import Input from '../components/Input';
import classNames from 'classnames';
import { getLoggedUser } from '../utils'
import moment from 'moment';
import Popover from '../components/Popover';
import Dropzone from '../components/Dropzone';

// import '../../styles/modules/user';

import Immutable from 'immutable';

class ProfilePage extends Component {

  constructor(props) {
    super(props);

    this.renderOwnProfile = this.renderOwnProfile.bind(this);
    this.renderProfile = this.renderProfile.bind(this);
    this.startEditing = this.startEditing.bind(this);
    this.cancelEditing = this.cancelEditing.bind(this);
    this.edit = this.edit.bind(this);
  }

  componentWillMount() {
    this.fetchUser();
  }

  fetchUser() {
    this.props.fetchUser(this.props.params.id)
  }

  startEditing(user, userDetail) {
    this.props.startEditingUser(user);
    this.props.startEditingUserDetail(userDetail);
  }

  cancelEditing() {
    this.props.cancelEditingUser();
    this.props.cancelEditingUserDetail();
  }

  edit() {
    this.props.editUser();
    this.props.editUserDetail({resetStatus: false});
  }

  renderOwnProfile(user) {
    if (user) {
      var userDetail = this.props.entities.getIn(['userDetails', user.get('userDetail').toString()])

      var editProfileComponent = null;
      var editingUser = this.props.userDetails.get('editingUser');

      if (editingUser) {
        var editingUserDetail = this.props.userDetailDetails.get('editingUserDetail')
        var existingFileUrls = [];
        var image = editingUserDetail.get('image');

        if (typeof image == 'string') {
          existingFileUrls = [image];
        }

        editProfileComponent =
          <div className='edit-profile-form'>
            <Input
              rowType={true}
              label='Username'
              value={editingUser.get('username') || ''}
              inverted={true}
              labelWidth={170}
              divider={true}
              disabled={true}
              errors={this.props.statusDetails.getIn(['errors', 'username'])}
              onChange={this.props.editEditingUser.bind(this, 'username')} />
            <Input
              rowType={true}
              label='Email'
              value={editingUser.get('email') || ''}
              inverted={true}
              labelWidth={170}
              divider={true}
              errors={this.props.statusDetails.getIn(['errors', 'email'])}
              onChange={this.props.editEditingUser.bind(this, 'email')} />
            <Input
              rowType={true}
              label='New Password'
              value={editingUser.get('newPassword') || ''}
              inverted={true}
              labelWidth={170}
              divider={true}
              name='dont-save-password'
              type='password'
              errors={this.props.statusDetails.getIn(['errors', 'newPassword'])}
              onChange={this.props.editEditingUser.bind(this, 'newPassword')} />
            <Input
              rowType={true}
              label='Password Confirmation'
              value={editingUser.get('passwordConfirmation') || ''}
              inverted={true}
              labelWidth={170}
              divider={true}
              type='password'
              errors={this.props.statusDetails.getIn(['errors', 'passwordConfirmation'])}
              onChange={this.props.editEditingUser.bind(this, 'passwordConfirmation')} />
            <Dropzone
              inverted={true}
              rowType={true}
              label='Profile Image'
              labelWidth={170}
              divider={30}
              placeholder='Drag or click here.'
              errors={this.props.statusDetails.getIn(['errors', 'image'])}
              errorMessage={'Valid images include (png, jpg).'}
              onChange={this.props.editEditingUserDetail.bind(this, 'image')}
              existingFileUrls={existingFileUrls}
             />
            <Input
              rowType={true}
              label='Confirm Password'
              value={editingUser.get('password') || ''}
              inverted={true}
              labelWidth={170}
              divider={true}
              name='dont-save-password'
              type='password'
              divider={30}
              errors={this.props.statusDetails.getIn(['errors', 'password'])}
              onChange={this.props.editEditingUser.bind(this, 'password')} 
              onKeyUp={this.handleKeyUp.bind(this, this.edit)}
              />
            <div className='form-actions'>
              <div className='form-action cancel-button inverted' onClick={this.cancelEditing}>
                Cancel
              </div>
              <div className='form-action submit-button' onClick={this.edit}>
                Save
              </div>
            </div>
          </div>
      }

      return (
        <DocumentTitle title={'User'}>
          <div className='page'>
            <div className='user-detail-area'>
              <div className='pic-area'>
                <div className='pic-background' />
                <img className='pic' src={userDetail.get('image')} />
              </div>
              <div className='user-detail-name'>
                {user.get('username')}
                <div className='icon edit-icon' onClick={this.startEditing.bind(this, user, userDetail)} />
              </div>
            </div>
            <div className={classNames('user-info-area', {'form-disabled': this.props.statusDetails.getIn(['progress', 'editProfile'])})}>
              {editProfileComponent}
            </div>
          </div>
        </DocumentTitle>
      );
    } else {
      return (
        <DocumentTitle title={'User'}>
          <div className='page'>
            <div className='user-detail-area'>
              <div className='pic-area'>
                <img className='pic' src={userDetail.get('image')} />
              </div>
              <div className='user-detail-name'>
              </div>
            </div>
            <div className='user-info-area'>
            </div>
          </div>
        </DocumentTitle>
      );
    }
  }

  renderProfile(user) {
    const userDetail = this.props.entities.getIn(['userDetails', user.get('userDetail').toString()]);

    return (
      <DocumentTitle title={'User'}>
        <div className='page'>
          <div className='user-detail-area'>
            <div className='pic-area'>
              <img className='pic' src={userDetail.get('image')} />
            </div>
            <div className='user-detail-name'>
              {user.get('username')}
            </div>
          </div>
          <div className='user-info-area'>
          </div>
        </div>
      </DocumentTitle>
    );
  }

  render() {
    var user = this.props.entities.getIn(['users', this.props.params.id.toString()]);
    const loggedUser = getLoggedUser();

    if (user && loggedUser) {
      if (user.get('id') == loggedUser.get('id')) {
        return this.renderOwnProfile(user);
      } else {
        return this.renderProfile(user);
      }
    } else {
      return null;
    }
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }
}

function mapStateToProps(state) {
  const { entities, routing, userDetails, userDetailDetails, loggedUserDetails, statusDetails } = state;

  return {
    entities,
    routing,
    userDetails,
    userDetailDetails,
    loggedUserDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  startEditingUser,
  cancelEditingUser,
  editEditingUser,
  editUser,
  startEditingUserDetail,
  cancelEditingUserDetail,
  editEditingUserDetail,
  editUserDetail,
  fetchUser,
})(ProfilePage);