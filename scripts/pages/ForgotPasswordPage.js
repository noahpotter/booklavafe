import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { forgotPassword, updateLoginDetails } from '../actions';
import classNames from 'classnames';
import Input from '../components/Input';

class ForgotPasswordPage extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleSendForgotPasswordEmailClick = this.handleSendForgotPasswordEmailClick.bind(this);
    this.handleFieldUpdate = this.handleFieldUpdate.bind(this);
  }

  render() {
    return (
      <DocumentTitle title={'Forgot Password'}>
        <div className={classNames('page', {'form-disabled': this.props.statusDetails.getIn(['progress', 'forgotPassword'])})}>
          <div className='header'>
            Booklava
          </div>
          <div className='body'>
            <div className='input-row'>
              <Input
                type='email' 
                name='email' 
                placeholder='Email' 
                value={this.props.loginDetails.get('email')}
                fieldName='Email'
                focusOnMount={!!this.props.loginDetails.get('email')}
                onKeyUp={this.handleKeyUp.bind(this, this.handleSendForgotPasswordEmailClick)}
                onChange={this.handleFieldUpdate.bind(this, ['email'])} />
            </div>
          </div>
          <div className='footer'>
            <div className='submit-button' onClick={this.handleSendForgotPasswordEmailClick}>
              {this.props.statusDetails.getIn(['progress', 'forgotPassword']) ? 'Sending Email...' : 'Reset Password'}
            </div>
          </div>
          <div className='links'>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/login'})}>
              Login
            </div>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/register'})}>
              New User?
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }

  handleSendForgotPasswordEmailClick() {
    this.props.forgotPassword({email: this.props.loginDetails.get('email')});
  }

  handleFieldUpdate(attrs, value) {
    this.props.updateLoginDetails(attrs, value);
  }
}

function mapStateToProps(state) {
  const { loginDetails, statusDetails } = state;

  return {
    state,
    loginDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  forgotPassword,
  updateLoginDetails,
  push
})(ForgotPasswordPage);