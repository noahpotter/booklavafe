import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import DiscussionPage from '../components/DiscussionPage';
import { fetchClubDiscussion, fetchClubDiscussionThreads, createClubDiscussionThread, fetchPosts } from '../actions';
import classNames from 'classnames';
import Textarea from '../components/Textarea';

// import '../../styles/modules/discussion';

class ClubDiscussionPage extends Component {

  constructor(props) {
    super(props);

    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails() { 
    var discussion = this.props.entities.getIn(['clubDiscussions', this.props.params.id.toString()]);
    var club = Immutable.fromJS({});
    if (discussion) {
      club = this.props.entities.getIn(['clubs', discussion.get('club').toString()]);
    }

    return (
      <div className='club-book-discussion-details-area'>
        <div className='discussion-details-title'>
          Discussion Details
        </div>
        <div className='club-book-discussion-details'>
          <div className='discussion-detail'>
            <div className='discussion-detail-field'>
              Club
            </div>
            <div className='discussion-detail-value'>
              {club.get('name')}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    var discussion = this.props.entities.getIn(['clubDiscussions', this.props.params.id.toString()]) || Immutable.fromJS({});

    return (
        <DiscussionPage
          discussionType='clubDiscussion'
          discussionId={this.props.params.id}
          createThreadFunc={this.props.createClubDiscussionThread}
          fetchDiscussions={this.props.fetchClubDiscussion.bind(this, {clubDiscussionId: this.props.params.id})}
          fetchDiscussionThreads={this.props.fetchClubDiscussionThreads.bind(this, this.props.params.id)}
          renderDetails={this.renderDetails}
          parentId={discussion.get('club')}
        />
    );
  }
}

function mapStateToProps(state) {
  const { entities, discussionDetails, loggedUserDetails, interactionDetails, postDetails } = state;

  return {
    entities,
    discussionDetails,
    loggedUserDetails,
    interactionDetails,
    postDetails
  };
}

export default connect(mapStateToProps, {
  fetchClubDiscussion,
  fetchClubDiscussionThreads,
  createClubDiscussionThread,
  fetchPosts,
})(ClubDiscussionPage);
