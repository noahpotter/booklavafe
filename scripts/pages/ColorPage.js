import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';

// import '../../styles/modules/colors_page';

export default class ColorPage extends Component {
  render() {
    var colors = [
      'lighter-grey',
      'light-grey',
      'grey',
      'dark-grey',
      'darker-grey',
    ]

    var colorComponents = [];
    for(var i=0; i<colors.length; i++) {
      colorComponents.push(
        <div className={'color-box ' + colors[i]}>
          {colors[i]}
        </div>
      );
    }

    return (
      <DocumentTitle title={'Colors Page'}>
        <div>
          {colorComponents}
        </div>
      </DocumentTitle>
    );
  }
}
