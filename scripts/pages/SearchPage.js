import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push, replace } from 'react-router-redux';
import { search, editSearchQuery, selectSearchResult, changeSearchType,
         joinClub, fetchClubs, setPage,
         showAddBookToClub, cancelAddBookToClub, submitAddBookToClub,
         selectBook, selectClub, updateEndDate, updateStartDate, setErrorStatus  } from '../actions';
import Input from '../components/Input';
import LoadingAnimation from '../components/LoadingAnimation';
import ExpandableText from '../components/ExpandableText';
import classNames from 'classnames';
import Immutable from 'immutable';
import Popover from '../components/Popover';
import PaginationControls from '../components/PaginationControls';
import { getLoggedUser, dateToMoment, isCurrentlyReadingBook, getBookFromQuery, sortAscAlpha, hasNewSearch, userHasOwnedClubs } from '../utils';
window.Immutable = Immutable;

// import '../../styles/modules/search';
// import '../../styles/modules/input';

class SearchPage extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);

    this.renderBookSearchItem = this.renderBookSearchItem.bind(this);
    this.renderClubSearchItem = this.renderClubSearchItem.bind(this);
    this.renderPersonSearchItem = this.renderPersonSearchItem.bind(this);
    this.renderSelectedBook = this.renderSelectedBook.bind(this);
    this.renderSelectedClub = this.renderSelectedClub.bind(this);
    this.renderSelectedPerson = this.renderSelectedPerson.bind(this);
    this.findClubsReadingBook = this.findClubsReadingBook.bind(this);
    this.search = this.search.bind(this);
    this.changePage = this.changePage.bind(this);
    this.submitAddBookToClub = this.submitAddBookToClub.bind(this);
    this.push = this.push.bind(this);
    this.replaceUrlState = this.replaceUrlState.bind(this);
  }

  componentWillMount() {
    this.fetchClubs();

    const {type, query, page} = this.props.query;

    var clubId = this.props.query.club
    if (clubId) {
      this.props.selectClub(clubId);
    }

    if (this.props.pathname == '/search' || this.props.pathname == '/search/') {
      if (this.isMissingQuery(this.props.query)) {
        var newQuery = this.getUrlQuery(this.props.query);
        this.props.setPage('search', newQuery.page);
        this.props.editSearchQuery(newQuery.query);
        this.props.changeSearchType(newQuery.type);
        this.replaceUrlState('/search', newQuery)
      } else {
        this.props.setPage('search', page);
        this.props.editSearchQuery(query);
        this.props.changeSearchType(type);
      }
      
      console.log('set query', query);
      this.search();
    }
  }

  componentWillUpdate(newProps) {
    // const {type, query, page} = newProps.router.location.query;

    // if (newProps.router.routes[newProps.router.routes.length - 1].name == 'search') {
    //   if (this.isMissingQuery(newProps.router.location.query)) {
    //     var newQuery = this.getUrlQuery(newProps.router.location.query, props);
    //     this.props.setPage('search', newQuery.page);
    //     this.props.editSearchQuery(newQuery.query);
    //     this.props.changeSearchType(newQuery.type);
    //     this.replaceUrlState(null, '/search', newQuery)
    //   }
    // }

    const {type, query, page} = newProps.query;

    var clubId = newProps.query.club
    if (clubId) {
      this.props.selectClub(clubId);
    }

    if (newProps.pathname == '/search' || newProps.pathname == '/search/') {
      if (this.isMissingQuery(newProps.query)) {
        var newQuery = this.getUrlQuery(newProps.query);
        this.props.setPage('search', newQuery.page);
        this.props.editSearchQuery(newQuery.query);
        this.props.changeSearchType(newQuery.type);
        this.replaceUrlState('/search', newQuery)
      }
      //  else {
      //   this.props.setPage('search', page);
      //   this.props.editSearchQuery(query);
      //   this.props.changeSearchType(type);
      // }
      
      // this.search();
    }
  }

  getUrlQuery(updateQuery, props = null) {
    var newQuery;
    if (!props) {
      newQuery = this.props.query;
    } else {
      newQuery = props.query;
    }

    if (!updateQuery) {
      updateQuery = {};
    }

    newQuery.type = updateQuery.type || newQuery.type || 'books';
    newQuery.query = updateQuery.query || newQuery.query || '';
    newQuery.page = updateQuery.page || newQuery.page || '1';
    return newQuery;
  }

  // componentDidUpdate(prevProps) {
  //   // if (!this.props.loggedUserDetails.get('id') && nextProps.loggedUserDetails.get('id')) {
  //   //   this.fetchClubs();
  //   // }
  //   const {type, query, page} = this.props.query;
  //   const {type: prevType, query: prevQuery, page: prevPage} = prevProps.query;

  //   if (type != prevType || query != prevQuery || page != prevPage) {
  //     this.search(type, query, page);
  //   }
  // }

  isMissingQuery(urlQuery) {
    const {type, query, page} = urlQuery;
    return (
      type == undefined || 
      type == '' || 
      type == null || 
      query == undefined || 
      query == null || 
      page == undefined || 
      page == '' ||
      page == null
    );
  } 

  push(pathname, query) {
    this.props.push({pathname, query: this.getUrlQuery(query)});
    this.search();
  }

  replaceUrlState(pathname, query) {
    this.props.replace({pathname, query: this.getUrlQuery(query)});
    this.search();
  }

  fetchClubs() {
    var userId = this.props.loggedUserDetails.get('id')
    if (userId) {
      this.props.fetchClubs(userId)
    }
  }

  search() {
    if (hasNewSearch()) {
      this.props.search();
    }
  }

  changePage(page) {
    this.props.setPage('search', page);
    this.push('/search', this.getUrlQuery({page}));
  }

  changeSearchType(type) {
    this.props.changeSearchType(type);
    this.push('/search', this.getUrlQuery({type}));
  }

  submitAddBookToClub(clubId) {
    if (clubId) {
      this.props.submitAddBookToClub();
    } else {
      this.props.setErrorStatus('selectClub', 'Please select a club.');
    }
  }

  findClubsReadingBook(book) {
    this.props.changeSearchType('clubs');
    this.push('/search', {query: 'book: ' + book.get('name')})
    // this.props.changeSearchType('clubs');
    // this.props.editSearchQuery('book: ' + book.get('name'))
    // this.props.search();
  }

  renderBookSearchItem(book) {
    return (
      <div key={'book' + book.get('id')} className='search-result book' onClick={this.props.selectSearchResult.bind(this, book.get('id'))}>
        <div className='details'>
          <div className='title'>
            {book.get('name')}
          </div>
          <div className='author'>
            {book.get('author')}
          </div>
        </div>
        <div className='actions'>
        </div>
      </div>
    );
  }

  renderClubSearchItem(club) {
    const isCurrentlyReading  = isCurrentlyReadingBook(club, this.props.entities, this.props.searchDetails.get('query'))
    const readingBook = getBookFromQuery(this.props.searchDetails.get('query'));

    var indicators = null;

    if (isCurrentlyReading) {
      indicators = 
        <div className='indicators'>
          <Popover
            activateOnHover={true}
            body={`${club.get('name')} is reading ${readingBook} right now!`}
          >
            <div className={classNames('indicator',  {'is-reading-book': isCurrentlyReading})} />
          </Popover>
        </div>
    }

    return (
      <div key={'club' + club.get('id')} className='search-result club' onClick={this.props.selectSearchResult.bind(this, club.get('id'))}>
        <div className='details'>
          <div className='name'>
            {club.get('name')}
          </div>
        </div>
        {indicators}
        <div className='actions'>
        </div>
      </div>
    );
  }

  renderPersonSearchItem(person) {
    return (
      <div key={'user' + person.get('id')} className='search-result person' onClick={this.props.selectSearchResult.bind(this, person.get('id'))}>
        <div className='details'>
          <div className='username'>
            {person.get('username')}
          </div>
        </div>
        <div className='actions'>
        </div>
      </div>
    );
  }

  renderSelectedBook(book = Immutable.fromJS({})) {
    var loggedUser = getLoggedUser();
    var clubs = Immutable.fromJS({});
    if (loggedUser) {
      clubs = this.props.entities.get('clubs').filter(club => club.get('president') == loggedUser.get('id'))
    }
    var selectedClubId = this.props.addBookDetails.get('clubId');
    var clubSelectOptions = [];
    sortAscAlpha(clubs, 'name').forEach(club => {
      clubSelectOptions.push(
        <div key={club.get('id')} className={classNames('select-option', {'active': club.get('id') == selectedClubId})} onClick={this.props.selectClub.bind(this, club.get('id'))}>
          {club.get('name')}
        </div>
      );
    })

    var selectForClubButton = null;
    var selectForClubForm = null;

    var bookDetails = null;

    if (book.get('id')) {
      if (userHasOwnedClubs(getLoggedUser())) {
        if (this.props.addBookDetails.get('showAddBookToClub')) {
          selectForClubForm =
            <div className={classNames('select-for-club-area', {'form-disabled': this.props.statusDetails.getIn(['progress', 'addBook'])})}>
              <div className={classNames('select-club-input', {'error': this.props.statusDetails.getIn(['errors', 'selectClub'])})}>
                <div className={classNames('validation', {'error': this.props.statusDetails.getIn(['errors', 'selectClub'])})}>
                  <Popover
                    activateOnHover={true}
                    body={this.props.statusDetails.getIn(['errors', 'selectClub'])}
                  >
                    <div className='popover-info popover-validation' />
                  </Popover>
                </div>
                <div className='select-for-club-options'>
                  {clubSelectOptions}
                </div>
              </div>
              <div className='select-for-club-area-right-side'>
                <Input 
                  className='select-start-date-input' 
                  value={this.props.addBookDetails.get('startDate')} 
                  onChange={this.props.updateStartDate}
                  errors={this.props.statusDetails.getIn(['errors', 'startDate'])} 
                  placeholder={'MM/DD/YYYY'}
                  label='Start Date'
                  inverted={true}
                  divider={true}
                  rowType={true} />
                <Input 
                  className='select-end-date-input' 
                  value={this.props.addBookDetails.get('endDate')} 
                  onChange={this.props.updateEndDate}
                  errors={this.props.statusDetails.getIn(['errors', 'endDate'])} 
                  placeholder={'MM/DD/YYYY'} 
                  label='End Date' 
                  inverted={true}
                  divider={true}
                  rowType={true} />
                <div className='spacer' />
                <div className='select-actions actions row fill-width fill-height'>
                  <div className='action inverted cancel-button' onClick={this.props.cancelAddBookToClub}>
                    Cancel
                  </div>
                  <div className='action submit-button' onClick={this.submitAddBookToClub.bind(this, selectedClubId)}>
                    Select
                  </div>
                </div>
              </div>
            </div>
        } else {
          selectForClubButton =
            <div className='primary-button' onClick={this.props.showAddBookToClub}>
              Select for club
            </div>
        }
      }

      bookDetails =
        <div className='search-selected-details book'>
          <div className='details'>
            <div className='title-area'>
              <div className='book-cover'>
                <img className='pic' src={book.get('coverImage')} />
              </div>
              <div className='book-meta'>
                <div className='title'>
                  <ExpandableText
                    height={125}
                    fontSize={26}
                  >
                    {book.get('name')}
                  </ExpandableText>
                </div>
                <div className='author'>
                  {book.get('author')}
                </div>
              </div>
            </div>
            <div className='amazon-link'>
              <a href={book.get('amazonLink')} target='blank'>Find on Amazon</a>
            </div>
            <div className='description' dangerouslySetInnerHTML={{__html: book.get('description')}} />
          </div>
          <div className='actions col'>
            <div className='action primary-button' onClick={this.findClubsReadingBook.bind(this, book)}>
              Find clubs reading this
            </div>
            {selectForClubButton}
            {selectForClubForm}
          </div>
        </div>
    }

    return (
      bookDetails
    );
  }

  renderSelectedClub(club = Immutable.fromJS({})) {
    var clubDetails = null;

    if (club.get('id')) {
      clubDetails =
        <div className='search-selected-details club'>
          <div className='details'>
            <div className='name'>
              {club.get('name')}
            </div>
            <div className='description'>
              {club.get('description')}
            </div>
          </div>
          <div className='actions col'>
            <div className='action primary-button' onClick={this.push.bind(this, `/clubs/${club.get('id')}/`)}>
              View homepage
            </div>
            <div className='action primary-button' onClick={this.props.joinClub.bind(this, club.get('id'))}>
              Join this club
            </div>
          </div>
        </div>
    }

    return (
      clubDetails
    );
  }

  renderSelectedPerson(person = Immutable.fromJS({})) {
    return (
      <div className='search-selected-details person'>
        <div className='details'>
          <div className='username'>
            {person.get('username')}
          </div>
        </div>
        <div className='actions'>
          <div className='action primary-button'>
            Profile
          </div>
          <div className='action primary-button'>
            Befriend
          </div>
        </div>
      </div>
    );
  }

  render() {
    function filterItems(items, ids) {
      return (
        items.filter(item => ids.includes(item.get('id')))
      );
    }

    var typeHash = {
      'books': {
        items: filterItems(this.props.entities.get('books'), this.props.searchDetails.get('books')),
        renderItemFunc: this.renderBookSearchItem,
        renderSelectedFunc: this.renderSelectedBook
      },
      'clubs': {
        items: filterItems(this.props.entities.get('clubs'), this.props.searchDetails.get('clubs')),
        renderItemFunc: this.renderClubSearchItem,
        renderSelectedFunc: this.renderSelectedClub
      },
      'users': {
        items: filterItems(this.props.entities.get('users'), this.props.searchDetails.get('users')),
        renderItemFunc: this.renderPersonSearchItem,
        renderSelectedFunc: this.renderSelectedPerson
      }
    } 

    var type = this.props.searchDetails.get('type');
    var query = this.props.searchDetails.get('query');
    var selectedTypeHash = typeHash[type];
    var searchResults;
    var selectedSearchResult;

    if (this.props.searchDetails.get('isSearching')) {
      searchResults = <LoadingAnimation />;
    } else if (selectedTypeHash) {
      if (selectedTypeHash.items.count() > 0) {
        searchResults = selectedTypeHash.items.map(item => {
          return selectedTypeHash.renderItemFunc(item);
        }).toArray();

        selectedSearchResult = selectedTypeHash.renderSelectedFunc(selectedTypeHash.items.find(item => item.get('id') == this.props.searchDetails.get('selectedId')));
      } else {
        searchResults = 
          <div className='no-search-results-found-message'>
            No results found.
          </div>
      }
    }

    var firstPageFunc, previousPageFunc, nextPageFunc;
    var currentPage = this.props.pageDetails.getIn(['page', 'search']);
    var hasMorePages = this.props.pageDetails.getIn(['hasMorePages', 'search']);
    if (currentPage > 1) {
      firstPageFunc = this.changePage.bind(this, 1);
      previousPageFunc = this.changePage.bind(this, this.props.pageDetails.getIn(['page', 'search']) - 1);
    }
    if (hasMorePages) {
      nextPageFunc = this.changePage.bind(this, this.props.pageDetails.getIn(['page', 'search']) + 1);
    }

    return (
      <DocumentTitle title={'Search'}>
        <div className='page'>
          <div className='search-area'>
            <div className='search-types'>
              <div className={classNames('search-type', {active: type == 'books'})} onClick={this.changeSearchType.bind(this, 'books')}>
                Book
              </div>
              <div className={classNames('search-type', {active: type == 'clubs'})} onClick={this.changeSearchType.bind(this, 'clubs')}>
                Club
              </div>
              {/*<div className={classNames('search-type', {active: type == 'users'})} onClick={this.props.changeSearchType.bind(this, 'users')}>
                Person
              </div>*/}
            </div>
            <div className='search-input'>
              <Input 
                type='text' 
                name='search' 
                placeholder='Search' 
                value={query}
                fieldName='Search'
                focusOnMount={true}
                onKeyUp={this.handleKeyUp.bind(this, this.push.bind(this, '/search', {type, query}))}
                onChange={this.props.editSearchQuery} />
              <div className='submit-query button' onClick={this.push.bind(this, '/search', {type, query})}>
                Go
              </div>
            </div>
            <div className='search-results'>
              {searchResults}
            </div>
            <div className='search-pagination-controls'>
              <PaginationControls
                onFirstPageClick={firstPageFunc}
                onPreviousPageClick={previousPageFunc}
                onNextPageClick={nextPageFunc}
              />
            </div>
          </div>
          <div className='search-selected-details-container'>
            {selectedSearchResult}
          </div>
        </div>
      </DocumentTitle>
    );
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }
}

function mapStateToProps(state, ownProps) {
  const { entities, routing, searchDetails, addBookDetails, loggedUserDetails, pageDetails, statusDetails } = state;

  return {
    entities,
    searchDetails,
    pageDetails,
    addBookDetails,
    loggedUserDetails,
    statusDetails,
    routing,
    query: ownProps.location.query,
    pathname: ownProps.location.pathname
  };
}

export default connect(mapStateToProps, {
  push,
  replace,
  search,
  setErrorStatus,
  selectSearchResult,
  changeSearchType,
  editSearchQuery,
  joinClub,
  showAddBookToClub,
  cancelAddBookToClub,
  submitAddBookToClub,
  selectBook,
  selectClub,
  updateEndDate,
  updateStartDate,
  setPage,
  fetchClubs
})(SearchPage);