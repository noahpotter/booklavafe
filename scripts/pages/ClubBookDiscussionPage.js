import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import DiscussionPage from '../components/DiscussionPage';
import { fetchClubBookDiscussion, fetchClubBookDiscussionThreads, createClubBookDiscussionThread, fetchPosts } from '../actions';
import classNames from 'classnames';
import Textarea from '../components/Textarea';
import Popover from '../components/Popover';
import Immutable from 'immutable';
import { discussionDateTime } from '../utils';

// import '../../styles/modules/discussion';

class ClubBookDiscussionPage extends Component {
  
  constructor(props) {
    super(props);

    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails() { 
    var discussion = this.props.entities.getIn(['clubBookDiscussions', this.props.params.id.toString()]);
    var clubBook = Immutable.fromJS({});
    var book = Immutable.fromJS({});
    if (discussion) {
      clubBook = this.props.entities.getIn(['clubBooks', discussion.get('clubBook').toString()]);
      if (clubBook) {
        book = this.props.entities.getIn(['books', clubBook.get('book').toString()]);
        if (book) {
        }
      }
    }

    return (
      <div className='club-book-discussion-details-area'>
        <div className='discussion-details-title'>
          Discussion Details
        </div>
        <div className='club-book-discussion-details'>
          <div className='discussion-detail'>
            <div className='discussion-detail-field'>
              Book
            </div>
            <div className='discussion-detail-value'>
              {book.get('name')}
            </div>
          </div>
          <div className='discussion-detail'>
            <div className='discussion-detail-field'>
              Chapters
            </div>
            <div className='discussion-detail-value'>
              {discussion.get('startChapter')} - {discussion.get('endChapter')}
            </div>
          </div>
          <div className='discussion-detail'>
            <div className='discussion-detail-field'>
              Date
              <Popover activateOnHover={true} body={'The time the discussion opened.'}>
                <div className='popover-info' />
              </Popover>
            </div>
            <div className='discussion-detail-value'>
              {discussionDateTime(discussion.get('startDate'))}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    var discussion = this.props.entities.getIn(['clubBookDiscussions', this.props.params.id.toString()]);
    var clubBook = Immutable.fromJS({});
    if (discussion) {
      clubBook = this.props.entities.getIn(['clubBooks', discussion.get('clubBook').toString()]);
    }

    return (
      <DiscussionPage
        discussionType='clubBookDiscussion'
        discussionId={this.props.params.id}
        createThreadFunc={this.props.createClubBookDiscussionThread}
        fetchDiscussions={this.props.fetchClubBookDiscussion.bind(this, {clubBookDiscussionId: this.props.params.id}, {'clubBook': true, 'book': true})}
        fetchDiscussionThreads={this.props.fetchClubBookDiscussionThreads.bind(this, this.props.params.id)}
        renderDetails={this.renderDetails}
        parentId={clubBook.get('club')}
      />
    );
  }
}

function mapStateToProps(state) {
  const { entities, discussionDetails, loggedUserDetails, interactionDetails, postDetails } = state;

  return {
    entities,
    discussionDetails,
    loggedUserDetails,
    interactionDetails,
    postDetails
  };
}

export default connect(mapStateToProps, {
  fetchClubBookDiscussion,
  fetchClubBookDiscussionThreads,
  createClubBookDiscussionThread,
  fetchPosts,
})(ClubBookDiscussionPage);
