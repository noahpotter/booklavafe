import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { resetPassword, updateLoginDetails } from '../actions';
import classNames from 'classnames';
import Input from '../components/Input';

class ResetPasswordPage extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleResetPasswordClick = this.handleResetPasswordClick.bind(this);
    this.handleFieldUpdate = this.handleFieldUpdate.bind(this);
  }

  render() {
    return (
      <DocumentTitle title={'Reset Password'}>
        <div className={classNames('page', {'form-disabled': this.props.statusDetails.getIn(['progress', 'resetPassword'])})}>
          <div className='header'>
            Booklava
          </div>
          <div className='body'>
            <div className='input-row'>
              <Input
                type='password' 
                name='password' 
                placeholder='New Password' 
                errors={this.props.statusDetails.getIn(['errors', 'password'])}
                value={this.props.loginDetails.get('password')}
                fieldName='Password'
                focusOnMount={true}
                onKeyUp={this.handleKeyUp.bind(this, this.handleResetPasswordClick)}
                onChange={this.handleFieldUpdate.bind(this, ['password'])} />
            </div>
            <div className='input-row'>
              <Input
                type='password' 
                name='passwordConfirmation' 
                placeholder='Password Confirmation' 
                errors={this.props.statusDetails.getIn(['errors', 'passwordConfirmation'])}
                value={this.props.loginDetails.get('passwordConfirmation')}
                fieldName='Password'
                onKeyUp={this.handleKeyUp.bind(this, this.handleResetPasswordClick)}
                onChange={this.handleFieldUpdate.bind(this, ['passwordConfirmation'])} />
            </div>
          </div>
          <div className='footer'>
            <div className='submit-button' onClick={this.handleResetPasswordClick}>
              {this.props.statusDetails.getIn(['progress', 'forgotPassword']) ? 'Sending Email...' : 'Reset Password'}
            </div>
          </div>
          <div className='links'>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/login'})}>
              Login
            </div>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/register'})}>
              New User?
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }

  handleResetPasswordClick() {
    this.props.resetPassword(this.props.state.routing.location.query.token);
  }

  handleFieldUpdate(attrs, value) {
    this.props.updateLoginDetails(attrs, value);
  }
}

function mapStateToProps(state) {
  const { loginDetails, statusDetails } = state;

  return {
    state,
    loginDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  resetPassword,
  updateLoginDetails,
  push
})(ResetPasswordPage);