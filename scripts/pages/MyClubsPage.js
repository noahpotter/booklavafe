import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { changeMonth, 
         startCreatingClub, cancelCreatingClub, editCreatingClub, createClub,
         startEditingClub, cancelEditingClub, editEditingClub, editClub,
         fetchClubs, deleteClub, unsubscribeClub, resetErrorStatus, resetProgressStatus,
         activate, deactivate } from '../actions';
import Input from '../components/Input';
import Textarea from '../components/Textarea';
import classNames from 'classnames';
import moment from 'moment';
import { push } from 'react-router-redux';
import { getLoggedUser, sortAscAlpha, getActiveEntity } from '../utils';
import Dropzone from '../components/Dropzone';
import Popover from '../components/Popover';
import ToggleButton from '../components/ToggleButton';

// import '../../styles/modules/clubs';
// import '../../styles/modules/calendar';

class MyClubsPage extends Component {

  constructor(props) {
    super(props);

    this.renderCalendar = this.renderCalendar.bind(this);
    this.fetchClubs = this.fetchClubs.bind(this);
    this.deleteClub = this.deleteClub.bind(this);
    this.unsubscribeClub = this.unsubscribeClub.bind(this);
    this.startCreatingClub = this.startCreatingClub.bind(this);
    this.startEditingClub = this.startEditingClub.bind(this);
  }

  componentWillMount() {
    this.fetchClubs();
  }

  componentWillUpdate(nextProps) {
    if (!this.props.loggedUserDetails.get('id') && nextProps.loggedUserDetails.get('id')) {
      this.fetchClubs();
    }
  }

  fetchClubs() {
    var userId = this.props.loggedUserDetails.get('id')
    if (userId) {
      this.props.fetchClubs(userId, {'clubBooks': true, 'clubBookDiscussions': true})
    }
  }

  startEditingClub(club) {
    this.props.resetErrorStatus();
    this.props.startEditingClub(club.toJS());
  }

  startCreatingClub() {
    this.props.resetErrorStatus();
    this.props.startCreatingClub();
  }

  deleteClub(club) {
    if (confirm(`Delete '${club.get('name')}'?`)) {
      this.props.deleteClub(club.get('id'));
    }
  }

  unsubscribeClub(club) {
    if (confirm(`Unsubscribe from '${club.get('name')}'?`)) {
      this.props.unsubscribeClub(club.get('id'));
    }
  }

  renderClubForm(club, editClub, cancelClub, executeClub) {
    var existingFileUrls = [];
    var image = club.get('image');

    if (typeof image == 'string') {
      existingFileUrls = [image];
    }

    var uploadClubImageInput = null;

    if (club.get('useClubImage')) {
      uploadClubImageInput =
        <Dropzone
          key={'upload-image-input'+club.get('id')}
          inverted={true}
          rowType={true}
          label='Image'
          placeholder='Drag or click here.'
          onChange={editClub.bind(this, 'image')}
          existingFileUrls={existingFileUrls}
          infoText='This image is displayed on the club homepage.'
         />
    }

    return [
      <Input 
        key={'name-input'+club.get('id')}
        inverted={true} 
        rowType={true} 
        label='Name' 
        value={club.get('name')} 
        focusOnMount={true}
        errors={this.props.statusDetails.getIn(['errors', 'name'])}
        onChange={editClub.bind(this, 'name')} 
        placeholder='...' 
        />,
      <Textarea 
        key={'description-input'+club.get('id')}
        inverted={true}
        colType={true} 
        label='Description' 
        maxHeight={300}
        value={club.get('description')} 
        errors={this.props.statusDetails.getIn(['errors', 'description'])}
        onChange={editClub.bind(this, 'description')} 
        placeholder='...'
        autosize={true}
        divider={true}
        minRows={4}
        />,
      <ToggleButton
        key={'club-image-input'+club.get('id')}
        rowType={true}
        label='Use Current Book Image'
        divider={true}
        onChange={editClub.bind(this, 'useClubImage')}
        oppositeValue={true}
        value={club.get('useClubImage')}
      />,
      uploadClubImageInput,
      <div key={'form-actions' +club.get('id')} className='form-actions'>
        <div className='form-action button grey inverted' onClick={cancelClub} >
          Cancel
        </div>
        <div className='form-action button primary' onClick={executeClub}>
          Save
        </div>
      </div>
    ];
  }

  renderDay(day) {
    var active = day.month() == this.props.calendarDetails.get('currentMonth')
    var discussions = this.props.entities.get('clubBookDiscussions').filter(discussion => moment(discussion.get('startDate')).isSame(day, 'day'));

    var eventComponents = discussions.map(discussion => {

      var clubBook = this.props.entities.getIn(['clubBooks', discussion.get('clubBook').toString()]);
      var club = this.props.entities.getIn(['clubs', clubBook.get('club').toString()]);
      var activeClub = getActiveEntity(this.props.entities, this.props.interactionDetails, 'club');

      if (club) {
        return (
          <div key={discussion.get('id')} className={classNames('calendar-day-event', {'active': activeClub && activeClub.get('id') == club.get('id')})} onMouseEnter={this.props.activate.bind(this, {'club': club.get('id'), 'clubBook': clubBook.get('id'), 'clubBookDiscussion': discussion.get('id')})} onClick={this.props.push.bind(this, {pathname: `/club_book_discussions/${discussion.get('id')}`})}>
          </div>
        );
      }

    }).toArray()

    return (
      <div key={day.date()} className={classNames('calendar-day-box', {'active': active, 'today': moment().isSame(day, 'day')})}>
        <div className='calendar-day'>
          {day.date()}
        </div>
        <div className='calendar-day-events'>
          {eventComponents}
        </div>
      </div>
    );
  }

  renderCalendar() {
    var currentMonth = moment().month(this.props.calendarDetails.get('currentMonth'))
    var startDay = moment(currentMonth).startOf('month').startOf('week');

    var weekComponents = [];

    for(var week=0; week<5; week++) {
      var dayComponents = [];

      for(var day=0; day<7; day++) {
        dayComponents.push(this.renderDay(startDay));
        startDay.add(1, 'day');
      }

      weekComponents.push(
        <div key={week} className='calendar-week'>
          {dayComponents}
        </div>
      );
    }

    return (
      <div className='calendar-container'>
        <div className='calendar'>
          <div className='calendar-header'>
            <div className='previous-month' onClick={this.props.changeMonth.bind(this, -1)} />
            <div className='current-month'>
              {currentMonth.format('MMMM')}
            </div>
            <div className='next-month' onClick={this.props.changeMonth.bind(this, 1)} />
          </div>
          <div className='calendar-body'>
            {weekComponents}
          </div>
        </div>
      </div>
    );
  }

  renderActiveDiscussion() {
    var activeClubBookDiscussion = getActiveEntity(this.props.entities, this.props.interactionDetails, 'clubBookDiscussion');
    if (activeClubBookDiscussion) {
      var discussion = this.props.entities.getIn(['clubBookDiscussions', activeClubBookDiscussion.get('id').toString()]);
      if (discussion) {
        var clubBook = this.props.entities.getIn(['clubBooks', discussion.get('clubBook').toString()]);
        var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]);
        var chaptersText = `Chapters ${discussion.get('startChapter')}-${discussion.get('endChapter')}`;

        return( 
          <div className='active-discussion-details'>
            <div className='active-discussion-text'>
              <div className='discussion-details-title' onClick={this.props.push.bind(this, {pathname: `/club_book_discussions/${discussion.get('id')}`})} >
                {book.get('name')}
              </div>
              <div className='date'>
                {moment(discussion.get('startDate')).format('M/D/YYYY - h:mm A')}
              </div>
              <div className='chapters'>
                {chaptersText}
                <Popover isOpen={true} activateOnHover={true} body={`Read to the end of chapter ${discussion.get('endChapter')}.`}>
                  <div className='popover-info' />
                </Popover>
              </div>
            </div>
            <div className='active-discussion-actions'>
            </div>
          </div>
        );
      }
    }
    return null;
  }

  renderOwnedClub(club) {
    var activeClub = getActiveEntity(this.props.entities, this.props.interactionDetails, 'club')
    var isActive = activeClub && activeClub.get('id') == club.get('id').toString();
    var editingClub = this.props.clubDetails.get('editingClub');
    var isEditingClub = editingClub ? editingClub.get('id') == club.get('id').toString() : false;
    var onMouseLeave = editingClub ? this.props.deactivate.bind(this, null) : null;

    var deleteIcon = null;
    if (!this.props.statusDetails.getIn(['progress', 'deleteClub'])) {
      deleteIcon =
        <div className='action icon remove-icon color-hover' onClick={this.deleteClub.bind(this, club)} />
    }

    return (
      <div key={club.get('id')} className={classNames('club', {'active': isActive || isEditingClub})} onMouseEnter={this.props.activate.bind(this, {'club': club.get('id')})} onMouseLeave={onMouseLeave} >
        <div className='club-name' onClick={this.props.push.bind(this, {pathname: `/clubs/${club.get('id')}/`})} >
          {club.get('name')}
        </div>
        <div className='club-actions actions'>
          <div className='action icon edit-icon color-hover' onClick={this.startEditingClub.bind(this, club)} />
          {deleteIcon}
        </div>
      </div>
    );
  }

  renderSubscribedClub(club) {
    var activeClub = getActiveEntity(this.props.entities, this.props.interactionDetails, 'club')
    var isActive = activeClub && activeClub.get('id') == club.get('id').toString();
    var editingClub = this.props.clubDetails.get('editingClub');
    var onMouseLeave = editingClub ? this.props.deactivate.bind(this, null) : null;

    var deleteIcon = null;
    if (this.props.statusDetails.getIn(['progress', 'unsubscribeClub']) != club.get('id')) {
      deleteIcon =
        <div className='action icon remove-icon color-hover' onClick={this.unsubscribeClub.bind(this, club)} />
    }

    return (
      <div key={club.get('id')} className={classNames('club', {'active': isActive})} onMouseEnter={this.props.activate.bind(this, {'club': club.get('id')})} onMouseLeave={onMouseLeave} >
        <div className='club-name' onClick={this.props.push.bind(this, {pathname: `/clubs/${club.get('id')}/`})} >
          {club.get('name')}
        </div>
        <div className='club-actions actions'>
          {deleteIcon}
        </div>
      </div>
    );
  }

  render() {
    var calendarArea = null,
        clubFormArea = null;

    if (this.props.clubDetails.get('creatingClub')) {
      clubFormArea = 
        <div className={classNames('create-club-area', {'form-disabled': this.props.statusDetails.getIn(['progress', 'createClub'])})}>
          <div className='title'>
            Create Club
          </div>
          {this.renderClubForm(this.props.clubDetails.get('creatingClub'), this.props.editCreatingClub, this.props.cancelCreatingClub, this.props.createClub)}
        </div>
    } else if (this.props.clubDetails.get('editingClub')) {
      clubFormArea =
        <div className={classNames('edit-club-area', {'form-disabled': this.props.statusDetails.getIn(['progress', 'editClub'])})}>
          <div className='title'>
            Edit Club
          </div>
          {this.renderClubForm(this.props.clubDetails.get('editingClub'), this.props.editEditingClub, this.props.cancelEditingClub, this.props.editClub)}
        </div>
    } else {
      calendarArea = 
        <div className='calendar-area'>
          {this.renderCalendar()}
          {this.renderActiveDiscussion()}
        </div>
    }

    const loggedUser = getLoggedUser();
    var ownedClubs,
          ownedClubsSection,
          subscribedClubs,
          subscribedClubsSection;

    if (loggedUser) {
      ownedClubs = this.props.entities.get('clubs').filter(club => loggedUser.get('ownedClubs').includes(club.get('id')))
      subscribedClubs = this.props.entities.get('clubs').filter(club => loggedUser.get('subscribedClubs').includes(club.get('id')) && !loggedUser.get('ownedClubs').includes(club.get('id')))

      ownedClubsSection = sortAscAlpha(ownedClubs, 'name').map(club => {
        return this.renderOwnedClub(club)
      }).toArray()

      subscribedClubsSection = sortAscAlpha(subscribedClubs, 'name').map(club => {
        return this.renderSubscribedClub(club)
      }).toArray()
    }

    return (
      <DocumentTitle title={'My Clubs'}>
        <div className='page'>
          <div className='clubs-area'>
            <div className='clubs-title'>
              <div className='title'>
                Owned Clubs
              </div>
              <div className='action-btn grey-button add icon-button' onClick={this.startCreatingClub} />
            </div>
            <div className='clubs'>
              {ownedClubsSection}
            </div>
            <div className='clubs-title'>
              <div className='title'>
                Subscribed Clubs
              </div>
            </div>
            <div className='clubs'>
              {subscribedClubsSection}
            </div>
          </div>
          {clubFormArea}
          {calendarArea}
        </div>
      </DocumentTitle>
    );
  }
}

function mapStateToProps(state) {
  const { entities, calendarDetails, clubDetails, loggedUserDetails, interactionDetails, statusDetails } = state;

  return {
    entities,
    calendarDetails,
    clubDetails,
    loggedUserDetails,
    interactionDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  changeMonth,
  startCreatingClub,
  cancelCreatingClub,
  editCreatingClub,
  createClub,
  startEditingClub,
  cancelEditingClub,
  editEditingClub,
  editClub,
  fetchClubs,
  deleteClub,
  unsubscribeClub,
  push,
  resetErrorStatus,
  resetProgressStatus,
  activate,
  deactivate
})(MyClubsPage);