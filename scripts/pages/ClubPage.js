import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { fetchClubBooks, fetchClub,
         startEditingClubBook, cancelEditingClubBook, editEditingClubBook, editClubBook,
         startCreatingClubBookDiscussion, cancelCreatingClubBookDiscussion, editCreatingClubBookDiscussion, createClubBookDiscussion, deleteClubBookDiscussion, startEditingClub,
         activate, deactivate, joinClub, deleteClubBook } from '../actions';
import Input from '../components/Input';
import ExpandableText from '../components/ExpandableText';
import classNames from 'classnames';
import { push } from 'react-router-redux';
import { getLoggedUser, getActiveEntity, addMinDistance, currentlyReadingBook, sortAscDate, sortDescDate, discussionDateTime, momentFormatDate } from '../utils'
import moment from 'moment';
import Popover from '../components/Popover';

// import '../../styles/modules/club';

import Immutable from 'immutable';
window.Immutable = Immutable;

function getAssociatedItems(items, field, id) {
  return items.filter(item => item.get(field) == id); 
}

class ClubPage extends Component {

  constructor(props) {
    super(props);

    this.renderGuestPage = this.renderGuestPage.bind(this);
    this.renderNonMemberPage = this.renderNonMemberPage.bind(this);
    this.renderMemberPage = this.renderMemberPage.bind(this);
    this.renderTimeLineSegment = this.renderTimeLineSegment.bind(this);
    this.renderAddBookTimeLineSegment = this.renderAddBookTimeLineSegment.bind(this);
    this.renderClubDiscussionAction = this.renderClubDiscussionAction.bind(this);
    this.saveClubBook = this.saveClubBook.bind(this);
    this.renderDiscussions = this.renderDiscussions.bind(this);
    this.startEditingClub = this.startEditingClub.bind(this);
  }

  componentWillMount() {
    this.fetchClubBooksWithDiscussions(this.props);
    this.fetchClub(this.props);
  }

  componentWillUpdate(newProps) {
    if (this.props.params.id != newProps.params.id) {
      this.fetchClubBooksWithDiscussions(newProps);
      this.fetchClub(newProps);
    }
  }

  deleteClubBook(clubBook) {
    if (confirm('Delete this book?')) {
      this.props.deleteClubBook(clubBook.get('id'));
    }
  }

  deleteClubBookDiscussion(clubBookDiscussion) {
    if (confirm('Delete this discussion?')) {
      this.props.deleteClubBookDiscussion(clubBookDiscussion.get('id'));
    }
  }

  startEditingClub(club) {
    var user = getLoggedUser();
    this.props.push({pathname: `/users/${user.get('id')}/clubs`});
    this.props.startEditingClub(club);
  }

  fetchClubBooksWithDiscussions(props) {
    this.props.fetchClubBooks({clubId: props.params.id}, {books: true, clubBookDiscussions: true})
  }

  fetchClub(props) {
    this.props.fetchClub(props.params.id)
  }

  saveClubBook() {
    this.props.editClubBook();
  }

  renderClubDiscussionAction(club) {
    return (
      <Popover
        activateOnHover={true}
        body='Discuss the club'
        preferPlace='right'
      >
        <div className='pic-action grey-button discuss' onClick={this.props.push.bind(this, {pathname: `/club_discussions/${club.get('discussions').first()}`})} />
      </Popover>
    );
  }

  renderClubImage(club) {
    var image = null;
    if (club.get('useClubImage')) {
      image = club.get('image');
    } else {
      var clubBooks = this.props.entities.get('clubBooks').filter(clubBook => clubBook.get('club') == club.get('id'));

      clubBooks = sortAscDate(clubBooks, 'startDate');

      // Attempt to find to use the first club book image that is being read right now and has an image
      clubBooks.forEach(clubBook => {
        if (!image && currentlyReadingBook(clubBook)) {
          var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]);
          if (book) {
            image = book.get('coverImage');
          }
        }
      })
    }

    return (<img className='pic' src={image} />);
  }

  renderNoBooksForOthers() {
    return (
      <div className='no-books-notification'>
        This club has yet to add a book.
      </div>
    );
  }

  renderNoBooksForPresident() {
    return (
      <div className='no-books-notification'>
        <div className='add-book-link' onClick={this.props.push.bind(this, {pathname: '/search', query: {type: 'books', club: this.props.params.id}})}>Add a book</div>
        {' and it will appear here.'}
      </div>
    );
  }

  renderBookTitle(book) {
    return (
      <ExpandableText
        fontSize={16}
        height={36}
      >
        <div className='book-name'>
          {book.get('name')}
        </div>
      </ExpandableText>
    );
  }

  renderAmazonLink(book) {
    return (
      <a href={book.get('amazonLink')} target='_blank' className='book-amazon-link'>
        Find on Amazon
      </a>
    );
  }

  renderDiscussions(clubBook, discussions) {
    var entryComponents = [];
    var activeClubBook = getActiveEntity(this.props.entities, this.props.interactionDetails, 'clubBook');
    var activeDiscussion = getActiveEntity(this.props.entities, this.props.interactionDetails, 'clubBookDiscussion');
    var index = 1;
    var currentZIndex = 100;
    var zIndexModifier = 1;
    var clubBookStartDate = moment(clubBook.get('startDate'));
    var now = moment();
    var startDate = null;
    var endDate = moment(clubBook.get('endDate'));
    var activeDiscussionDetails = null;

    if (!activeClubBook || activeClubBook.get('id') != clubBook.get('id')) {
      activeDiscussion = null;
    }

    discussions = sortAscDate(discussions, 'startDate');

    if (discussions.size > 0) {
      var firstDiscussion = discussions.first();
      var firstDiscussionStartDate = moment(firstDiscussion.get('startDate'));

      if (clubBookStartDate.isBefore(firstDiscussionStartDate) && clubBookStartDate.isBefore(now)) {
        startDate = clubBookStartDate;
      } else if (firstDiscussionStartDate.isBefore(clubBookStartDate) && firstDiscussionStartDate.isBefore(now)) {
        startDate = firstDiscussionStartDate;
      } else {
        startDate = now;
      }
    } else {
      startDate = now;
    }

    var todayMarkerLeft = startDate.diff(moment()) / startDate.diff(endDate) * 100;

    var todayMarker = null;

    if (todayMarkerLeft < 100) {
      todayMarker = 
        <Popover
          activateOnHover={true}
          body='Today'
        >
          <div className='today-marker' style={{left: `${todayMarkerLeft}%`}} />
        </Popover>
    }

    var lefts = [];

    discussions.forEach(discussion => {
      var discussionStartDate = moment(discussion.get('startDate'));
      var ratio =  startDate.diff(discussionStartDate) / startDate.diff(endDate);
      lefts.push(ratio * 100);
    });

    lefts = addMinDistance(lefts, 5);
    var indexes = {};

    var i=0;
    discussions.forEach((discussion) => {
      var isActivateDiscussion = activeDiscussion && activeDiscussion.get('id') == discussion.get('id');

      entryComponents.push(
        <div key={'discussion'+discussion.get('id')} className={classNames('discussion-timeline-entry', {'active': isActivateDiscussion})} style={{left: `${lefts[i]}%`, zIndex: currentZIndex}} onMouseEnter={this.props.activate.bind(this, {'clubBook': clubBook.get('id'), 'clubBookDiscussion': discussion.get('id')})} onClick={this.props.push.bind(this, {pathname: `/club_book_discussions/${discussion.get('id')}`})} >
          {index}
        </div>
      );

      indexes[discussion.get('id')] = index;

      if (isActivateDiscussion) {
        zIndexModifier = -1;
      }

      currentZIndex += zIndexModifier;
      i++;
      index++;
    });

    if (activeDiscussion) {
      var chaptersText = `Chapters ${activeDiscussion.get('startChapter')}-${activeDiscussion.get('endChapter')}`;

      activeDiscussionDetails = 
        <div className='active-discussion-details'>
          <div className='active-discussion-text'>
            <div className='discussion-details-title' onClick={this.props.push.bind(this, {pathname: `/club_book_discussions/${activeDiscussion.get('id')}`})} >
              Discussion {indexes[activeDiscussion.get('id')]}
            </div>
            <div className='date'>

              {discussionDateTime(activeDiscussion.get('startDate'))}
            </div>
            <div className='chapters'>
              {chaptersText}
              <Popover isOpen={true} activateOnHover={true} body={`Read to the end of chapter ${activeDiscussion.get('endChapter')}.`}>
                <div className='popover-info' />
              </Popover>
            </div>
          </div>
          <div className='active-discussion-actions'>
            <div className='active-discussion-action delete' onClick={this.deleteClubBookDiscussion.bind(this, activeDiscussion)} />
          </div>
        </div>
    }

    return (
      <div key='disucssions-component' className='discussion-details'>
        <div className='discussion-timeline'>
          <div className='discussion-timeline-entry-cont'>
            {entryComponents}
            {todayMarker}
          </div>
        </div>
        {activeDiscussionDetails}
      </div>
    );
  }

  renderTimeLineSegment(clubBook) {
    var startDate = moment(clubBook.get('startDate'));
    var endDate = momentFormatDate(clubBook.get('endDate'));

    return(
      <div className='timeline-segment'>
        <Popover
          activateOnHover={true}
          body={<div><div>Book start date</div><div>End date: {endDate}</div></div>}
          preferPlace='right'
        >
          <div className='date-information'>
              <div className='date-month'>
                {startDate.utc().format('MMM')}
              </div>
              <div className='date-day'>
                {startDate.utc().format('D')}
              </div>
          </div>
        </Popover>
        <div className='timeline-line'>
        </div>
      </div>
    )
  }

  renderAddBookTimeLineSegment() {
    return(
      <div className='timeline-segment'>
        <div className='timeline-segment-circle' onClick={this.props.push.bind(this, {pathname: '/search', query: {type: 'books', club: this.props.params.id}})}>
          <div className='add-book-icon' />
        </div>
        <div className='timeline-line' />
      </div>
    )
  }

  renderGuestPage(club) {
    var clubBooks = sortDescDate(getAssociatedItems(this.props.entities.get('clubBooks'), 'club', club.get('id')), 'startDate');
    var bookEntries = [];
    if (clubBooks.size > 0) {
      bookEntries = clubBooks.map(clubBook => {
        var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]) || Immutable.fromJS({})

        return (
          <div key={'clubBook'+clubBook.get('id')} className='book-entry'>
            {this.renderTimeLineSegment(clubBook)}
            <div className='book-entry-details'>
              <div className='book-title-area'>
                {this.renderBookTitle(book)}
              </div>
              <div className='book-amazon-link-area'>
                {this.renderAmazonLink(book)}
              </div>
              <div className='book-details'>
                {book.get('description')}
              </div>
            </div>
          </div>
        );
      })
    } else {
      bookEntries = this.renderNoBooksForOthers();
    }

    return (
      <DocumentTitle title={'Club'}>
        <div className='page'>
          <div className='club-detail-area'>
            <div className='pic-area'>
              <div className='pic-background' />
              {this.renderClubImage(club)}
              <div className='pic-actions'>
              </div>
            </div>
            <div className='club-detail-name'>
              {club.get('name')}
            </div>
            <div className='club-detail-description'>
              {club.get('description')}
            </div>
          </div>
          <div className='club-book-history-area'>
            {bookEntries}
          </div>
        </div>
      </DocumentTitle>
    );
  }

  renderNonMemberPage(club) {
    var clubBooks =  sortDescDate(getAssociatedItems(this.props.entities.get('clubBooks'), 'club', club.get('id')), 'startDate');

    var bookEntries 
    if (clubBooks.size > 0) {
      bookEntries = clubBooks.map(clubBook => {
        var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]) || Immutable.fromJS({})

        return (
          <div key={'clubBook'+clubBook.get('id')} className='book-entry'>
            {this.renderTimeLineSegment(clubBook)}
            <div className='book-entry-details'>
              <div className='book-title-area'>
                {this.renderBookTitle(book)}
              </div>
              <div className='book-amazon-link-area'>
                {this.renderAmazonLink(book)}
              </div>
              <div className='book-details'>
                {book.get('description')}
              </div>
            </div>
          </div>
        );
      })
    } else {
      bookEntries = this.renderNoBooksForOthers();
    }

    return (
      <DocumentTitle title={'Club'}>
        <div className='page'>
          <div className='club-detail-area'>
            <div className='pic-area'>
              <div className='pic-background' />
              {this.renderClubImage(club)}
              <div className='pic-actions'>
                <Popover
                  activateOnHover={true}
                  body='Join'
                  preferPlace='right'
                >
                  <div className='pic-action add' onClick={this.props.joinClub.bind(this, club.get('id'))} />
                </Popover>
                {this.renderClubDiscussionAction(club)}
              </div>
            </div>
            <div className='club-detail-name'>
              {club.get('name')}
            </div>
            <div className='club-detail-description'>
              {club.get('description')}
            </div>
          </div>
          <div className='club-book-history-area'>
            {bookEntries}
          </div>
        </div>
      </DocumentTitle>
    );
  }

  renderMemberPage(club) {
    var bookEntries = [];

    var clubBooks = sortDescDate(getAssociatedItems(this.props.entities.get('clubBooks'), 'club', club.get('id')), 'startDate');
    if (clubBooks.size > 0) {
      clubBooks.forEach(clubBook => {

        var discussions = getAssociatedItems(this.props.entities.get('clubBookDiscussions'), 'clubBook', clubBook.get('id'))
        var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]) || Immutable.fromJS({});

        var discussionsElement = this.renderDiscussions(clubBook, discussions);

        bookEntries.push(
          <div key={'clubBook'+clubBook.get('id')} className='book-entry'>
            {this.renderTimeLineSegment(clubBook)}
            <div className='book-entry-details'>
              <div className='book-title-area'>
                {this.renderBookTitle(book)}
              </div>
              <div className='book-amazon-link-area'>
                {this.renderAmazonLink(book)}
              </div>
              {discussionsElement}
            </div>
          </div>
        );
      });
    } else {
      bookEntries = this.renderNoBooksForOthers();
    }

    return (
      <DocumentTitle title={'Club'}>
        <div className='page'>
          <div className='club-detail-area'>
            <div className='pic-area'>
              <div className='pic-background' />
              {this.renderClubImage(club)}
              <div className='pic-actions'>
                {this.renderClubDiscussionAction(club)}
              </div>
            </div>
            <div className='club-detail-name'>
              {club.get('name')}
            </div>
            <div className='club-detail-description'>
              {club.get('description')}
            </div>
          </div>
          <div className='club-book-history-area' >
            {bookEntries}
          </div>
        </div>
      </DocumentTitle>
    );
  }

  renderPresidentPage(club) {
    var bookEntries = [];
    var addNewBook = null;

    var clubBooks = sortDescDate(getAssociatedItems(this.props.entities.get('clubBooks'), 'club', club.get('id')), 'startDate');

    if (clubBooks.size > 0) {
      addNewBook = 
        <div key='add-book' className='add-book-entry'>
          {this.renderAddBookTimeLineSegment()}
          <div className='add-book-entry-text'>
          </div>
        </div>

      clubBooks.forEach(clubBook => {
        var book = this.props.entities.getIn(['books', clubBook.get('book').toString()]) || Immutable.fromJS({});
        var body = null;
        var editingClubBook = this.props.clubBookDetails.get('editingClubBook');
        var editClubBookDetailsButton = null;

        if (editingClubBook && editingClubBook.get('id') == clubBook.get('id')) {
          body = 
            <div className='edit-club-book'>
              <Input
                rowType={true}
                label='Start Date'
                errors={this.props.statusDetails.getIn(['errors', 'startDate'])}
                value={this.props.clubBookDetails.get('editingClubBook').get('startDate')}
                placeholder='MM/DD/YYYY'
                inverted={true}
                divider={true}
                onChange={this.props.editEditingClubBook.bind(this, 'startDate')} />
              <Input
                rowType={true}
                label='End Date'
                errors={this.props.statusDetails.getIn(['errors', 'endDate'])}
                value={this.props.clubBookDetails.get('editingClubBook').get('endDate')}
                placeholder='MM/DD/YYYY'
                inverted={true}
                divider={true}
                onChange={this.props.editEditingClubBook.bind(this, 'endDate')} />
              <div className='actions row'>
                <div className='action cancel-button inverted' onClick={this.props.cancelEditingClubBook}>
                  Cancel
                </div>
                <div className='action submit-button' onClick={this.saveClubBook}>
                  Save
                </div>
              </div>
            </div>

          editClubBookDetailsButton = 
            <span className='edit-club-book-details cancel' onClick={this.deleteClubBook.bind(this, clubBook)} />

        } else {
          var discussions = getAssociatedItems(this.props.entities.get('clubBookDiscussions'), 'clubBook', clubBook.get('id'))

          var discussionsElement = this.renderDiscussions(clubBook, discussions);

          var createNewDiscussionComponent = null;
          var creatingClubBookDiscussion = this.props.discussionDetails.get('creatingClubBookDiscussion');

          if (creatingClubBookDiscussion && creatingClubBookDiscussion.get('clubBook') == clubBook.get('id')) {

            createNewDiscussionComponent =
              <div key='create-new-discussion-component' className='create-new-discussion-form'>
                <Input
                  rowType={true}
                  label='Start Date'
                  inputWidth={120}
                  value={this.props.discussionDetails.get('creatingClubBookDiscussion').get('startDate')}
                  errors={this.props.statusDetails.getIn(['errors', 'startDate'])}
                  placeholder='MM/DD/YYYY'
                  inputClassName='inverted center'
                  divider={true}
                  onChange={this.props.editCreatingClubBookDiscussion.bind(this, 'startDate')} />
                <Input
                  rowType={true}
                  label='Start Time'
                  inputWidth={120}
                  value={this.props.discussionDetails.get('creatingClubBookDiscussion').get('startTime')}
                  errors={this.props.statusDetails.getIn(['errors', 'startTime'])}
                  placeholder='5:00 PM'
                  inputClassName='inverted center'
                  divider={true}
                  onChange={this.props.editCreatingClubBookDiscussion.bind(this, 'startTime')} />
                <Input
                  rowType={true}
                  label='Start Chapter'
                  inputWidth={120}
                  value={this.props.discussionDetails.get('creatingClubBookDiscussion').get('startChapter')}
                  errors={this.props.statusDetails.getIn(['errors', 'startChapter'])}
                  placeholder='Ex. 1'
                  inputClassName='inverted center'
                  divider={true}
                  info={'Readers start at the beginning of this chapter.'}
                  onChange={this.props.editCreatingClubBookDiscussion.bind(this, 'startChapter')} />
                <Input
                  rowType={true}
                  label='End Chapter'
                  inputWidth={120}
                  value={this.props.discussionDetails.get('creatingClubBookDiscussion').get('endChapter')}
                  errors={this.props.statusDetails.getIn(['errors', 'endChapter'])}
                  placeholder='Ex. 3'
                  inputClassName='inverted center'
                  divider={true}
                  info={'Readers read to the end of this chapter.'}
                  onChange={this.props.editCreatingClubBookDiscussion.bind(this, 'endChapter')} />
                <div className='actions row'>
                  <div className='action cancel-button inverted' onClick={this.props.cancelCreatingClubBookDiscussion}>
                    Cancel
                  </div>
                  <div className='action submit-button' onClick={this.props.createClubBookDiscussion.bind(this, clubBook.get('id'))}>
                    Create
                  </div>
                </div>
              </div>
          } else {
            createNewDiscussionComponent =
              <div key='create-new-discussion-component' className='create-new-discussion-button primary-button' onClick={this.props.startCreatingClubBookDiscussion.bind(this, clubBook.get('id'), discussions)}>
                Create Discussion
              </div>  
          }

          body = 
            [
              discussionsElement,
              createNewDiscussionComponent
            ]

          editClubBookDetailsButton =
            <span className='edit-club-book-details start' onClick={this.props.startEditingClubBook.bind(this, clubBook)} />
        }

        bookEntries.push(
          <div key={'clubBook'+clubBook.get('id')} className='book-entry'>
            {this.renderTimeLineSegment(clubBook)}
            <div className='book-entry-details'>
              <div className='book-title-area'>
                {this.renderBookTitle(book)}
                {editClubBookDetailsButton}
              </div>
              <div className='book-amazon-link-area'>
                {this.renderAmazonLink(book)}
              </div>
              {body}
            </div>
          </div>
        );
      });
    } else {
      bookEntries = this.renderNoBooksForPresident();
    }

    return (
      <DocumentTitle title={'Club'}>
        <div className='page'>
          <div className='club-detail-area'>
            <div className='pic-area'>
              <div className='pic-background' />
              {this.renderClubImage(club)}
              <div className='pic-actions'>
                {this.renderClubDiscussionAction(club)}
              </div>
            </div>
            <div className='club-detail-name'>
              {club.get('name')}
              <div className='icon edit-icon' onClick={this.startEditingClub.bind(this, club)} />
            </div>
            <div className='club-detail-description'>
              {club.get('description')}
            </div>
          </div>
          <div className='club-book-history-area' >
            {addNewBook}
            {bookEntries}
          </div>
        </div>
      </DocumentTitle>
    );
  }

  render() {
    var club = this.props.entities.getIn(['clubs', this.props.params.id.toString()]);

    if (club) {
      const user = getLoggedUser();

      if (!user) {
        return this.renderGuestPage(club);
      } else if (user.get('ownedClubs').includes(club.get('id'))) {
        // Eventually replace this with a page specific to presidents
        return this.renderPresidentPage(club);
      } else if (user.get('subscribedClubs').includes(club.get('id'))) {
        return this.renderMemberPage(club);
      } else {
        return this.renderNonMemberPage(club);
      }
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  const { entities, routing, clubBookDetails, discussionDetails, interactionDetails, loggedUserDetails, statusDetails } = state;

  return {
    entities,
    routing,
    clubBookDetails,
    loggedUserDetails,
    discussionDetails,
    interactionDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  push,
  fetchClubBooks,
  fetchClub,
  startEditingClubBook,
  cancelEditingClubBook,
  editEditingClubBook,
  editClubBook,
  startCreatingClubBookDiscussion,
  cancelCreatingClubBookDiscussion,
  editCreatingClubBookDiscussion,
  createClubBookDiscussion,
  deleteClubBookDiscussion,
  startEditingClub,
  activate,
  deactivate,
  joinClub,
  deleteClubBook
})(ClubPage);