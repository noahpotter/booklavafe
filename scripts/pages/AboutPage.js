import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import DiscussionPage from '../components/DiscussionPage';
import { fetchClubDiscussion, fetchClubDiscussionThreads, createClubDiscussionThread, fetchPosts } from '../actions';
import classNames from 'classnames';
import Textarea from '../components/Textarea';

// import '../../styles/modules/about';

class ClubDiscussionPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='about-page'>
        <div className='title'>
          Booklava
        </div>
        <div className='overview'>
          <p>
            Booklava is an innovative, simple, useable, and convenient book club service that differs from face-to-face clubs by featuring:
          </p>
          <ul>
            <li>
              24/7 participation time
            </li>
            <li>
              Enhanced chance for readers to find clubs of interest due to online availablity instead of geographical availability
            </li>
            <li>
              Clear and easy access to information
            </li>
            <li>
              Centralized club promotions
            </li>
          </ul>
          <p>
            By utilizing the Internet, Booklava structures, centralizes, and makes available all book club related activities and provides new methods of sharing and storing information of interest. Booklava hopes to spark a new generation of book club participation so that the joys of discussing books remains alive and thriving.
          </p>
        </div>
        <div className='spacer'>
        </div>
        <div className='amazon-disclaimer'>
          Booklava is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking to amazon.com.
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { entities, discussionDetails, loggedUserDetails, interactionDetails, postDetails } = state;

  return {
    entities,
    discussionDetails,
    loggedUserDetails,
    interactionDetails,
    postDetails
  };
}

export default connect(mapStateToProps, {
  fetchClubDiscussion,
  fetchClubDiscussionThreads,
  createClubDiscussionThread,
  fetchPosts,
})(ClubDiscussionPage);
