import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import Markdown from 'react-remarkable';
import markdown from '../markdown/homepage.md';

// import '../../styles/modules/home';

export default class DefaultPage extends Component {
  render() {
    return (
      <DocumentTitle title={' Page'}>
        <div className='home-page'>
          <Markdown options={{html: true}} source={markdown} />
        </div>
      </DocumentTitle>
    );
  }
}
