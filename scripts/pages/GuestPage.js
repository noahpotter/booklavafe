import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { login, updateLoginDetails } from '../actions';
import Input from '../components/Input';

// import '../../styles/modules/guest_pages';
// import '../../styles/modules/input';
// import '../../styles/modules/button';
// import '../../styles/modules/logo_header';

export default class GuestPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='guest-pages'>
        {this.props.children}
      </div>
    );
  }
}