import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { login, updateLoginDetails } from '../actions';
import classNames from 'classnames';
import Input from '../components/Input';

class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleFieldUpdate = this.handleFieldUpdate.bind(this);
  }

  render() {
    return (
      <DocumentTitle title={'Login'}>
        <div className={classNames('page', {'form-disabled': this.props.statusDetails.getIn(['progress', 'login'])})}>
          <div className='header'>
            Booklava
          </div>
          <div className='body'>
            <div className='input-row'>
              <Input 
                type='email' 
                name='email' 
                placeholder='Email' 
                value={this.props.loginDetails.get('email')}
                fieldName='Email or password'
                divider={true}
                errors={this.props.statusDetails.getIn(['errors', 'email'])}
                focusOnMount={!!!this.props.loginDetails.get('email')}
                onKeyUp={this.handleKeyUp.bind(this, this.handleLoginClick)}
                onChange={this.handleFieldUpdate.bind(this, ['email'])} />
            </div>
            <div className='input-row'>
              <Input 
                type='password' 
                name='password' 
                placeholder='Password' 
                errors={this.props.statusDetails.getIn(['errors', 'password'])}
                focusOnMount={!!this.props.loginDetails.get('email')}
                value={this.props.loginDetails.get('password')} 
                onKeyUp={this.handleKeyUp.bind(this, this.handleLoginClick)} 
                onChange={this.handleFieldUpdate.bind(this, ['password'])} />
            </div>
          </div>
          <div className='footer'>
            <div className='submit-button' onClick={this.handleLoginClick}>
              Login
            </div>
          </div>
          <div className='links'>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/register'})}>
              New user?
            </div>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/passwords/forgot'})}>
              Forgot Password?
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }

  handleLoginClick() {
    this.props.login({email: this.props.loginDetails.get('email'), password: this.props.loginDetails.get('password')});
  }

  handleFieldUpdate(attrs, value) {
    this.props.updateLoginDetails(attrs, value);
  }
}

function mapStateToProps(state) {
  const { loginDetails, statusDetails } = state;

  return {
    state,
    loginDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  login,
  updateLoginDetails,
  push
})(LoginPage);