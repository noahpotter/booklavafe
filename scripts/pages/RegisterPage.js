import React, { Component, PropTypes } from 'react';
import DocumentTitle from 'react-document-title';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createUser, updateLoginDetails } from '../actions';
import classNames from 'classnames';
import Input from '../components/Input';

// import '../../styles/modules/guest_pages';
// import '../../styles/modules/input';
// import '../../styles/modules/button';
// import '../../styles/modules/logo_header';

class RegisterPage extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleRegisterClick = this.handleRegisterClick.bind(this);
    this.handleFieldUpdate = this.handleFieldUpdate.bind(this);
  }

  render() {
    return (
      <DocumentTitle title={'Register'}>
        <div className={classNames('page', {'form-disabled': this.props.statusDetails.getIn(['progress', 'createUser'])})}>
          <div className='header'>
            Booklava
          </div>
          <div className='body'>
            <div className='input-row'>
              <Input 
                type='email' 
                name='email' 
                placeholder='Email' 
                value={this.props.loginDetails.get('email')}
                fieldName='Email or password'
                divider={true}
                errors={this.props.statusDetails.getIn(['errors', 'email'])}
                focusOnMount={!!!this.props.loginDetails.get('email')}
                onKeyUp={this.handleKeyUp.bind(this, this.handleRegisterClick)}
                onChange={this.handleFieldUpdate.bind(this, ['email'])} />
            </div>
            <div className='input-row'>
              <Input 
                type='username' 
                name='username' 
                placeholder='Username' 
                value={this.props.loginDetails.get('username')}
                fieldName='Username'
                divider={true}
                errors={this.props.statusDetails.getIn(['errors', 'username'])}
                focusOnMount={!!this.props.loginDetails.get('email')}
                onKeyUp={this.handleKeyUp.bind(this, this.handleRegisterClick)}
                onChange={this.handleFieldUpdate.bind(this, ['username'])} />
            </div>
            <div className='input-row'>
              <Input 
                type='password' 
                name='password' 
                placeholder='Password' 
                divider={true}
                errors={this.props.statusDetails.getIn(['errors', 'password'])}
                value={this.props.loginDetails.get('password')} 
                onKeyUp={this.handleKeyUp.bind(this, this.handleRegisterClick)} 
                onChange={this.handleFieldUpdate.bind(this, ['password'])} />
            </div>
            <div className='input-row'>
              <Input 
                type='password' 
                name='passwordConfirmation' 
                placeholder='Password Confirmation' 
                errors={this.props.statusDetails.getIn(['errors', 'passwordConfirmation'])}
                value={this.props.loginDetails.get('passwordConfirmation')} 
                onKeyUp={this.handleKeyUp.bind(this, this.handleRegisterClick)} 
                onChange={this.handleFieldUpdate.bind(this, ['passwordConfirmation'])} />
            </div>
          </div>
          <div className='footer'>
            <div className='submit-button' onClick={this.handleRegisterClick}>
              Register
            </div>
          </div>
          <div className='links'>
            <div className='link' onClick={this.props.push.bind(self, {pathname: '/login'})}>
              Login
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }

  handleKeyUp(func, e) {
    if (e.keyCode === 13) {
      func();
    }
  }

  handleRegisterClick() {
    this.props.createUser(this.props.loginDetails.toJS());
  }

  handleFieldUpdate(attrs, value) {
    this.props.updateLoginDetails(attrs, value);
  }
}

function mapStateToProps(state) {
  const { loginDetails, statusDetails } = state;

  return {
    loginDetails,
    statusDetails
  };
}

export default connect(mapStateToProps, {
  createUser,
  updateLoginDetails,
  push
})(RegisterPage);