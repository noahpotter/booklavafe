import React, { Component, PropTypes } from 'react';
import ReactDOMServer from 'react-dom/server';
import { connect } from 'react-redux';
import { createSuccessNotification, removeNotification } from './actions';
import NotificationSystem from 'react-notification-system';

// import '../styles/modules/variables';
// import '../styles/modules/app';
// import '../styles/modules/icons';
// import '../styles/modules/popover';
// import '../styles/modules/spacer';
// import '../styles/modules/sidebar';
// import '../styles/modules/variables';

class App extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(props) {
    super(props);
    this.addNotification = this.addNotification.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
    this.removeNotification = this.removeNotification.bind(this);

    this.state = {
      notificationSystem: null,
    }
  }

  componentDidUpdate() {
    this.renderNotifications(this.props);
  }

  componentDidMount() {
    this.setState({notificationSystem: this.refs.notificationSystem});

    // var message = 
    //   <div>
    //     <b>Hello</b>
    //     <span> This is more text</span>
    //   </div>

    // this.props.createSuccessNotification({title: 'Successfully Added Book', message: ReactDOMServer.renderToString(message)});
  }

  addNotification(notification) {
    this.state.notificationSystem.addNotification({
      title: notification.get('title'),
      message: notification.get('message'),
      level: notification.get('level'),
      position: 'br',
      // autoDismiss: 0,
      uid: notification.get('uid'),
      onRemove: this.removeNotification
    });
  }

  removeNotification(notification) {
    this.props.removeNotification(notification);
  }

  renderNotifications(props, newProps) {
    props.notificationDetails.get('notifications').forEach(notification => {
      this.addNotification(notification);
    });
  }

  render() {
    var style = {
      Dismiss: {
        DefaultStyle: {
          top: 'calc(50% - 4px)',
          right: '10px'
        }
      },
      NotificationItem: {
        DefaultStyle: {
          paddingRight: '30px'
        }
      }
    }

    return (
      <div className='app'>
        {this.props.children}
        <NotificationSystem ref="notificationSystem" allowHTML={true} style={style} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { notificationDetails } = state

  return {
    notificationDetails
  };
}

export default connect(mapStateToProps, {
  createSuccessNotification,
  removeNotification,
})(App);