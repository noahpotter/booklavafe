import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import { editFeedback, hidePopupForm, submitFeedback } from './actions';
import { RouteHandler } from 'react-router';
import classNames from 'classnames';
import config from 'config';
import SideBar from './components/SideBar';
import Popup from './components/Popup';
import Textarea from './components/Textarea';
import visibility from './vendor/visibility';

import '../styles/modules/about';
import '../styles/modules/actions';
import '../styles/modules/app';
import '../styles/modules/base';
import '../styles/modules/button';
import '../styles/modules/calendar';
import '../styles/modules/club';
import '../styles/modules/clubs';
import '../styles/modules/colors_page';
import '../styles/modules/discussion';
import '../styles/modules/dropzone';
import '../styles/modules/expandable_text';
import '../styles/modules/feedback';
import '../styles/modules/forms';
import '../styles/modules/guest_pages';
import '../styles/modules/home';
import '../styles/modules/icons';
import '../styles/modules/input';
import '../styles/modules/loading_animation';
import '../styles/modules/logo_header';
import '../styles/modules/pagination_controls';
import '../styles/modules/picture';
import '../styles/modules/popover';
import '../styles/modules/popup';
import '../styles/modules/search';
import '../styles/modules/sidebar';
import '../styles/modules/spacer';
import '../styles/modules/toggle_button';
import '../styles/modules/user';

class Base extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var feedbackPopup =
      <Popup
        visible={this.props.popupDetails.getIn(['forms', 'feedback'])}
        title='Leave Feedback'
        onCancel={this.props.hidePopupForm.bind(this, 'feedback')}
        acceptText='Submit'
        onAccept={this.props.submitFeedback}
      >
        <div className='feedback-form'>
          <Textarea 
            value={this.props.feedbackDetails.get('message')}
            errors={this.props.statusDetails.getIn(['errors', 'feedback', 'message'])}
            onChange={this.props.editFeedback.bind(this)}
            colType={true}
            label='Message'
            placeholder="Let us know how we're doing!"
            focusOnMount={true}
          />
        </div>
      </Popup>

    return (
      <DocumentTitle title='Chain Reactions'>
        <div className={classNames('base', {'form-disabled': this.props.statusDetails.getIn(['progress', 'logout'])})}>
          <SideBar pathname={this.props.pathname} />
          {feedbackPopup}
          {this.props.children}
          {/*<div style={{minHeight: (config.env == 'dev' ? '200px' : '0px')}} />*/}
        </div>
      </DocumentTitle>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { statusDetails, popupDetails, feedbackDetails } = state;

  return {
    state,
    popupDetails,
    feedbackDetails,
    statusDetails,
    pathname: ownProps.location.pathname
  };
}

export default connect(mapStateToProps, {
  editFeedback,
  submitFeedback,
  hidePopupForm
})(Base);