import { CALL_API, Schemas } from '../middleware/api';

export const SHOW_POPUP_FORM = 'SHOW_POPUP_FORM';
export const HIDE_POPUP_FORM = 'HIDE_POPUP_FORM';

export function showPopupForm(form) {
  return {
    type: SHOW_POPUP_FORM,
    form
  }
}

export function hidePopupForm(form) {
  return {
    type: HIDE_POPUP_FORM,
    form
  }
}
