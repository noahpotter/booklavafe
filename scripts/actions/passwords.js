import { CALL_API, Schemas } from '../middleware/api';
import Immutable from 'immutable';
import config from 'config';
import { setProgressStatus, resetProgressStatus, setErrorStatus } from './status';

import { push } from 'react-router-redux';

export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILURE = 'FORGOT_PASSWORD_FAILURE';

export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_FAILURE = 'RESET_PASSWORD_FAILURE';

export function forgotPassword(data) {
  data['domain'] = config.FRONTEND;

  return function(dispatch, getState) {
    dispatch(setProgressStatus('forgotPassword', true));
    return dispatch({
      [CALL_API]: {
        types: [FORGOT_PASSWORD, {type: FORGOT_PASSWORD_SUCCESS, next: push.bind(this, {pathname: '/login'})}, FORGOT_PASSWORD_FAILURE],
        endpoint: `/passwords/forgot/`,
        method: 'POST',
        data
      }
    })
  }

}

export function resetPassword(token) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('resetPassword', true));
    var state = getState();
    var loginDetails = state.loginDetails
    var data = {
      token,
      password: loginDetails.get('password')
    }

    if (loginDetails.get('password') != loginDetails.get('passwordConfirmation')) {
      dispatch(resetProgressStatus());
      return dispatch(setErrorStatus('passwordConfirmation', 'Passwords do not match'));
    }

    return dispatch({
      [CALL_API]: {
        types: [RESET_PASSWORD, {type: RESET_PASSWORD_SUCCESS, next: push.bind(this, {pathname: '/login'})}, RESET_PASSWORD_FAILURE],
        endpoint: `/passwords/reset/`,
        method: 'POST',
        data
      }
    })
  }
}
