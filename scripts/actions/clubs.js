import React, { Component, PropTypes } from 'react';
import ReactDOMServer from 'react-dom/server';

import { CALL_API, Schemas } from '../middleware/api';
import { getLoggedUser } from '../utils';
import { fetchClubBooks } from './club_books';
import { fetchClubDiscussions } from './club_discussions';
import { fetchUser } from './users';
import { setProgressStatus } from './status';
import { push } from 'react-router-redux';
import { createErrorNotification, createSuccessNotification } from './notifications';

export const START_CREATING_CLUB = 'START_CREATING_CLUB';
export const EDIT_CREATING_CLUB = 'EDIT_CREATING_CLUB';
export const CANCEL_CREATING_CLUB = 'CANCEL_CREATING_CLUB';

export const FETCH_CLUBS = 'FETCH_CLUBS';
export const FETCH_CLUBS_SUCCESS = 'FETCH_CLUBS_SUCCESS';
export const FETCH_CLUBS_FAILURE = 'FETCH_CLUBS_FAILURE';

export const FETCH_CLUB = 'FETCH_CLUB';
export const FETCH_CLUB_SUCCESS = 'FETCH_CLUB_SUCCESS';
export const FETCH_CLUB_FAILURE = 'FETCH_CLUB_FAILURE';

export const CREATE_CLUB = 'CREATE_CLUB';
export const CREATE_CLUB_SUCCESS = 'CREATE_CLUB_SUCCESS';
export const CREATE_CLUB_FAILURE = 'CREATE_CLUB_FAILURE';

export const START_EDITING_CLUB = 'START_EDITING_CLUB';
export const EDIT_EDITING_CLUB = 'EDIT_EDITING_CLUB';
export const CANCEL_EDITING_CLUB = 'CANCEL_EDITING_CLUB';

export const EDIT_CLUB = 'EDIT_CLUB';
export const EDIT_CLUB_SUCCESS = 'EDIT_CLUB_SUCCESS';
export const EDIT_CLUB_FAILURE = 'EDIT_CLUB_FAILURE';

export const DELETE_CLUB = 'DELETE_CLUB';
export const DELETE_CLUB_SUCCESS = 'DELETE_CLUB_SUCCESS';
export const DELETE_CLUB_FAILURE = 'DELETE_CLUB_FAILURE';

export const UNSUBSCRIBE_CLUB = 'UNSUBSCRIBE_CLUB';
export const UNSUBSCRIBE_CLUB_SUCCESS = 'UNSUBSCRIBE_CLUB_SUCCESS';
export const UNSUBSCRIBE_CLUB_FAILURE = 'UNSUBSCRIBE_CLUB_FAILURE';

export const JOIN_CLUB = 'JOIN_CLUB';
export const JOIN_CLUB_SUCCESS = 'JOIN_CLUB_SUCCESS';
export const JOIN_CLUB_FAILURE = 'JOIN_CLUB_FAILURE';

function handleFetchedClubs(userId, includes, response) {
  return function(dispatch, getState) {
    var state = getState();

    if (includes && includes['clubDiscussions']) {
      state.entities.get('clubs').forEach(club => {
        if (club.get('members').includes(userId)) {
          dispatch(fetchClubDiscussions({clubId: club.get('id')}, includes));
        }
      }) 
    }

    if (includes && includes['clubBooks']) {
      state.entities.get('clubs').forEach(club => {
        if (club.get('members').includes(userId)) {
          dispatch(fetchClubBooks({clubId: club.get('id')}, includes));
        }
      }) 
    }
  }
}

export function fetchClubs(userId, includes) {
  return function(dispatch, getState) {

    return dispatch({
      [CALL_API]: {
        types: [FETCH_CLUBS, {type: FETCH_CLUBS_SUCCESS, next: handleFetchedClubs.bind(this, userId, includes)}, FETCH_CLUBS_FAILURE],
        endpoint: `/users/${userId}/clubs/`,
        method: 'get',
        // schema: Schemas.CLUB_MEMBERSHIP_ARRAY
        schema: Schemas.CLUB_ARRAY
      }
    })
  }
}

export function fetchClub(clubId) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB, FETCH_CLUB_SUCCESS, FETCH_CLUB_FAILURE],
      endpoint: `/clubs/${clubId}/`,
      method: 'get',
      schema: Schemas.CLUB
    }
  }
}

export function startCreatingClub() {
  return {
    type: START_CREATING_CLUB
  }
}

export function editCreatingClub(attr, value) {
  return {
    type: EDIT_CREATING_CLUB,
    attr,
    value
  }
}

export function cancelCreatingClub() {
  return {
    type: CANCEL_CREATING_CLUB
  }
}

export function createClub() {
  return function(dispatch, getState) {
    const loggedUserDetails = getLoggedUser()
    var club = getState().clubDetails.get('creatingClub')

    var image = club.get('image');
      
    var data = club;

    function handleCreateClubFailure(response) {
      if (response.owned_club_limit_reached) {
        dispatch(createErrorNotification({title: 'Max clubs reached', message: response.owned_club_limit_reached}));
      }
    }
    
    if (image instanceof Array && image[0] instanceof File) {
      data = new FormData()
      data.append('image', club.get('image')[0])
      data.append('name', club.get('name'))
      data.append('description', club.get('description'))
      data.append('use_club_image', club.get('useClubImage'))
    } else {
      data = data.remove('image');
      data = data.toJS();
    }

    dispatch(setProgressStatus('createClub', true));
    return dispatch({
      [CALL_API]: {
        types: [CREATE_CLUB, CREATE_CLUB_SUCCESS, {type: CREATE_CLUB_FAILURE, handle: handleCreateClubFailure}],
        endpoint: `/users/${loggedUserDetails.get('id')}/clubs/`,
        method: 'POST',
        schema: Schemas.CLUB,
        data
      }
    })
  }
}

export function startEditingClub(club) {
  return {
    type: START_EDITING_CLUB,
    club
  }
}

export function editEditingClub(attr, value) {
  return {
    type: EDIT_EDITING_CLUB,
    attr,
    value
  }
}

export function cancelEditingClub() {
  return {
    type: CANCEL_EDITING_CLUB
  }
}

export function editClub() {
  return function(dispatch, getState) {
    var club = getState().clubDetails.get('editingClub')

    var image = club.get('image');
      
    var data = club;
    if (image instanceof Array && image[0] instanceof File) {
      data = new FormData()
      data.append('image', club.get('image')[0])
      data.append('name', club.get('name'))
      data.append('description', club.get('description'))
      data.append('use_club_image', club.get('useClubImage'))
    } else {
      data = data.remove('image');
      data = data.toJS();
    }

    dispatch(setProgressStatus('editClub', true));
    return dispatch({
      [CALL_API]: {
        types: [EDIT_CLUB, {type: EDIT_CLUB_SUCCESS, next: createSuccessNotification.bind(this, {message: `${club.get('name')} was successfully edited.`})}, EDIT_CLUB_FAILURE],
        endpoint: `/clubs/${club.get('id')}/`,
        method: 'PATCH',
        schema: Schemas.CLUB,
        data
      }
    })
  }
}

// Make sure to test this is removed from the store
export function deleteClub(id) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('deleteClub', true));
    return dispatch({
      [CALL_API]: {
        types: [DELETE_CLUB, DELETE_CLUB_SUCCESS, DELETE_CLUB_FAILURE],
        endpoint: `/clubs/${id}/`,
        data: {id, type: 'clubs'},
        headers: {'Cache-Control': 'no-cache'},
        method: 'delete'
      }
    })
  }
}

export function unsubscribeClub(id) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('unsubscribeClub', id));
    var userId = getState().loggedUserDetails.get('id');
    return dispatch({
      [CALL_API]: {
        types: [UNSUBSCRIBE_CLUB, {type: UNSUBSCRIBE_CLUB_SUCCESS, next: fetchUser.bind(this, getLoggedUser().get('id'))}, UNSUBSCRIBE_CLUB_FAILURE],
        endpoint: `/users/${userId}/clubs/${id}/`,
        method: 'delete'
      }
    })
  }
}

export function joinClub(id) {
  return function(dispatch, getState) {
    var userId = getState().loggedUserDetails.get('id');
    var club = getState().entities.getIn(['clubs', id.toString()]);

    function handleJoinClubFailure(response) {
      if (response.subscribed_club_limit_reached) {
        dispatch(createErrorNotification({title: 'Max clubs reached', message: response.subscribed_club_limit_reached}));
      } else if (response.unique_together) {
        dispatch(createErrorNotification({message: response.unique_together}));
      }
    }

    function handleSuccess(response) {
      var message = 
        <div>
          <span>Successfully joined club </span>
          <b>{club.get('name')}</b>
          .
        </div>

      dispatch(push({pathname: `/clubs/${id}`}));
      dispatch(createSuccessNotification({title: 'Joined Club', message: ReactDOMServer.renderToString(message)}))
    }

    return dispatch({
      [CALL_API]: {
        types: [JOIN_CLUB, {type: JOIN_CLUB_SUCCESS, handle: handleSuccess}, {type: JOIN_CLUB_FAILURE, handle: handleJoinClubFailure}],
        endpoint: `/club_memberships/`,
        data: {'user_id': userId, 'club_id': id},
        method: 'post',
        schema: Schemas.CLUB_MEMBERSHIP
      }
    })
  }
}