import { CALL_API, Schemas } from '../middleware/api';
import { fetchClubBooks } from './club_books';
import Immutable from 'immutable';
import moment from 'moment';

export const EDIT_SEARCH_QUERY = 'EDIT_SEARCH_QUERY';
export const SELECT_SEARCH_RESULT = 'SELECT_SEARCH_RESULT';
export const CHANGE_SEARCH_TYPE = 'CHANGE_SEARCH_TYPE';

export const SEARCH = 'SEARCH';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'SEARCH_FAILURE';

function handleSearchEntities(response) {
  return function(dispatch, getState) {
    var state = getState();
    const type = state.searchDetails.get('type');
    const query = state.searchDetails.get('query');

    const query_parts = query.split(' ')
    if (query_parts.length > 0) {
      if (query_parts[0] == 'book:') {
        query_parts.splice(0, 1);
        const book_name = query_parts.join(' ')
        const entities = Immutable.fromJS(response.entities);
        const datetime = moment().utc().format('YYYY-MM-DD HH:mm:ss');

        var clubs = entities.get('clubs');
        if (clubs) {
          clubs.forEach(club => {
            dispatch(fetchClubBooks({clubId: club.get('id'), query: {book__name__iexact: book_name, start_date__lte: datetime, end_date__gte: datetime}}))
          })
        }
      }
    }
  }
}

export function search() {
  return function(dispatch, getState) {
    var state = getState();

    const schemas = {
      'books': Schemas.BOOK_ARRAY,
      'clubs': Schemas.CLUB_ARRAY,
      'users': Schemas.USER_ARRAY
    }
    const type = state.searchDetails.get('type');
    const query = state.searchDetails.get('query');
    const page = state.pageDetails.getIn(['page', 'search']);

    dispatch({
      [CALL_API]: {
        types: [SEARCH, {type: SEARCH_SUCCESS, next: handleSearchEntities}, SEARCH_FAILURE],
        endpoint: '/search/',
        method: 'get',
        schema: schemas[type],
        query: {query, type, page},
        initialActionData: {query, type, page}
      }
    });
  }
}

export function editSearchQuery(query) {
  return {
    type: EDIT_SEARCH_QUERY,
    query
  }
}

export function selectSearchResult(id) {
  return {
    type: SELECT_SEARCH_RESULT,
    id
  }
}

export function changeSearchType(searchType) {
  return {
    type: CHANGE_SEARCH_TYPE,
    searchType
  }
}