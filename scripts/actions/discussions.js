import { CALL_API, Schemas } from '../middleware/api';

export const TOGGLE_DISCUSSION_DETAILS = 'TOGGLE_DISCUSSION_DETAILS';

export function toggleDiscussionDetails() {
  return {
    type: TOGGLE_DISCUSSION_DETAILS
  }
}