import React, { Component, PropTypes } from 'react';
import ReactDOMServer from 'react-dom/server';

import { CALL_API, Schemas } from '../middleware/api';
import { push } from 'react-router-redux';
import { setErrors, resetErrorStatus, setProgressStatus, resetProgressStatus } from './status';
import { createErrorNotification, createSuccessNotification } from './notifications';
import { parseDate } from '../utils';

export const SHOW_ADD_BOOK_TO_CLUB = 'SHOW_ADD_BOOK_TO_CLUB';
export const CANCEL_ADD_BOOK_TO_CLUB = 'CANCEL_ADD_BOOK_TO_CLUB';

export const SELECT_BOOK = 'SELECT_BOOK';
export const SELECT_CLUB = 'SELECT_CLUB';
export const UPDATE_END_DATE = 'UPDATE_END_DATE';
export const UPDATE_START_DATE = 'UPDATE_START_DATE';

export const ADD_BOOK_TO_CLUB = 'ADD_BOOK_TO_CLUB';
export const ADD_BOOK_TO_CLUB_SUCCESS = 'ADD_BOOK_TO_CLUB_SUCCESS';
export const ADD_BOOK_TO_CLUB_FAILURE = 'ADD_BOOK_TO_CLUB_FAILURE';

export const FETCH_BOOK = 'FETCH_BOOK';
export const FETCH_BOOK_SUCCESS = 'FETCH_BOOK_SUCCESS';
export const FETCH_BOOK_FAILURE = 'FETCH_BOOK_FAILURE';

import moment from 'moment';

export function fetchBook(bookId) {
  return {
    [CALL_API]: {
      types: [FETCH_BOOK, FETCH_BOOK_SUCCESS, FETCH_BOOK_FAILURE],
      endpoint: `/books/${bookId}/`,
      method: 'get',
      schema: Schemas.BOOK
    }
  }
}

export function showAddBookToClub() {
  return {
    type: SHOW_ADD_BOOK_TO_CLUB
  }
}

export function cancelAddBookToClub() {
  return {
    type: CANCEL_ADD_BOOK_TO_CLUB
  }
}

export function submitAddBookToClub() {
  return function(dispatch, getState) {
    dispatch(resetErrorStatus());
    dispatch(setProgressStatus('addBook', true));
    const state = getState();
    const addBookDetails = state.addBookDetails;
    const bookId = state.searchDetails.get('selectedId');
    const clubId = addBookDetails.get('clubId');
    const club = state.entities.getIn(['clubs', clubId.toString()]);
    const book = state.entities.getIn(['books', bookId.toString()]);

    function success() {
      var message = 
        <div>
          <span>Book </span>
          <b>{book.get('name')}</b>
          <span> was successfully added to club </span>
          <b>{club.get('name')}</b>
          .
        </div>

      dispatch(push({pathname: `/clubs/${addBookDetails.get('clubId')}`}));
      dispatch(createSuccessNotification({title: 'Successfully Added Book', message: ReactDOMServer.renderToString(message) }));
      return dispatch(cancelAddBookToClub());
    }

    // Check date formats

    var startDate = parseDate(addBookDetails.get('startDate'));
    var startFormatted = startDate.format('YYYY-MM-DDT00:00');

    var endDate = parseDate(addBookDetails.get('endDate'));
    var endFormatted = endDate.format('YYYY-MM-DDT00:00');

    var errors = {};
    var isValid = true;

    if (!startDate.isValid()) {
      errors['startDate'] = 'Format should be MM/DD/YYYY';
      isValid = false;
    }

    if  (!endDate.isValid()) {
      errors['endDate'] = 'Format should be MM/DD/YYYY';
      isValid = false;
    }

    if (isValid) {
      return dispatch({
        [CALL_API]: {
          types: [ADD_BOOK_TO_CLUB, {type: ADD_BOOK_TO_CLUB_SUCCESS, next: success.bind(this)}, ADD_BOOK_TO_CLUB_FAILURE],
          endpoint: `/clubs/${clubId}/club_books/`,
          method: 'post',
          schema: Schemas.CLUB_BOOK,
          // schema: Schemas.CLUB_MEMBERSHIP,
          data: {bookId, endDate: endFormatted, startDate: startFormatted}
        }
      })
    } else {
      dispatch(resetProgressStatus());
      return dispatch(setErrors(errors));
    }
  }
}

export function selectClub(clubId) {
  return {
    type: SELECT_CLUB,
    clubId
  }
}

export function selectBook(bookId) {
  return {
    type: CANCEL_ADD_BOOK_TO_CLUB,
    bookId
  }
}

export function updateStartDate(startDate) {
  return {
    type: UPDATE_START_DATE,
    startDate
  }
}

export function updateEndDate(endDate) {
  return {
    type: UPDATE_END_DATE,
    endDate
  }
}