import { CALL_API, Schemas } from '../middleware/api';
import { decamelize } from 'humps';
import { setProgressStatus } from './status';

export const CREATE_THREAD = 'CREATE_THREAD';
export const CREATE_THREAD_SUCCESS = 'CREATE_THREAD_SUCCESS';
export const CREATE_THREAD_FAILURE = 'CREATE_THREAD_FAILURE';

export const START_CREATING_THREAD = 'START_CREATING_THREAD';
export const PAUSE_CREATING_THREAD = 'PAUSE_CREATING_THREAD';
export const CANCEL_CREATING_THREAD = 'PAUSE_CREATING_THREAD';
export const EDIT_CREATING_THREAD = 'EDIT_CREATING_THREAD';

export const DELETE_THREAD = 'DELETE_THREAD';
export const DELETE_THREAD_SUCCESS = 'DELETE_THREAD_SUCCESS';
export const DELETE_THREAD_FAILURE = 'DELETE_THREAD_FAILURE';

export const FETCH_THREADS = 'FETCH_THREADS';
export const FETCH_THREADS_SUCCESS = 'FETCH_THREADS_SUCCESS';
export const FETCH_THREADS_FAILURE = 'FETCH_THREADS_FAILURE';

export const FETCH_NEW_THREADS = 'FETCH_NEW_THREADS';
export const FETCH_NEW_THREADS_SUCCESS = 'FETCH_NEW_THREADS_SUCCESS';
export const FETCH_NEW_THREADS_FAILURE = 'FETCH_NEW_THREADS_FAILURE';

export const SELECT_THREAD_ITEM = 'SELECT_THREAD_ITEM';
export const DESELECT_THREAD_ITEM = 'DESELECT_THREAD_ITEM';
export const TOGGLE_ACTIVE_THREAD_ITEM = 'TOGGLE_ACTIVE_THREAD_ITEM';

export const SHOW_HIERARCHY = 'SHOW_HIERARCHY';
export const HIDE_HIERARCHY = 'HIDE_HIERARCHY';

export function createClubDiscussionThread() {
  return function(dispatch, getState) {
    var state = getState();
    var data = state.threadDetails.get('creatingThread');
    return createThread(dispatch, state, 'club_discussions', data.get('clubDiscussionId'))
  }
}

export function createClubBookDiscussionThread() {
  return function(dispatch, getState) {
    var state = getState();
    var data = state.threadDetails.get('creatingThread');
    return createThread(dispatch, state, 'club_book_discussions', data.get('clubBookDiscussionId'))
  }
}

function createThread(dispatch, state, type, typeId) {
  var data = state.threadDetails.get('creatingThread').toJS();
  data.user = state.loggedUserDetails.get('id');

  dispatch(setProgressStatus('createThread', true));
  return dispatch({
    [CALL_API]: {
      types: [CREATE_THREAD, CREATE_THREAD_SUCCESS, CREATE_THREAD_FAILURE],
      endpoint: `/${type}/${typeId}/threads/`,
      method: 'post',
      schema: Schemas.THREAD,
      data,
      meta: {
        parentError: 'thread'
      }
    }
  })
}

export function editCreatingThread(attr, value) {
  return {
    type: EDIT_CREATING_THREAD,
    attr,
    value
  }
}

export function deleteThread(id) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('deleteThread', id));
    return dispatch({
      [CALL_API]: {
        types: [DELETE_THREAD, DELETE_THREAD_SUCCESS, DELETE_THREAD_FAILURE],
        endpoint: `/threads/${id}/`,
        data: {id, type: 'threads'},
        method: 'delete'
      }
    })
  }
}

export function fetchClubBookDiscussionThreads(clubBookDiscussionId) {
  return {
    [CALL_API]: {
      types: [FETCH_THREADS, FETCH_THREADS_SUCCESS, FETCH_THREADS_FAILURE],
      endpoint: `/club_book_discussions/${clubBookDiscussionId}/threads/`,
      method: 'get',
      schema: Schemas.THREAD_ARRAY
    }
  }
}

export function fetchClubDiscussionThreads(clubDiscussionId) {
  return {
    [CALL_API]: {
      types: [FETCH_THREADS, FETCH_THREADS_SUCCESS, FETCH_THREADS_FAILURE],
      endpoint: `/club_discussions/${clubDiscussionId}/threads/`,
      method: 'get',
      schema: Schemas.THREAD_ARRAY
    }
  }
}

export function fetchNewThreads(discussionType, discussionId, has_threads) {
  return {
    [CALL_API]: {
      types: [FETCH_NEW_THREADS, FETCH_NEW_THREADS_SUCCESS, FETCH_NEW_THREADS_FAILURE],
      endpoint: `/${decamelize(discussionType)}s/${discussionId}/threads/`,
      method: 'get',
      schema: Schemas.THREAD_ARRAY,
      query: {has_threads},
      resetStatus: false,
      meta: {
        apiResponseAffectsStatus: false
      }
    }
  }
}

export function startReply(postId) {
  return {
    type: START_REPLY,
    postId,
  }
}

export function sendReply(data) {
  return {
    [CALL_API]: {
      types: [CREATE_REPLY, REPLY_SUCCESS, REPLY_FAILURE],
      endpoint: `replies`,
      schema: Schemas.REPLY
    }
  };
}

export function cancelReply() {
  return {
    type: CANCEL_REPLY,
  }
}

export function startCreatingThread(discussionType, discussionId) {
  return {
    type: START_CREATING_THREAD,
    discussionType,
    discussionId
  }
}

export function pauseCreatingThread() {
  return {
    type: PAUSE_CREATING_THREAD,
  }
}

export function cancelCreatingThread() {
  return {
    type: CANCEL_CREATING_THREAD,
  }
}

export function selectThreadItem(threadId) {
  return function(dispatch, getState) {
    var state = getState();
    var takenColors = [];
    state.threadDetails.get('activeThreads').forEach(activeThread => {
      takenColors.push(activeThread.get('color'));
    });
    var colorIndex = '0';
    for(var i=0; i<state.discussionDetails.get('colors').size; i++) {
      if (takenColors.indexOf(i) == -1) {
        colorIndex = i;
        break;
      }
    }
    return dispatch({
      type: SELECT_THREAD_ITEM,
      threadId,
      colorIndex
    });
  }
}

export function deselectThreadItem(threadId) {
  return {
    type: DESELECT_THREAD_ITEM,
    threadId
  }
}

export function showHierarchy(threadId, postId) {
  return {
    type: SHOW_HIERARCHY,
    threadId,
    postId
  }
}

export function hideHierarchy(threadId) {
  return {
    type: HIDE_HIERARCHY,
    threadId
  }
}

export function toggleActiveThreadItem(threadId) {
  return function(dispatch, getState) {
    if (getState().threadDetails.get('activeThreads').has(threadId.toString())) {
      dispatch(deselectThreadItem(threadId));
    } else {
      dispatch(selectThreadItem(threadId));
    }
  }
}
