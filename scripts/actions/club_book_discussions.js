import { CALL_API, Schemas } from '../middleware/api';
import { fetchClubBook } from './club_books';
import { setErrors, resetErrorStatus } from './status';
import { parseDate, parseTime } from '../utils';
import moment from 'moment';
window.moment = moment;

export const CREATE_CLUB_BOOK_DISCUSSION = 'CREATE_CLUB_BOOK_DISCUSSION';
export const CREATE_CLUB_BOOK_DISCUSSION_SUCCESS = 'CREATE_CLUB_BOOK_DISCUSSION_SUCCESS';
export const CREATE_CLUB_BOOK_DISCUSSION_FAILURE = 'CREATE_CLUB_BOOK_DISCUSSION_FAILURE';

export const DELETE_CLUB_BOOK_DISCUSSION = 'DELETE_CLUB_BOOK_DISCUSSION';
export const DELETE_CLUB_BOOK_DISCUSSION_SUCCESS = 'DELETE_CLUB_BOOK_DISCUSSION_SUCCESS';
export const DELETE_CLUB_BOOK_DISCUSSION_FAILURE = 'DELETE_CLUB_BOOK_DISCUSSION_FAILURE';

export const FETCH_CLUB_BOOK_DISCUSSIONS = 'FETCH_CLUB_BOOK_DISCUSSIONS';
export const FETCH_CLUB_BOOK_DISCUSSIONS_SUCCESS = 'FETCH_CLUB_BOOK_DISCUSSIONS_SUCCESS';
export const FETCH_CLUB_BOOK_DISCUSSIONS_FAILURE = 'FETCH_CLUB_BOOK_DISCUSSIONS_FAILURE';

export const FETCH_CLUB_BOOK_DISCUSSION = 'FETCH_CLUB_BOOK_DISCUSSION';
export const FETCH_CLUB_BOOK_DISCUSSION_SUCCESS = 'FETCH_CLUB_BOOK_DISCUSSION_SUCCESS';
export const FETCH_CLUB_BOOK_DISCUSSION_FAILURE = 'FETCH_CLUB_BOOK_DISCUSSION_FAILURE';

export const START_CREATING_CLUB_BOOK_DISCUSSION = 'START_CREATING_CLUB_BOOK_DISCUSSION';
export const EDIT_CREATING_CLUB_BOOK_DISCUSSION = 'EDIT_CREATING_CLUB_BOOK_DISCUSSION';
export const CANCEL_CREATING_CLUB_BOOK_DISCUSSION = 'CANCEL_CREATING_CLUB_BOOK_DISCUSSION';

// Eventually update all fetching method to use this style, or create a generic factory to create these methods

function handleFetchedClubBookDiscussions(clubBookId, includes, response) {
  return function(dispatch, getState) {
    var state = getState();

    state.entities.get('clubBookDiscussions').forEach(clubBookDiscussion => {
      if (clubBookDiscussion.get('clubBook') == clubBookId) {
        handleFetchedClubBookDiscussion(clubBookDiscussion.get('id'), includes, response);
      }
    });
  }
}

function handleFetchedClubBookDiscussion(clubBookDiscussionId, includes, response) {
  return function(dispatch, getState) {
    var state = getState();

    if (includes) {
      var immutableIncludes = Immutable.fromJS(includes);
      immutableIncludes = immutableIncludes.merge({'clubBook': false});

      if (includes['clubBook']) {
        dispatch(fetchClubBook({clubBookId: state.entities.getIn(['clubBookDiscussions', clubBookDiscussionId]).get('clubBook')}, immutableIncludes.toJS()));
      }
    }
  }
}

export function fetchClubBookDiscussion(data, includes) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB_BOOK_DISCUSSION, {type: FETCH_CLUB_BOOK_DISCUSSION_SUCCESS, next: handleFetchedClubBookDiscussion.bind(this, data.clubBookDiscussionId, includes)}, FETCH_CLUB_BOOK_DISCUSSION_FAILURE],
      endpoint: `/club_book_discussions/${data.clubBookDiscussionId}/`,
      method: 'GET',
      schema: Schemas.CLUB_BOOK_DISCUSSION
    }
  }
}

export function fetchClubBookDiscussions(data, includes) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB_BOOK_DISCUSSIONS, {type: FETCH_CLUB_BOOK_DISCUSSIONS_SUCCESS, next: handleFetchedClubBookDiscussions.bind(this, data.clubBookId, includes)}, FETCH_CLUB_BOOK_DISCUSSIONS_FAILURE],
      endpoint: `/club_books/${data.clubBookId}/discussions/`,
      method: 'GET',
      schema: Schemas.CLUB_BOOK_DISCUSSION_ARRAY
    }
  }
}

export function startCreatingClubBookDiscussion(clubBookId, discussions) {
  return {
    type: START_CREATING_CLUB_BOOK_DISCUSSION,
    clubBookId,
    discussions
  }
}

export function editCreatingClubBookDiscussion(attr, value) {
  return {
    type: EDIT_CREATING_CLUB_BOOK_DISCUSSION,
    attr,
    value
  }
}

export function cancelCreatingClubBookDiscussion() {
  return {
    type: CANCEL_CREATING_CLUB_BOOK_DISCUSSION
  }
}

export function createClubBookDiscussion(clubBookId) {
  return function(dispatch, getState) {
    dispatch(resetErrorStatus());
    var discussion = getState().discussionDetails.get('creatingClubBookDiscussion')

    // Fix startDate and endDate formats
    var startDate = parseDate(discussion.get('startDate'));
    var startTime = parseTime(discussion.get('startTime'));

    var errors = {};
    var hasErrors = false;

    if (!startDate.isValid()) {
      errors['startDate'] = 'Format should be MM/DD/YYYY';
      hasErrors = true;
    }

    if (!startTime.isValid()) {
      errors['startTime'] = 'Format should be HH:MM AM/PM';
      hasErrors = true;
    }

    if (hasErrors) {
      return dispatch(setErrors(errors));
    } else {
      var date = startDate.add(startTime);

      discussion = discussion.set('startDate', date.utc().format('YYYY-MM-DDTHH:mm'))
      discussion = discussion.set('clubBookId', clubBookId)

      return dispatch({
        [CALL_API]: {
          types: [CREATE_CLUB_BOOK_DISCUSSION, CREATE_CLUB_BOOK_DISCUSSION_SUCCESS, CREATE_CLUB_BOOK_DISCUSSION_FAILURE],
          endpoint: `/club_books/${discussion.get('clubBookId')}/discussions/`,
          method: 'POST',
          schema: Schemas.CLUB_BOOK_DISCUSSION,
          data: discussion.toJS()
        }
      })
    }
  }
}

export function deleteClubBookDiscussion(id) {
  return {
    [CALL_API]: {
        types: [DELETE_CLUB_BOOK_DISCUSSION, DELETE_CLUB_BOOK_DISCUSSION_SUCCESS, DELETE_CLUB_BOOK_DISCUSSION_FAILURE],
        endpoint: `/club_book_discussions/${id}/`,
        method: 'DELETE',
        data: {id, type: 'clubBookDiscussions'},
        schema: Schemas.CLUB_BOOK_DISCUSSION,
    }
  }
}