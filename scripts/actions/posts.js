import { CALL_API, Schemas } from '../middleware/api';
import { setProgressStatus } from './status';

export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const START_CREATING_POST = 'START_CREATING_POST';
export const EDIT_CREATING_POST = 'EDIT_CREATING_POST';
export const CANCEL_CREATING_POST = 'CANCEL_CREATING_POST';

export const CREATE_POST = 'CREATE_POST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const START_EDITING_POST = 'START_EDITING_POST';
export const EDIT_EDITING_POST = 'EDIT_EDITING_POST';
export const CANCEL_EDITING_POST = 'CANCEL_EDITING_POST';

export const EDIT_POST = 'EDIT_POST';
export const EDIT_POST_SUCCESS = 'EDIT_POST_SUCCESS';
export const EDIT_POST_FAILURE = 'EDIT_POST_FAILURE';

export const DELETE_POST = 'DELETE_POST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

export const REPORT_POST = 'REPORT_POST';
export const REPORT_POST_SUCCESS = 'REPORT_POST_SUCCESS';
export const REPORT_POST_FAILURE = 'REPORT_POST_FAILURE';

export const FETCH_NEW_POSTS = 'FETCH_NEW_POSTS';
export const FETCH_NEW_POSTS_SUCCESS = 'FETCH_NEW_POSTS_SUCCESS';
export const FETCH_NEW_POSTS_FAILURE = 'FETCH_NEW_POSTS_FAILURE';

export function fetchPosts(threadId) {
  return {
    [CALL_API]: {
      types: [FETCH_POSTS, FETCH_POSTS_SUCCESS, FETCH_POSTS_FAILURE],
      endpoint: `/threads/${threadId}/posts/`,
      method: 'get',
      schema: Schemas.POST_ARRAY
    }
  }
}

export function fetchNewPosts(threadId, has_posts) {
  return {
    [CALL_API]: {
      types: [FETCH_NEW_POSTS, FETCH_NEW_POSTS_SUCCESS, FETCH_NEW_POSTS_FAILURE],
      endpoint: `/threads/${threadId}/posts/`,
      method: 'get',
      schema: Schemas.POST_ARRAY,
      query: {has_posts},
      resetStatus: false,
      meta: {
        apiResponseAffectsStatus: false
      }
    }
  }
}

export function startCreatingPost(data) {
  return {
    type: START_CREATING_POST,
    data
  }
}

export function editCreatingPost(attr, value) {
  return {
    type: EDIT_CREATING_POST,
    attr,
    value
  }
}

export function cancelCreatingPost() {
  return {
    type: CANCEL_CREATING_POST
  }
}

export function createPost() {
  return function(dispatch, getState) {
    var creatingPost = getState().postDetails.get('creatingPost');
    dispatch(setProgressStatus('replyPost', creatingPost.get('threadId')))
    return dispatch({
      [CALL_API]: {
        types: [CREATE_POST, CREATE_POST_SUCCESS, CREATE_POST_FAILURE],
        endpoint: `/threads/${creatingPost.get('threadId')}/posts/`,
        method: 'post',
        schema: Schemas.POST,
        data: creatingPost.toJS(),
        meta: {
          parentError: `thread_${creatingPost.get('threadId')}`
        }
      }
    })
  }
}

export function startEditingPost(post) {
  return {
    type: START_EDITING_POST,
    post
  }
}

export function editEditingPost(attr, value) {
  return {
    type: EDIT_EDITING_POST,
    attr,
    value
  }
}

export function cancelEditingPost() {
  return {
    type: CANCEL_EDITING_POST
  }
}

export function editPost() {
  return function(dispatch, getState) {
    var post = getState().postDetails.get('editingPost')

    return dispatch({
      [CALL_API]: {
        types: [EDIT_POST, EDIT_POST_SUCCESS, EDIT_POST_FAILURE],
        endpoint: `/posts/${post.get('id')}/`,
        method: 'PATCH',
        schema: Schemas.POST,
        data: post.toJS(),
        meta: {
          parentError: `post_${post.get('id')}`
        }
      }
    })
  }
}

// Make sure to test this is removed from the store
export function deletePost(id) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('deletePost', id));
    return dispatch({
      [CALL_API]: {
        types: [DELETE_POST, DELETE_POST_SUCCESS, DELETE_POST_FAILURE],
        endpoint: `/posts/${id}/`,
        data: {id, type: 'posts'},
        method: 'DELETE',
        schema: Schemas.POST,
      }
    })
  }
}

export function reportPost(id) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('reportPost', id));
    return dispatch({
      [CALL_API]: {
        types: [REPORT_POST, REPORT_POST_SUCCESS, REPORT_POST_FAILURE],
        endpoint: `/posts/${id}/report/`,
        method: 'POST',
        schema: Schemas.POST,
      }
    })
  }
}
