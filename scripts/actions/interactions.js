import { CALL_API, Schemas } from '../middleware/api';
import { fetchClubBooks } from './club_books';

export const ACTIVATE = 'ACTIVATE';
export const DEACTIVATE = 'DEACTIVATE';

export function activate(entities) {
  return {
    type: ACTIVATE,
    entities
  }
}

export function deactivate(entities) {
  return {
    type: DEACTIVATE,
    entities
  }
}