import { CALL_API, Schemas } from '../middleware/api';
import { hidePopupForm } from './popups';
import Immutable from 'immutable';
import moment from 'moment';

export const EDIT_FEEDBACK = 'EDIT_FEEDBACK';

export const SUBMIT_FEEDBACK = 'SUBMIT_FEEDBACK';
export const SUBMIT_FEEDBACK_SUCCESS = 'SUBMIT_FEEDBACK_SUCCESS';
export const SUBMIT_FEEDBACK_FAILURE = 'SUBMIT_FEEDBACK_FAILURE';

export function editFeedback(message) {
  return {
    type: EDIT_FEEDBACK,
    message
  }
}

export function submitFeedback() {
  return function(dispatch, getState) {
    dispatch(hidePopupForm('feedback'));

    var state = getState();
    var stores = {};
    var data = {};

    data.message = state.feedbackDetails.get('message');
    data.time = moment().format();

    for(var attr in state) {
      if (state[attr]['toJS']) {
        stores[attr] = state[attr].toJS();
      }
    }

    data.stores = stores;
    // data.stores = {};

    return dispatch({
      [CALL_API]: {
        types: [SUBMIT_FEEDBACK, SUBMIT_FEEDBACK_SUCCESS, SUBMIT_FEEDBACK_FAILURE],
        endpoint: '/feedback/',
        method: 'POST',
        data
      }
    })
  }
}