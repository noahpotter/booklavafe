import { CALL_API, Schemas } from '../middleware/api';
import Immutable from 'immutable';
export * from './search';
export * from './calendar';
export * from './clubs';
export * from './users';
export * from './books';
export * from './club_books';
export * from './club_book_discussions';
export * from './club_discussions';
export * from './discussions';
export * from './threads';
export * from './posts';
export * from './interactions';
export * from './popups';
export * from './feedback';
export * from './pagination';
export * from './passwords';
export * from './user_details';
export * from './notifications';
export * from './status';
import { fetchUser, fetchUserAfterLogin } from './users'

export const INITIAL_STARTUP = 'INITIAL_STARTUP';

export function loadPage() {
  return (dispatch, getState) => {
    dispatch({
      type: INITIAL_STARTUP
    })

    if (getState().loggedUserDetails.get('id')) {
      dispatch(fetchUser(getState().loggedUserDetails.get('id')))
    }
  }
}

// export function login() {
//   return function(dispatch, getState) {
//     return dispatch({
//       type: LOGIN,
//       loginDetails: getState().loginDetails
//     })
//   }
// }