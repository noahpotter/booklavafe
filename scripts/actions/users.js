import { CALL_API, Schemas } from '../middleware/api';
import { setErrorStatus, setProgressStatus, resetProgressStatus } from './status';
import { push } from 'react-router-redux';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const UPDATE_LOGIN_DETAILS = 'UPDATE_LOGIN_DETAILS';
export const UPDATE_SEARCH_DETAILS = 'UPDATE_SEARCH_DETAILS';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT = 'LOGOUT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
export const CREATE_USER_FAILURE = 'CREATE_USER_FAILURE';

export const START_EDITING_USER = 'START_EDITING_USER';
export const EDIT_EDITING_USER = 'EDIT_EDITING_USER';
export const CANCEL_EDITING_USER = 'CANCEL_EDITING_USER';

export const EDIT_USER = 'EDIT_USER';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE';

// Fetches a single user
// Relies on the custom API middleware defined in ../middleware/api.js.
export function fetchUser(id) {
  return {
    [CALL_API]: {
      types: [FETCH_USER, FETCH_USER_SUCCESS, FETCH_USER_FAILURE],
      endpoint: `/users/${id}/`,
      method: 'get',
      schema: Schemas.USER
    }
  }
}

export function fetchUserAfterLogin() {
  return (dispatch, getState) => {
    return dispatch({
      [CALL_API]: {
        types: [FETCH_USER, {type: FETCH_USER_SUCCESS, next: push.bind(this, {pathname: '/'})}, FETCH_USER_FAILURE],
        endpoint: (state) => { return `/users/${state.loggedUserDetails.get('id')}/` },
        method: 'get',
        schema: Schemas.USER
      }
    })
  }
}


export function login(data) {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('login', true));
    return dispatch({
      [CALL_API]: {
        types: [LOGIN, {type: LOGIN_SUCCESS, next: fetchUserAfterLogin}, LOGIN_FAILURE],
        endpoint: `/api-token-auth/`,
        method: 'post',
        data: data
      }
    })
  }
}

export function logout() {
  return (dispatch, getState) => {
    dispatch(setProgressStatus('logout', true));
    return dispatch({
      [CALL_API]: {
        types: [LOGOUT, {type: LOGOUT_SUCCESS, next: push.bind(this, {pathname: '/'}) }, LOGOUT_FAILURE],
        endpoint: `/api-token-auth/`,
        method: 'delete',
        headers: {
          'Accept': 'application/json'
        },
        data: {id: getState().loggedUserDetails.get('id')}
      }
    })
  } 
}

// Fetches a single user unless it is cached.
// Relies on Redux Thunk middleware.
export function loadUser(login, requiredFields = []) {
  return (dispatch, getState) => {
    const user = getState().entities.users[login];
    if (user && requiredFields.every(key => user.hasOwnProperty(key))) {
      return null;
    }

    return dispatch(fetchUser(login));
  };
}

export function updateLoginDetails(attrs, value) {
  return {
    type: UPDATE_LOGIN_DETAILS,
    attrs,
    value
  }
}

export function createUser(user) {

  return function(dispatch, getState) {
    if (user.password != user.passwordConfirmation) {
      dispatch(resetProgressStatus());
      return dispatch(setErrorStatus('passwordConfirmation', 'Passwords do not match'));
    }

    dispatch(setProgressStatus('createUser', true));
    return dispatch({
      [CALL_API]: {
        types: [CREATE_USER, {type: CREATE_USER_SUCCESS, next: login.bind(this, user)}, CREATE_USER_FAILURE],
        endpoint: `/users/`,
        method: 'post',
        schema: Schemas.USER,
        data: user
      }
    })
  }
}

export function startEditingUser(user) {
  return {
    type: START_EDITING_USER,
    user
  }
}

export function editEditingUser(attr, value) {
  return {
    type: EDIT_EDITING_USER,
    attr,
    value
  }
}

export function cancelEditingUser() {
  return {
    type: CANCEL_EDITING_USER
  }
}

export function editUser() {
  return function(dispatch, getState) {
    dispatch(setProgressStatus('editProfile', true));
    var user = getState().userDetails.get('editingUser')

    if (user.get('newPassword') && user.get('newPassword') != user.get('passwordConfirmation')) {
      return dispatch(setErrorStatus('passwordConfirmation', 'Passwords do not match'));
    }

    return dispatch({
      [CALL_API]: {
        types: [EDIT_USER, EDIT_USER_SUCCESS, EDIT_USER_FAILURE],
        endpoint: `/users/${user.get('id')}/`,
        method: 'PATCH',
        schema: Schemas.USER,
        data: user.toJS()
      }
    })
  }
}