import { CALL_API, Schemas } from '../middleware/api';
import { fetchClubBooks } from './club_books';

export const CREATE_NOTIFICATION = 'CREATE_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

export function createSuccessNotification(config) {
  return createNotification(config.message, config.title || 'Success', 'success');
}

export function createErrorNotification(config) {
  return createNotification(config.message, config.title || 'Error', 'error');
}

export function createWarningNotification(config) {
  return createNotification(config.message, config.title || 'Warning', 'warning');
}

export function createNotification(message, title, level) {
  return {
    type: CREATE_NOTIFICATION,
    title,
    message,
    level
  }
}

export function removeNotification(notification) {
  return {
    type: REMOVE_NOTIFICATION,
    notification
  }
}