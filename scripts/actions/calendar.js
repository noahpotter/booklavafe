import { CALL_API, Schemas } from '../middleware/api';

export const CHANGE_CURRENT_MONTH = 'CHANGE_CURRENT_MONTH';

export function changeMonth(change) {
  return {
    type: CHANGE_CURRENT_MONTH,
    change
  }
}