import { CALL_API, Schemas } from '../middleware/api';
import { fetchClubBookDiscussions } from './club_book_discussions';
import { fetchBook } from './books';
import { resetErrorStatus, setErrors } from './status';
import { parseDate, momentToDate } from '../utils';
import moment from 'moment';
import Immutable from 'immutable';

export const FETCH_CLUB_BOOK = 'FETCH_CLUB_BOOK';
export const FETCH_CLUB_BOOK_SUCCESS = 'FETCH_CLUB_BOOK_SUCCESS';
export const FETCH_CLUB_BOOK_FAILURE = 'FETCH_CLUB_BOOK_FAILURE';

export const FETCH_CLUB_BOOKS = 'FETCH_CLUB_BOOKS';
export const FETCH_CLUB_BOOKS_SUCCESS = 'FETCH_CLUB_BOOKS_SUCCESS';
export const FETCH_CLUB_BOOKS_FAILURE = 'FETCH_CLUB_BOOKS_FAILURE';

export const START_EDITING_CLUB_BOOK = 'START_EDITING_CLUB_BOOK';
export const CANCEL_EDITING_CLUB_BOOK = 'CANCEL_EDITING_CLUB_BOOK';
export const EDIT_EDITING_CLUB_BOOK = 'EDIT_EDITING_CLUB_BOOK';

export const EDIT_CLUB_BOOK = 'EDIT_CLUB_BOOK';
export const EDIT_CLUB_BOOK_SUCCESS = 'EDIT_CLUB_BOOK_SUCCESS';
export const EDIT_CLUB_BOOK_FAILURE = 'EDIT_CLUB_BOOK_FAILURE';

export const DELETE_CLUB_BOOK = 'DELETE_CLUB_BOOK';
export const DELETE_CLUB_BOOK_SUCCESS = 'DELETE_CLUB_BOOK_SUCCESS';
export const DELETE_CLUB_BOOK_FAILURE = 'DELETE_CLUB_BOOK_FAILURE';

function handleFetchedClubBooks(clubId, includes, response) {
  return function(dispatch, getState) {
    var state = getState();

    state.entities.get('clubBooks').forEach(clubBook => {
      if (clubBook.get('club') == clubId) {
        dispatch(handleFetchedClubBook(clubBook.get('id'), includes, response));
      }
    });
  }
}

function handleFetchedClubBook(clubBookId, includes, response) {
  return function(dispatch, getState) {
    var state = getState();

    if (includes) {
      var immutableIncludes = Immutable.fromJS(includes);
      immutableIncludes = immutableIncludes.merge({'book': false, 'books': false, 'clubBookDiscussion': false, 'clubBookDiscussions': false});

      if (includes['book'] || includes['books']) {
        dispatch(fetchBook(state.entities.getIn(['clubBooks', clubBookId.toString()]).get('book'), immutableIncludes.toJS()));
      }

      if (includes['clubBookDiscussion'] || includes['clubBookDiscussions']) {
        dispatch(fetchClubBookDiscussions({clubBookId}, immutableIncludes.toJS()));
      }
    }
  }
}

export function fetchClubBook(data, includes) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB_BOOK, {type: FETCH_CLUB_BOOK_SUCCESS, next: handleFetchedClubBook.bind(this, data.clubBookId, includes)}, FETCH_CLUB_BOOK_FAILURE],
      endpoint: `/club_books/${data.clubBookId}/`,
      method: 'get',
      schema: Schemas.CLUB_BOOK
    }
  }
}

// Change all fetching to use (data, includes) format
export function fetchClubBooks(data, includes) {
  return function(dispatch, getState) {

    return dispatch({
      [CALL_API]: {
        types: [FETCH_CLUB_BOOKS, {type: FETCH_CLUB_BOOKS_SUCCESS, next: handleFetchedClubBooks.bind(this, data.clubId, includes)}, FETCH_CLUB_BOOKS_FAILURE],
        endpoint: `/clubs/${data.clubId}/club_books/`,
        method: 'get',
        query: data.query,
        schema: Schemas.CLUB_BOOK_ARRAY
      }
    })
  }
}

export function startEditingClubBook(clubBook) {
  return function(dispatch, getState) {

    dispatch(resetErrorStatus());

    return dispatch({
      type: START_EDITING_CLUB_BOOK,
      clubBook
    });
  }
}

export function editEditingClubBook(attr, value) {
  return {
    type: EDIT_EDITING_CLUB_BOOK,
    attr,
    value
  }
}

export function cancelEditingClubBook() {
  return {
    type: CANCEL_EDITING_CLUB_BOOK
  }
}

export function editClubBook() {
  return function(dispatch, getState) {
    dispatch(resetErrorStatus());

    var clubBook = getState().clubBookDetails.get('editingClubBook')

    var startDate = parseDate(moment(clubBook.get('startDate')));
    var endDate = parseDate(moment(clubBook.get('endDate')));

    var errors = {};
    var hasError = false;

    if (!startDate.isValid()) {
      errors['startDate'] = 'Format should be MM/DD/YYYY';
      hasError = true;
    }

    if (!endDate.isValid()) {
      errors['endDate'] = 'Format should be MM/DD/YYYY';
      hasError = true;
    }

    if (hasError) {
      return dispatch(setErrors(errors));
    } else {
      clubBook = clubBook.set('startDate', momentToDate(startDate.utc()))
      clubBook = clubBook.set('endDate', momentToDate(endDate.utc()))

      return dispatch({
        [CALL_API]: {
          types: [EDIT_CLUB_BOOK, EDIT_CLUB_BOOK_SUCCESS, EDIT_CLUB_BOOK_FAILURE],
          endpoint: `/club_books/${clubBook.get('id')}/`,
          method: 'PATCH',
          schema: Schemas.CLUB_BOOK,
          data: clubBook.toJS()
        }
      })
    }
  }
}

export function deleteClubBook(id) {
  return {
    [CALL_API]: {
      types: [DELETE_CLUB_BOOK, DELETE_CLUB_BOOK_SUCCESS, DELETE_CLUB_BOOK_FAILURE],
      endpoint: `/club_books/${id}/`,
      data: {id, type: 'clubBooks'},
      method: 'DELETE'
    }
  }
}