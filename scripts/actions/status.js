import { CALL_API, Schemas } from '../middleware/api';

export const RESET_STATUS = 'RESET_STATUS';
export const RESET_ERROR_STATUS = 'RESET_ERROR_STATUS';
export const RESET_PROGRESS_STATUS = 'RESET_PROGRESS_STATUS';
export const SET_ERROR_STATUS = 'SET_ERROR_STATUS';
export const SET_PROGRESS_STATUS = 'SET_PROGRESS_STATUS';
export const SET_ERRORS = 'SET_ERRORS';

export function setErrorStatus(attr, message) {
  return {
    type: SET_ERROR_STATUS,
    attr,
    message
  }
}

export function setProgressStatus(attr, message) {
  return {
    type: SET_PROGRESS_STATUS,
    attr,
    message
  }
}

export function setErrors(errors) {
  return {
    type: SET_ERRORS,
    errors
  }
}

export function resetErrorStatus() {
  return {
    type: RESET_ERROR_STATUS,
  }
}

export function resetProgressStatus() {
  return {
    type: RESET_PROGRESS_STATUS,
  }
}
