import { CALL_API, Schemas } from '../middleware/api';
import moment from 'moment';
window.moment = moment;

export const CREATE_CLUB_DISCUSSION = 'CREATE_CLUB_DISCUSSION';
export const CREATE_CLUB_DISCUSSION_SUCCESS = 'CREATE_CLUB_DISCUSSION_SUCCESS';
export const CREATE_CLUB_DISCUSSION_FAILURE = 'CREATE_CLUB_DISCUSSION_FAILURE';

export const FETCH_CLUB_DISCUSSIONS = 'FETCH_CLUB_DISCUSSIONS';
export const FETCH_CLUB_DISCUSSIONS_SUCCESS = 'FETCH_CLUB_DISCUSSIONS_SUCCESS';
export const FETCH_CLUB_DISCUSSIONS_FAILURE = 'FETCH_CLUB_DISCUSSIONS_FAILURE';

export const FETCH_CLUB_DISCUSSION = 'FETCH_CLUB_DISCUSSION';
export const FETCH_CLUB_DISCUSSION_SUCCESS = 'FETCH_CLUB_DISCUSSION_SUCCESS';
export const FETCH_CLUB_DISCUSSION_FAILURE = 'FETCH_CLUB_DISCUSSION_FAILURE';

export const START_CREATING_CLUB_DISCUSSION = 'START_CREATING_CLUB_DISCUSSION';
export const EDIT_CREATING_CLUB_DISCUSSION = 'EDIT_CREATING_CLUB_DISCUSSION';
export const CANCEL_CREATING_CLUB_DISCUSSION = 'CANCEL_CREATING_CLUB_DISCUSSION';

export function fetchClubDiscussion(data) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB_DISCUSSION, FETCH_CLUB_DISCUSSION_SUCCESS, FETCH_CLUB_DISCUSSION_FAILURE],
      endpoint: `/club_discussions/${data.clubDiscussionId}/`,
      method: 'GET',
      schema: Schemas.CLUB_DISCUSSION
    }
  }
}

export function fetchClubDiscussions(data) {
  return {
    [CALL_API]: {
      types: [FETCH_CLUB_DISCUSSIONS, FETCH_CLUB_DISCUSSIONS_SUCCESS, FETCH_CLUB_DISCUSSIONS_FAILURE],
      endpoint: `/clubs/${data.clubId}/discussions/`,
      method: 'GET',
      schema: Schemas.CLUB_DISCUSSION_ARRAY
    }
  }
}

export function startCreatingClubDiscussion() {
  return {
    type: START_CREATING_CLUB_DISCUSSION
  }
}

export function editCreatingClubDiscussion(attr, value) {
  return {
    type: EDIT_CREATING_CLUB_DISCUSSION,
    attr,
    value
  }
}

export function cancelCreatingClubDiscussion() {
  return {
    type: CANCEL_CREATING_CLUB_DISCUSSION
  }
}

export function createClubDiscussion(clubId) {
  return function(dispatch, getState) {
    var discussion = getState().discussionDetails.get('creatingClubDiscussion')

    // Fix startDate and endDate formats
    var startDate = discussion.get('startDate');
    var startTime = discussion.get('startTime');
    discussion = discussion.set('startDate', moment(`${startDate} ${startTime}`, 'MM/DD/YYYY LT').utc().format('YYYY-MM-DDTHH:mm'))
    discussion = discussion.set('clubId', clubId)

    return dispatch({
      [CALL_API]: {
        types: [CREATE_CLUB_DISCUSSION, CREATE_CLUB_DISCUSSION_SUCCESS, CREATE_CLUB_DISCUSSION_FAILURE],
        endpoint: `/club_discussions/`,
        method: 'POST',
        schema: Schemas.CLUB_DISCUSSION,
        data: discussion.toJS()
      }
    })
  }
}