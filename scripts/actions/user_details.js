import { CALL_API, Schemas } from '../middleware/api';
import { setErrorStatus, resetProgressStatus } from './status';

export const FETCH_USER_DETAIL = 'FETCH_DETAIL_USER';
export const FETCH_USER_DETAIL_SUCCESS = 'FETCH_USER_DETAIL_SUCCESS';
export const FETCH_USER_DETAIL_FAILURE = 'FETCH_USER_DETAIL_FAILURE';

export const START_CREATING_USER_DETAIL = 'START_CREATING_USER_DETAIL';
export const EDIT_CREATING_USER_DETAIL = 'EDIT_CREATING_USER_DETAIL';
export const CANCEL_CREATING_USER_DETAIL = 'CANCEL_CREATING_USER_DETAIL';

export const START_EDITING_USER_DETAIL = 'START_EDITING_USER_DETAIL';
export const EDIT_EDITING_USER_DETAIL = 'EDIT_EDITING_USER_DETAIL';
export const CANCEL_EDITING_USER_DETAIL = 'CANCEL_EDITING_USER_DETAIL';

export const EDIT_USER_DETAIL = 'EDIT_USER_DETAIL';
export const EDIT_USER_DETAIL_SUCCESS = 'EDIT_USER_DETAIL_SUCCESS';
export const EDIT_USER_DETAIL_FAILURE = 'EDIT_USER_DETAIL_FAILURE';

// Fetches a single user
// Relies on the custom API middleware defined in ../middleware/api.js.
export function fetchUserDetail(id) {
  return {
    [CALL_API]: {
      types: [FETCH_USER_DETAIL, FETCH_USER_DETAIL_SUCCESS, FETCH_USER_DETAIL_FAILURE],
      endpoint: `/user_details/${id}/`,
      method: 'get',
      schema: Schemas.USER_DETAIL
    }
  }
}

export function startEditingUserDetail(userDetail) {
  return {
    type: START_EDITING_USER_DETAIL,
    userDetail
  }
}

export function editEditingUserDetail(attr, value) {
  return {
    type: EDIT_EDITING_USER_DETAIL,
    attr,
    value
  }
}

export function cancelEditingUserDetail() {
  return {
    type: CANCEL_EDITING_USER_DETAIL
  }
}

export function editUserDetail(args) {
  return function(dispatch, getState) {
    var state = getState();

    var userDetail = getState().userDetailDetails.get('editingUserDetail')
    var user = getState().userDetails.get('editingUser')
    userDetail = userDetail.set('password', user.get('password'));

    if (user.get('newPassword') && user.get('newPassword') != user.get('passwordConfirmation')) {
      dispatch(resetProgressStatus());
      return dispatch(setErrorStatus('passwordConfirmation', 'Passwords do not match'));
    }

    console.log('set password to ', userDetail.get('password'))

    var image = userDetail.get('image');
      
    var data = userDetail;
    
    if (image instanceof Array && image[0] instanceof File) {
      data = new FormData()
      data.append('image', userDetail.get('image')[0])
      data.append('password', userDetail.get('password'))
    } else {
      data = data.remove('image');
      data = data.toJS();
    }

    return dispatch({
      [CALL_API]: {
        types: [EDIT_USER_DETAIL, EDIT_USER_DETAIL_SUCCESS, EDIT_USER_DETAIL_FAILURE],
        endpoint: `/user_details/${userDetail.get('id')}/`,
        method: 'PATCH',
        schema: Schemas.USER_DETAIL,
        data,
        ...args
      }
    })
  }
}