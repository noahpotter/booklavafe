import { CALL_API, Schemas } from '../middleware/api';

export const SET_PAGE = 'SET_PAGE';

export function setPage(pageType, page) {
  return {
    type: SET_PAGE,
    pageType,
    page
  }
}
