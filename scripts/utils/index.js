import Immutable from 'immutable';
import moment from 'moment';
export * from './clubs';
export * from './search';
export * from './math';
export * from './sort';
export * from './date';
export * from './clubBook';
export * from './interaction';

export function getLoggedUser() {
  const state = window.store.getState();
  const userId = state.loggedUserDetails.get('id');

  if (userId) {
    return state.entities.getIn(['users', userId.toString()])
  }
}

export function hasLoggedUser(loggedUserDetails) {
  var id = loggedUserDetails.get('id');
  return id;
}

export function userHasOwnedClubs(user) {
  if (user) {
    return user.get('ownedClubs').size > 0;
  }
  
  return false;
}

// http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport/7557433#7557433
export function isElementInVisibleInParent (el) {
    var childRect = el.getBoundingClientRect();
    var parentRect = el.parentNode.getBoundingClientRect();

    return (
        childRect.top >= parentRect.top &&
        childRect.bottom <= parentRect.bottom &&
        childRect.left >= parentRect.left &&
        childRect.right <= parentRect.right
    );
}