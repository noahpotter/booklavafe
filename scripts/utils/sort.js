import moment from 'moment';

export function sortAscDate(objects, attr) {
  return objects.sort((o1, o2) => {
      if (moment(o1.get(attr)).isBefore(moment(o2.get(attr)))) {
        return -1
      } else if (moment(o1.get(attr)).isAfter(moment(o2.get(attr)))) {
        return 1
      } else {
        return 0
      }
    }
  )
}

export function sortDescDate(objects, attr) {
  return objects.sort((o1, o2) => {
      if (moment(o1.get(attr)).isAfter(moment(o2.get(attr)))) {
        return -1
      } else if (moment(o1.get(attr)).isBefore(moment(o2.get(attr)))) {
        return 1
      } else {
        return 0
      }
    }
  )
}

export function sortAscAlpha(objects, attr) {
  return objects.sort((o1, o2) => {
    return o1.get(attr).toLowerCase().localeCompare(o2.get(attr).toLowerCase());
  })
}