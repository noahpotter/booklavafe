// numbers is a sorted array of numbers between 0 and 100
// this function makes it so that two numbers are at least a min distance apart
// by default we don't take space before the first number and after the last number
function allAtLeastMin(numbers, minDistance) {
  for(var i=0; i<numbers.length; i++) {
    if (numbers[i] < minDistance) {
      return false;
    }
  }

  return true;
}


// assert numbers has more than 1 element
// sorry this is so confusing, basically this scales down extra space after making all differences at least the minimum
export function addMinDistance(numbers, minDistance) {
  const initialValue = numbers[0];
  const allowableSize = 100 - numbers[0];
  const baseSize = (numbers.length-1) * minDistance;
  var currentSize = 0;
  var differences = [];

  for(var i=1; i<numbers.length; i++) {
    const difference = numbers[i] - numbers[i-1];
    if (difference < minDistance) {
      differences.push(minDistance)
      currentSize += minDistance;
    } else {
      differences.push(difference);
      currentSize += difference;
    }
  }

  const allowableExtra = allowableSize - baseSize;
  const currentExtra = currentSize - baseSize;

  if (currentSize > allowableSize) {
    var ratio = allowableExtra / currentExtra;

    for(var i=0; i<differences.length; i++) {
      // Final difference is the minDistance plus the scaled down extra space
      differences[i] = minDistance + (differences[i] - minDistance) * ratio;
    }
  }

  numbers = [initialValue];
  for(var i=0; i<differences.length; i++) {
    numbers.push(numbers[i] + differences[i])
  }
  
  return numbers;
}
