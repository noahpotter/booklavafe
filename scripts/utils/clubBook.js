import { dateToMoment } from './';
import moment from 'moment';

export function currentlyReadingBook(clubBook) {
  return dateToMoment(clubBook.get('startDate')).isBefore(moment()) && dateToMoment(clubBook.get('endDate')).isAfter(moment());
}