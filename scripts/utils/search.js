export function getBookFromQuery(query) {
  var query_parts = query.split(' ');
  if (query_parts.length > 0 && query_parts[0] == 'book:') {
    query_parts.splice(0, 1)
    return query_parts.join(' ');
  }

  return '';
}

export function hasNewSearch() {
  const state = window.store.getState();
  const searchDetails = state.searchDetails;
  const pageDetails = state.pageDetails;

  const query = {
    query: searchDetails.get('query'),
    type: searchDetails.get('type'),
    page: pageDetails.getIn(['page', 'search'])
  }

  const result = query.query && (
    query.query != searchDetails.getIn(['lastSearch', 'query']) || 
    query.type != searchDetails.getIn(['lastSearch', 'type']) || 
    query.page != searchDetails.getIn(['lastSearch', 'page']));

  return result;
}