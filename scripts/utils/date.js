import moment from 'moment';

export function discussionDateTime(date) {
  return moment(date).format('M/D/YYYY - h:mm A');
}

export function momentFormatDate(date) {
  if (date) {
    return moment(date).format('MM/DD/YYYY');
  }
}

export function dateToMoment(date) {
  if (date) {
    return moment(date)
  }
}

export function parseDate(date) {
  return moment(date, ['M/D/YYYY', 'M/D/YY'], true);
}

export function parseTime(time) {
  return moment(time, ['h:m a', 'H:m'], true);
}

export function momentToDate(date) {
  if (date) {
    return date.format('YYYY-MM-DDTHH:mm');
  }
}