import { dateToMoment } from './date';
import { getBookFromQuery } from './search';

export function isCurrentlyReadingBook(club, entities, query) {
  if (club && entities && query) {
    var book = getBookFromQuery(query);

    if (book != '') {
      var isCurrentlyReading = false;
      const clubClubBooks = entities.get('clubBooks').filter(clubBook => clubBook.get('club') == club.get('id'));
      clubClubBooks.forEach(clubBook => {
        const clubBookBookName = entities.getIn(['books', clubBook.get('book').toString()]).get('name');
        if (!isCurrentlyReading && clubBookBookName.toLowerCase() == book.toLowerCase() && dateToMoment(clubBook.get('startDate')).isBefore(moment()) && dateToMoment(clubBook.get('endDate')).isAfter(moment())) {

          isCurrentlyReading = true;
        }
      })

      return isCurrentlyReading;
    }
  }

  return false;
}