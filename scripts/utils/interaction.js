export function getActiveEntity(entities, interactionDetails, type) {
  const id = interactionDetails.getIn(['active', type]);
  if (id) {
    var entity = entities.getIn([type + 's', id.toString()]);
    return entity;
  }
}
