// import 'babel-core/polyfill';
import React from 'react';
import { render } from 'react-dom';
import App from './App';
import Root from './Root';
import { AppContainer } from 'react-hot-loader';

render(
  <AppContainer>
    <Root />
  </AppContainer>,
  document.getElementById('root')
);

 if (module.hot) {
  console.log('hot')
  module.hot.accept('./Root', () => {
    console.log('hotter')
    const NextRoot = require('./Root').default;
    render(
      <AppContainer>
        <NextRoot />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}

// export const store = configureStore({
//   entities: Immutable.fromJS({
//     threads: {
//       // 0: {id: 0, discussionId: 0, originalPost: 0},
//       // '-1': {id: -1, discussionId: 0, originalPost: -3},
//       // '-2': {id: -2, discussionId: 0, originalPost: -5}
//     },
//     posts: {
//       // 0: {id: 0, thread: 0, parentPost: null, content: 'Replying', nestedLevel: 0}, 
//       // '-1': {id: -1, thread: 0, parentPost: 0, content: 'Post 1', nestedLevel: 0}, 
//       // '-2': {id: -2, thread: 0, parentPost: -1, content: 'Reply to post 1', nestedLevel: 1}, 
//       // '-3': {id: -3, thread: -1, parentPost: null, content: 'Normal', nestedLevel: 0},
//       // '-4': {id: -4, thread: -1, parentPost: -3, content: 'Post 1', nestedLevel: 0},
//       // '-5': {id: -5, thread: -2, parentPost: null, content: 'View hierarchy', nestedLevel: 0},
//       // '-6': {id: -6, thread: -2, parentPost: -5, content: '1', nestedLevel: 0},
//       // '-7': {id: -7, thread: -2, parentPost: -6, content: '1.1', nestedLevel: 1},
//       // '-8': {id: -8, thread: -2, parentPost: -7, content: '1.1.1', nestedLevel: 2},
//       // '-13': {id: -13, thread: -2, parentPost: -8, content: '1.1.1.2', nestedLevel: 3},
//       // '-9': {id: -9, thread: -2, parentPost: -8, content: '1.1.1.1', nestedLevel: 3},
//       // '-12': {id: -12, thread: -2, parentPost: -9, content: '1.1.1.1.1', nestedLevel: 4},
//       // '-10': {id: -10, thread: -2, parentPost: -5, content: '2', nestedLevel: 0},
//       // '-11': {id: -11, thread: -2, parentPost: -10, content: '2.1', nestedLevel: 1}
//     },
//     books: {
//       // 0: {id: 0, name: 'Robot, I', author: 'H.P. Lovecraft', description: 'Lots of pages to be enjoyed here. Immerse yourself in a world of adventure and fantasy.'},
//       // '-1': {id: -1, name: 'Harry Potter', author: 'Gerbert', description: 'Dark mystery. Prepare to be thrilled.'},
//       // '-2': {id: -2, name: 'The Foundation', author: 'Upstein G.', description: 'Not much is know about this. History was just beginning.'}
//     },
//     clubs: {
//       // 0: {id: 0, president: 0, members: [0], name: 'Fantasy Fighters', description: 'This is the fantasy clubs description' },
//       // '-1': {id: -1, president: -1, members: [-1], name: 'History Heroics', description: 'This is the history clubs description' },
//       // '-2': {id: -2, president: -2, members: [-2], name: 'Fahrenheit Foes', description: 'This is the fahrenheit clubs description' }
//     },
//     clubBooks: {
//       // 0: {id: 0, book: 0, club: 0, startDate: '', endDate: '', startMonth: 'Nov', startDay: 6 },
//       // '-1': {id: -1, book: 0, club: -1, startDate: '', endDate: '', startMonth: 'Nov', startDay: 6 }
//     },
//     clubBookDiscussions: {
//       // 0: {id: 0, clubBook: 0, index: 1, startDate: '10/4/2015' },
//       // '-1': {id: -1, clubBook: 0, index: 2, startDate: '9/4/2015' },
//       // '-2': {id: -2, clubBook: -1, index: 1, startDate: '9/4/2015' }
//     },
//     clubDiscussions: {
//     },
//     users: {
//       // 0: {id: 0, username: 'John Sno', email: 'sno@winter.com', ownedClubs: [0], subscribedClubs: [-1]},
//       // '-1': {id: -1, username: 'Allex Carr', email: 'all@ex.com', ownedClubs: [-1], subscribedClubs: [-1]},
//       // '-2': {id: -2, username: 'Yoj', email: 'yoj@mej.com', ownedClubs: [-2], subscribedClubs: [-1]}
//     },
//     userDetails: {
      
//     },
//     clubMemberships: {

//     }
//   }),
//   // discussionDetails: Immutable.fromJS({
//     // typingNewThread: true,
//     // replyingToPost: -2,
//     // activeThreads: {
//     //   0: {id: 0, color: 0},
//     //   '-2': {id: -2, color: 1}
//     // },
//     // showHierarchy: {
//     //   '-2': {thread: -2, post: -12}
//     // },
//     // colors: ['#23BCFF', '#A1E3FF', '#6BA6BF', '#017BB1', '#29708F']
//   // }),
//   // loginDetails: Immutable.fromJS({
//     // username: '',
//     // email: '',
//     // password: '',
//     // passwordConfirmation: ''
//   // }),
//   // searchDetails: Immutable.fromJS({
//   //   query: '',
//   //   type: 'books', // books, clubs, people
//   //   selectedId: 0,
//   //   books: [0, -1, -2],
//   //   clubs: [0],
//   //   users: [0],
//   //   isSearching: false
//   // }),
//   addBookDetails: Immutable.fromJS({
//     showAddBookToClub: false,
//     clubId: null,
//     bookId: null,
//     endDate: '',
//   }),
//   // calendarDetails: Immutable.fromJS({
//   //   currentMonth: 0
//   // }),
//   // loggedUserDetails: Immutable.fromJS({
//   //   id: 0,
//   //   apiKey: ''
//   // })
// });

// render(
//   <Provider store={store}>
//     <Root />
//   </Provider>,
//   document.getElementById('root')
// );