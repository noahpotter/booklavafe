import Immutable from 'immutable'
import { push } from 'react-router-redux';
import * as actions from '../actions'

const HISTORY_API = '@@router/CALL_HISTORY_METHOD'

// export default store => next => action => {
export function authorize(store) {
  return next => action => {

    const guestAuthorizedRoutes = Immutable.fromJS(['/login', '/register', '/passwords/forgot', '/passwords/reset'])
    const loggedInUnauthorizedRoutes = Immutable.fromJS(['/login', '/register', '/passwords/forgot', '/passwords/reset'])

    const loggedUserRedirect = '/';
    const guestRedirect = '/login';

    var targetPath = '';
    if (action.type == HISTORY_API) {
      targetPath = action.payload.args[0].pathname
    } else if (action.type == actions.INITIAL_STARTUP) {
      targetPath = window.location.pathname;
    }

    const loggedUserId = store.getState().loggedUserDetails.get('id');
    const shouldRedirectLoggedUser = loggedUserId && loggedInUnauthorizedRoutes.includes(targetPath);
    const shouldRedirectGuest = !loggedUserId && !guestAuthorizedRoutes.includes(targetPath);

    // Attempt to simply inject the corrected route, needs to make sure this middleware is called first?
    // Opposed to store.dispatch
    if (action.type == HISTORY_API) {
      if (shouldRedirectLoggedUser) {
        action.payload.args[0].pathname = loggedUserRedirect;
        return next(push(action.payload.args[0]))
      } else if (shouldRedirectGuest) {
        action.payload.args[0].pathname = guestRedirect;
        return next(push(action.payload.args[0]))
      }
    } else if (action.type == actions.INITIAL_STARTUP) {
      if (shouldRedirectLoggedUser) {
        return next(push({pathname: loggedUserRedirect}))
      } else if (shouldRedirectGuest) {
        return next(push({pathname: guestRedirect}))
      }
    }
    
    if (action.type == HISTORY_API) {
      next({type: 'RESET_STATUS'})
    }
    
    return next(action);
  }
};