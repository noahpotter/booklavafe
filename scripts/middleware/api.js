import { Schema, arrayOf, normalize } from 'normalizr';
import { camelizeKeys, decamelizeKeys } from 'humps';
import config from 'config';
import 'isomorphic-fetch';
import url from 'url';
import ImmutableJS from 'immutable';
import * as ActionTypes from '../actions';

window.url = url;

const API_ROOT = config.BACKEND;

var appFetch = fetch;

export function setFetch(newFetch) {
  appFetch = newFetch;
}

function getUrl(endpoint, query) {
  if (endpoint.indexOf(API_ROOT) === -1) {
    endpoint = API_ROOT + endpoint;
  }

  var urlObj = url.parse(endpoint);
  urlObj.query = query;

  return url.format(urlObj);
}

function textStatus(response) {
  let text = response.text();
  if (response.status >= 200 && response.status < 300) {
    return text.then(text => {
      return {
        text,
        response
      }
    })
  } else {
    return text.then(Promise.reject.bind(Promise));
  }
}

function jsonStatus(response) {
  let json = response.json();
  if (response.status >= 200 && response.status < 300) {
    return json.then(json => {
      return {
        json,
        response
      }
    })
  } else {
    return json.then(json => {
      Promise.reject.bind(Promise)
      return Promise.reject(camelizeKeys(json));
    });
  }
}

function camelize(schema, jsonResponse) {
  if (jsonResponse.json) {
    console.log("got json", jsonResponse, jsonResponse.json);
    const camelizedJson = camelizeKeys(jsonResponse.json);

    if (schema) {
      return {
        rawData: camelizedJson,
        ...normalize(camelizedJson, schema)
      };
    } else {
      return camelizedJson;
    }
  } else {
    console.log("didnt get json", jsonResponse, jsonResponse.json);
    return {};
  }

}

// function expectsText(method, headers) {
//   return false;
//   if (headers) {
//     return !headers['Accept'] && method.toLowerCase() == 'delete';
//   } else if (schema) {
//     return method.toLowerCase() == 'delete';
//   }
// }

function getHeaders(headers, apiKey, isFormData, method) {
  var defaultHeaders = 
    {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

  // if (expectsText(method, headers)) {
  //     headers['Accept'] = 'text/html';
  // }

  headers = ImmutableJS.fromJS(defaultHeaders).merge(headers);
  if (isFormData) {
    headers = headers.remove('Content-Type');
    // headers = headers.set('Content-Type', 'multipart/form-data');
  }
  headers = headers.filter((value, key) => { return value != null });
  headers = headers.toJS();

  if (apiKey) {
    headers['Authorization'] = 'Token ' + apiKey
  }

  return headers;
}

function fetchFromApi(object) {
  var data = object.data;
  var isFormData = object.data instanceof FormData;

  if (!isFormData) {
    data = JSON.stringify(decamelizeKeys(object.data));
  }

  var objectToSend = {
    method: object.method, 
    headers: getHeaders(object.headers, object.apiKey, isFormData, object.method), 
    body: data,
    files: object.files,
    query: object.query
  }

  console.log('Sending data to:', objectToSend.method, object.endpoint, '\nbody:', objectToSend.body, '\nheaders:', objectToSend.headers, '\nquery:', objectToSend.query);

  return appFetch(getUrl(object.endpoint, object.query), objectToSend);
}

const clubSchema = new Schema('clubs');
const userSchema = new Schema('users');
const clubBookSchema = new Schema('clubBooks');
const clubMembershipSchema = new Schema('clubMemberships');
const bookSchema = new Schema('books');
const clubBookDiscussionSchema = new Schema('clubBookDiscussions');
const clubDiscussionSchema = new Schema('clubDiscussions');
const threadSchema = new Schema('threads');
const postSchema = new Schema('posts');
const userDetailSchema = new Schema('userDetails');

userSchema.define({
  userDetail: userDetailSchema
})

clubSchema.define({
  president: userSchema,
  members: arrayOf(userSchema),
  clubBooks: arrayOf(clubBookSchema),
  clubDiscussions: arrayOf(clubDiscussionSchema)
});

clubBookSchema.define({
  club: clubSchema,
  book: bookSchema,
  clubBookDiscussions: arrayOf(clubBookDiscussionSchema)
})

clubMembershipSchema.define({ 
  club: clubSchema,
  user: userSchema
})

clubBookDiscussionSchema.define({
  clubBook: clubBookSchema,
})

clubDiscussionSchema.define({
  club: clubSchema,
})

threadSchema.define({
  clubBookDiscussion: clubBookDiscussionSchema,
  clubDiscussion: clubDiscussionSchema,
  originalPost: postSchema
})

postSchema.define({
  
})

export const Schemas = {
  USER: userSchema,
  USER_ARRAY: arrayOf(userSchema),
  USER_DETAIL: userDetailSchema,
  USER_DETAIL_ARRAY: arrayOf(userDetailSchema),
  CLUB: clubSchema,
  CLUB_ARRAY: arrayOf(clubSchema),
  CLUB_BOOK: clubBookSchema,
  CLUB_BOOK_ARRAY: arrayOf(clubBookSchema),
  CLUB_MEMBERSHIP: clubMembershipSchema,
  CLUB_MEMBERSHIP_ARRAY: arrayOf(clubMembershipSchema),
  BOOK: bookSchema,
  BOOK_ARRAY: arrayOf(bookSchema),
  CLUB_BOOK_DISCUSSION: clubBookDiscussionSchema,
  CLUB_BOOK_DISCUSSION_ARRAY: arrayOf(clubBookDiscussionSchema),
  CLUB_DISCUSSION: clubDiscussionSchema,
  CLUB_DISCUSSION_ARRAY: arrayOf(clubDiscussionSchema),
  THREAD: threadSchema,
  THREAD_ARRAY: arrayOf(threadSchema),
  POST: postSchema,
  POST_ARRAY: arrayOf(postSchema)
};

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('Call API');

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export function api(store) {
  return next => action => {

    const callAPI = action[CALL_API];
    if (typeof callAPI === 'undefined') {
      return next(action);
    }

    let { endpoint } = callAPI;
    const { schema, types } = callAPI;

    if (typeof endpoint === 'function') {
      endpoint = endpoint(store.getState());
    }

    if (typeof endpoint !== 'string') {
      throw new Error('Specify a string endpoint endpoint.');
    }
    // if (!schema) {
    //   throw new Error('Specify one of the exported Schemas.');
    // }
    if (!Array.isArray(types) || types.length !== 3) {
      throw new Error('Expected an array of three action types.');
    }
    // if (!types.every(type => typeof type === 'string')) {
    //   throw new Error('Expected action types to be strings.');
    // }
    function actionWith(data) {
      const finalAction = Object.assign({}, action, data);
      delete finalAction[CALL_API];
      return finalAction;
    }

    if (callAPI.resetStatus == undefined || callAPI.resetStatus) { // Default to reseting the status unless called with resetStatus set
      next(actionWith({ type: ActionTypes.RESET_ERROR_STATUS }))
    }
    next(actionWith({ type: types[0], initialActionData: callAPI.initialActionData }));

    var apiKey;
    if (store.getState().loggedUserDetails) {
      apiKey = store.getState().loggedUserDetails.get('apiKey');
    }

    var configuration = {
        endpoint, 
        types: callAPI.types,
        method: callAPI.method,
        headers: {...callAPI.headers},
        apiKey,
        data: callAPI.data,
        form_data: callAPI.form_data,
        query: callAPI.query,
        files: callAPI.files,
        meta: callAPI.meta,
        schema: callAPI.schema
      };

    var fetchResult = fetchFromApi(configuration);

    // if (expectsText(callAPI.method, callAPI.headers)) {
    //   fetchResult = fetchResult
    //     .then(textStatus);
    // } else {
      fetchResult = fetchResult
        .then(jsonStatus)
        .then(camelize.bind(this, schema));
    // }

    fetchResult = fetchResult.then(
      response => {
        var toSend = {
          response,
          configuration,
          previousState: store.getState(),
          type: types[1].type || types[1]
        }

        console.log('Got response to ', callAPI.method, endpoint, response)

        next(actionWith(toSend))

        if (types[1].handle) {
          store.dispatch(types[1].handle(response))
        }

        if (types[1].next) {
          store.dispatch(types[1].next(response))
        }

      },
      error => {
        var toSend = {
          error: error || 'Something bad happened',
          configuration,  
          previousState: store.getState(),
          type: types[2].type || types[2],
        }

        console.log('Got error to', callAPI.method, endpoint, error);

        next(actionWith(toSend));

        if (types[2].next) {
          store.dispatch(types[2].next(error))
        }

        if (types[2].handle) {
          if (error.detail) {
            error.detail = JSON.parse(error.detail[0])
            error = Object.assign(error, error.detail);
          }

          if (error.nonFieldErrors) {
            error.nonFieldErrors = JSON.parse(error.nonFieldErrors[0])
            error = Object.assign(error, error.nonFieldErrors);
          }
          
          types[2].handle(error);
        }
      }
    )

    return fetchResult;
  }
};

window.fetchFromApi = fetchFromApi;
window.logOutResponse = response => response.json().then(json => console.log(response, json))
