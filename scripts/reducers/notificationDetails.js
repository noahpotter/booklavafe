import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'
import store from 'store'

var idCounter = 1;

export function notificationDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      notifications: {}
    });
  }

  switch(action.type) {
    case ActionTypes.CREATE_NOTIFICATION:
      const id = idCounter++;
      const notification = Immutable.fromJS({'uid': id, 'title': action.title, 'message': action.message, 'level': action.level});
      return state.setIn(['notifications', id], notification);
    case ActionTypes.REMOVE_NOTIFICATION:
      return state.removeIn(['notifications', action.notification.uid]);
    default:
      return state;
  }
}