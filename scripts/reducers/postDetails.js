import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'

export function postDetails(state, action) {
  function defaultPostDetails() {
    return {
      threadId: null,
      parentPostId: null,
      content: ''
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      creatingPost: null,
      editingPost: null
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_POST:
      var creatingPost = Immutable.fromJS(defaultPostDetails());
      creatingPost = creatingPost.merge(action.data);
      return state.merge({creatingPost: creatingPost, editingPost: null})
    case ActionTypes.CANCEL_CREATING_POST:
      return state.merge({creatingPost: null})
    case ActionTypes.EDIT_CREATING_POST:
      return state.setIn(['creatingPost', action.attr], action.value)
    case ActionTypes.CREATE_POST_SUCCESS:
      return state.remove('creatingPost');
    case ActionTypes.START_EDITING_POST:
      return state.merge({editingPost: action.post, creatingPost: null})
    case ActionTypes.CANCEL_EDITING_POST:
      return state.merge({editingPost: null})
    case ActionTypes.EDIT_EDITING_POST:
      return state.setIn(['editingPost', action.attr], action.value)
    case ActionTypes.EDIT_POST_SUCCESS:
      return state.remove('editingPost');
    default:
      return state;
  }
}