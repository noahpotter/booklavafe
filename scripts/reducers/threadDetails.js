import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'
import moment from 'moment';

export function threadDetails(state, action) {

  function defaultThreadDetails() {
    return {
      content: '',
      user: '',
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      showCreatingThread: false,
      creatingThread: null,
      activeThreads: {},
      showHierarchy: {},
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_THREAD:
      var data = defaultThreadDetails();
      data[action.discussionType + 'Id'] = action.discussionId;
      return state.merge({'creatingThread': data, showCreatingThread: true});
    case ActionTypes.PAUSE_CREATING_THREAD:
      return state.set('showCreatingThread', false);
    case ActionTypes.CANCEL_CREATING_THREAD:
      return state.merge({'creatingThread': null, showCreatingThread: false});
    case ActionTypes.CREATE_THREAD_SUCCESS:
      return state.merge({'creatingThread': null, showCreatingThread: false});
    case ActionTypes.EDIT_CREATING_THREAD:
      return state.setIn(['creatingThread', action.attr], action.value)
    case ActionTypes.SELECT_THREAD_ITEM:
      return state.setIn(['activeThreads', action.threadId.toString()], Immutable.fromJS({id: action.threadId, color: action.colorIndex}));
    case ActionTypes.DESELECT_THREAD_ITEM:
      return state.deleteIn(['activeThreads', action.threadId.toString()]);
    case ActionTypes.SHOW_HIERARCHY:
      return state.setIn(['showHierarchy', action.threadId.toString()], Immutable.fromJS({thread: action.threadId, post: action.postId}));
    case ActionTypes.HIDE_HIERARCHY:
      return state.deleteIn(['showHierarchy', action.threadId.toString()]);
    case ActionTypes.CREATE_POST_SUCCESS:
      var showHierarchyThread = state.getIn(['showHierarchy', action.response.rawData.thread.toString()]);
      if (showHierarchyThread) {
        return state.setIn(['showHierarchy', action.response.rawData.thread.toString()], Immutable.fromJS({thread: action.response.rawData.thread, post: action.response.rawData.id}));
      } else {
        return state;
      }
    default:
      return state;
  }
}