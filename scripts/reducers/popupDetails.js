import * as ActionTypes from '../actions';
import Immutable from 'immutable';

var searchCount = 10;

export function popupDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      forms: {
        feedback: false
      }
    });
  }

  switch(action.type) {
    case ActionTypes.SHOW_POPUP_FORM:
      return state.setIn(['forms', action.form], true);
    case ActionTypes.HIDE_POPUP_FORM:
      return state.setIn(['forms', action.form], false);
    default:
      return state;
  }
}