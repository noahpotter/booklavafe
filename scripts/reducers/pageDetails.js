import * as ActionTypes from '../actions';
import Immutable from 'immutable';

var searchCount = 10;

export function pageDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      page: {
        search: 1
      },
      hasMorePages: {
        search: false
      }
    });
  }

  switch(action.type) {
    case ActionTypes.SET_PAGE:
      return state.setIn(['page', action.pageType], parseInt(action.page));
    case ActionTypes.SEARCH_SUCCESS:
      if (action.response.rawData.length < searchCount) {
        return state.setIn(['hasMorePages', 'search'], false);
      } else {
        return state.setIn(['hasMorePages', 'search'], true);
      }
    // case ActionTypes.DEACTIVATE:
    //   return state.set('active', getDefaultActivateEntities());
    default:
      return state;
  }
}