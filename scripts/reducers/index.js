import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import { combineReducers } from 'redux';
import { searchDetails } from './searchDetails';
import { calendarDetails } from './calendarDetails';
import { loggedUserDetails } from './loggedUserDetails';
import { clubDetails } from './clubDetails';
import { addBookDetails } from './addBookDetails';
import { clubBookDetails } from './clubBookDetails';
import { pageDetails } from './pageDetails';
import { discussionDetails } from './discussionDetails';
import { interactionDetails } from './interactionDetails';
import { popupDetails } from './popupDetails';
import { feedbackDetails } from './feedbackDetails';
import { statusDetails } from './statusDetails';
import { userDetails } from './userDetails';
import { userDetailDetails } from './userDetailDetails';
import { threadDetails } from './threadDetails';
import { notificationDetails } from './notificationDetails';
import { postDetails } from './postDetails';
import { Schemas } from '../middleware/api';

import store from 'store'

function isDeleteSuccess(action) {
  return (
    action.configuration && 
    action.configuration.method.toLowerCase() == 'delete' && 
    action.type.indexOf("_SUCCESS") != -1);
}

// Updates an entity cache in response to any action with response.entities.
function entities(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({ users: {}, userDetails: {}, threads: {}, posts: {}, books: {}, clubs: {}, clubBooks: {}, clubBookDiscussions: {}, clubDiscussions: {} });
  }

  if (action.response && action.response.entities) {
    // state = state.mergeDeep(action.response.entities);
    var oldEntities = state.toJS();
    var newEntities = action.response.entities;

    for(var entityType in newEntities) {
      for(var key in newEntities[entityType]) {
        oldEntities[entityType][key] = newEntities[entityType][key];
      }
    }

    state = Immutable.fromJS(oldEntities);
  }

  if (isDeleteSuccess(action) && action.configuration.data && action.configuration.data.type != null && !action.configuration.schema) {
    state = state.deleteIn([action.configuration.data.type, action.configuration.data.id.toString()]);
  }

  switch(action.type) {
    case ActionTypes.CREATE_CLUB_SUCCESS:
      var ownedClubs = state.getIn(['users', action.previousState.loggedUserDetails.get('id').toString(), 'ownedClubs'])
      ownedClubs = ownedClubs.push(Object.keys(action.response.entities.clubs)[0])
      state = state.setIn(['users', action.previousState.loggedUserDetails.get('id').toString(), 'ownedClubs'], ownedClubs) 
      return state;
    case ActionTypes.JOIN_CLUB_SUCCESS:
      var subscribedClubs = state.getIn(['users', action.previousState.loggedUserDetails.get('id').toString(), 'subscribedClubs'])
      subscribedClubs = subscribedClubs.push(Object.keys(action.response.entities.clubs)[0])
      state = state.setIn(['users', action.previousState.loggedUserDetails.get('id').toString(), 'subscribedClubs'], subscribedClubs) 
      return state;
  }

  return state;
}

function loginDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      username: '',
      email: store.get('email') || '',
      password: '',
      passwordConfirmation: ''
    });
  }

  switch(action.type) {
    case ActionTypes.UPDATE_LOGIN_DETAILS:
      if (action.attrs == 'email') {
        store.set('email', action.value);
      }
      return state.setIn(action.attrs, action.value);
    case ActionTypes.LOGIN_SUCCESS:
      return state.merge({password: '', passwordConfirmation: ''})
    default:
      return state;
  }
}

// Updates error message to notify about the failed fetches.
function errorMessage(state = null, action) {
  const { type, error } = action;

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null;
  } else if (error) {
    return action.error;
  }

  return state;
}

const rootReducer = combineReducers({
  entities,
  discussionDetails,
  loginDetails,
  searchDetails,
  calendarDetails,
  loggedUserDetails,
  clubDetails,
  clubBookDetails,
  addBookDetails,
  userDetails,
  interactionDetails,
  popupDetails,
  feedbackDetails,
  userDetailDetails,
  threadDetails,
  notificationDetails,
  postDetails,
  pageDetails,
  statusDetails,
});

export const reducers = {
  entities,
  discussionDetails,
  loginDetails,
  searchDetails,
  calendarDetails,
  loggedUserDetails,
  clubDetails,
  clubBookDetails,
  addBookDetails,
  userDetails,
  interactionDetails,
  popupDetails,
  feedbackDetails,
  userDetailDetails,
  threadDetails,
  notificationDetails,
  postDetails,
  pageDetails,
  statusDetails,
}

export default rootReducer;