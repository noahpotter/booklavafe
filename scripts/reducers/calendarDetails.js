import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import moment from 'moment'

export function calendarDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      currentMonth: moment().month(),
    });
  }

  switch(action.type) {
    case ActionTypes.CHANGE_CURRENT_MONTH:
      var nextMonth = state.get('currentMonth') + action.change
      if (nextMonth >= 12) {
        nextMonth = 0;
      } else if (nextMonth <= -1) {
        nextMonth = 11;
      }
      return state.merge({'currentMonth': nextMonth});
    default:
      return state;
  }
}