import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'

export function userDetails(state, action) {
  function defaultUserDetails() {
    return {
      username: '',
      email: ''
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      creatingUser: null,
      editingUser: null
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_USER:
      return state.merge({creatingUser: defaultUserDetails(), editingUser: null})
    case ActionTypes.CANCEL_CREATING_USER:
      return state.merge({creatingUser: null})
    case ActionTypes.EDIT_CREATING_USER:
      return state.setIn(['creatingUser', action.attr], action.value)
    case ActionTypes.START_EDITING_USER:
      return state.merge({editingUser: action.user, creatingUser: null})
    case ActionTypes.CANCEL_EDITING_USER:
      return state.merge({editingUser: null})
    case ActionTypes.EDIT_EDITING_USER:
      return state.setIn(['editingUser', action.attr], action.value)
    default:
      return state;
  }
}