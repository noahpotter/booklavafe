import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'
import moment from 'moment';

export function clubBookDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      editingClubBook: null
    });
  }


  switch(action.type) {
    case ActionTypes.START_EDITING_CLUB_BOOK:
      var editingClubBook = action.clubBook;
      editingClubBook = editingClubBook.set('startDate', moment(editingClubBook.get('startDate')).utc().format('M/D/YYYY'))
      editingClubBook = editingClubBook.set('endDate', moment(editingClubBook.get('endDate')).utc().format('M/D/YYYY'))

      return state.merge({editingClubBook})
    case ActionTypes.CANCEL_EDITING_CLUB_BOOK:
      return state.merge({editingClubBook: null})
    case ActionTypes.EDIT_EDITING_CLUB_BOOK:
      return state.setIn(['editingClubBook', action.attr], action.value)
    case ActionTypes.EDIT_CLUB_BOOK_SUCCESS:
      return state.merge({editingClubBook: null})
    default:
      return state;
  }
}