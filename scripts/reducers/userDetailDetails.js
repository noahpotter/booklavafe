import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'

export function userDetailDetails(state, action) {
  function defaultUserDetailDetails() {
    return {
      image: null
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      creatingUserDetail: null,
      editingUserDetail: null
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_USER_DETAIL:
      return state.merge({creatingUserDetail: defaultUserDetailDetails(), editingUserDetail: null})
    case ActionTypes.CANCEL_CREATING_USER_DETAIL:
      return state.merge({creatingUserDetail: null})
    case ActionTypes.EDIT_CREATING_USER_DETAIL:
      return state.setIn(['creatingUserDetail', action.attr], action.value)
    case ActionTypes.START_EDITING_USER_DETAIL:
      return state.merge({editingUserDetail: action.userDetail, creatingUserDetail: null})
    case ActionTypes.CANCEL_EDITING_USER_DETAIL:
      return state.merge({editingUserDetail: null})
    case ActionTypes.EDIT_EDITING_USER_DETAIL:
      return state.setIn(['editingUserDetail', action.attr], action.value)
    default:
      return state;
  }
}