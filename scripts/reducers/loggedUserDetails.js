import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'
import store from 'store'

export function loggedUserDetails(state, action) {
  if (typeof state == 'undefined') {
    var apiKey = store.get('apiKey');

    if (!apiKey) {
      store.set('userId', null);
    }

    var id = parseInt(store.get('userId'));
    if (isNaN(id)) {
      id = null;
    }

    state = Immutable.fromJS({
      id,
      apiKey
    });
  }

  switch(action.type) {
    case ActionTypes.LOGIN_SUCCESS:
      store.set('userId', action.response.id)
      store.set('apiKey', action.response.token)
      return state.merge(Immutable.fromJS({'apiKey': action.response.token, 'id': action.response.id}));
    case ActionTypes.FETCH_USER_FAILURE:
      if (action.error.detail == 'Invalid token.') {
        store.set('userId', null)
        store.set('apiKey', '')
        return state.merge(Immutable.fromJS({'apiKey': '', 'id': null}));
      }
      return state;
    case ActionTypes.LOGOUT_SUCCESS:
      store.set('userId', null)
      store.set('apiKey', '')
      return state.merge(Immutable.fromJS({'apiKey': '', 'id': null}));
    default:
      return state;
  }
}