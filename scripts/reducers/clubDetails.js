import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'

export function clubDetails(state, action) {
  function defaultClubDetails() {
    return {
      name: '',
      description: '',
      useClubImage: false,
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      creatingClub: null,
      editingClub: null
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_CLUB:
      return state.merge({creatingClub: defaultClubDetails(), editingClub: null})
    case ActionTypes.CANCEL_CREATING_CLUB:
      return state.merge({creatingClub: null})
    case ActionTypes.EDIT_CREATING_CLUB:
      return state.setIn(['creatingClub', action.attr], action.value)
    case ActionTypes.START_EDITING_CLUB:
      return state.merge({editingClub: action.club, creatingClub: null})
    case ActionTypes.CANCEL_EDITING_CLUB:
      return state.merge({editingClub: null})
    case ActionTypes.EDIT_EDITING_CLUB:
      return state.setIn(['editingClub', action.attr], action.value)
    case ActionTypes.CREATE_CLUB_SUCCESS:
    case ActionTypes.EDIT_CLUB_SUCCESS:
      return state.merge({'creatingClub': null, 'editingClub': null});
    default:
      return state;
  }
}