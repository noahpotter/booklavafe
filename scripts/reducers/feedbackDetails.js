import * as ActionTypes from '../actions';
import Immutable from 'immutable';

export function feedbackDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      message: ''
    });
  }

  switch(action.type) {
    case ActionTypes.EDIT_FEEDBACK:
      return state.set('message', action.message);
    case ActionTypes.SUBMIT_FEEDBACK:
      return state.set('message', '');
    default:
      return state;
  }
}