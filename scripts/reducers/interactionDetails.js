import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'

export function interactionDetails(state, action) {
  function getDefaultActivateEntities() {
    return {
      'user': null,
      'club': null,
      'clubBook': null,
      'clubBookDiscussion': null,
      'clubDiscussion': null,
      'thread': null,
      'post': null,
      'book': null
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      active: getDefaultActivateEntities()
    });
  }

  switch(action.type) {
    case ActionTypes.ACTIVATE:
      return state.merge({'active': action.entities});
    case ActionTypes.DEACTIVATE:
      if (action.entities) {
        for (var key in action.entities) {
          state = state.setIn(['active', key], null);
        }
        return state;
      } else {
        return state.set('active', getDefaultActivateEntities());
      }
    case ActionTypes.CREATE_POST_SUCCESS:
      var postId = state.getIn(['active', 'post']);
      if (postId == action.response.rawData.parentPost) {
        return state.setIn(['active', 'post'], action.response.rawData.id);
      } else {
        return state;
      }
    default:
      return state;
  }
}