import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import localStorage from 'localStorage'
import moment from 'moment';

export function discussionDetails(state, action) {

  function defaultClubBookDiscussion() {
    return {
      startDate: moment().format('M/D/YYYY'),
      startTime: moment().format('h:mm A'),
      startChapter: 1,
      endChapter: 2
    }
  }

  function defaultClubDiscussion() {
    return {
      date: moment().format('M/D/YYYY'),
    }
  }

  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      creatingClubBookDiscussion: null,
      creatingClubDiscussion: null,
      colors: ['#23BCFF', '#A1E3FF', '#6BA6BF', '#017BB1', '#29708F'],
      detailsOpen: false
    });
  }

  switch(action.type) {
    case ActionTypes.START_CREATING_CLUB_BOOK_DISCUSSION:
      var clubBookDiscussion = defaultClubBookDiscussion();
      clubBookDiscussion.clubBook = action.clubBookId;

      // Find farthest chapter so far
      var farthestEndChapter = 0;
      action.discussions.forEach(discussion => {
        farthestEndChapter = Math.max(farthestEndChapter, parseInt(discussion.get('endChapter')));
      });
      clubBookDiscussion.startChapter = farthestEndChapter + 1;
      clubBookDiscussion.endChapter = farthestEndChapter + 2;

      return state.merge({creatingClubBookDiscussion: clubBookDiscussion})
    case ActionTypes.CREATE_CLUB_BOOK_DISCUSSION_SUCCESS:
      return state.merge({creatingClubBookDiscussion: null});
    case ActionTypes.CANCEL_CREATING_CLUB_BOOK_DISCUSSION:
      return state.merge({creatingClubBookDiscussion: null})
    case ActionTypes.EDIT_CREATING_CLUB_BOOK_DISCUSSION:
      return state.setIn(['creatingClubBookDiscussion', action.attr], action.value)
    case ActionTypes.START_CREATING_CLUB_DISCUSSION:
      return state.merge({creatingClubDiscussion: defaultClubDiscussion()})
    case ActionTypes.CANCEL_CREATING_CLUB_DISCUSSION:
      return state.merge({creatingClubDiscussion: null})
    case ActionTypes.EDIT_CREATING_CLUB_DISCUSSION:
      return state.setIn(['creatingClubDiscussion', action.attr], action.value)
    case ActionTypes.TOGGLE_DISCUSSION_DETAILS:
      return state.set('detailsOpen', !state.get('detailsOpen'));
    default:
      return state;
  }
}