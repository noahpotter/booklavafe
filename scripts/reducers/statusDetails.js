import * as ActionTypes from '../actions';
import Immutable from 'immutable';

var searchCount = 10;

export function statusDetails(state, action) {
  function defaultState() {
    return Immutable.fromJS({
      errors: {},
      progress: {}
    });
  }

  if (typeof state == 'undefined') {
    state = defaultState();
  }

  var isFailedAPIResponse = action.type.indexOf('_FAILURE') != -1;
  var isSuccessAPIResponse = action.type.indexOf('_SUCCESS') != -1;
  var isAPIResponse = isFailedAPIResponse || isSuccessAPIResponse;

  if (isAPIResponse) {
    var meta = Immutable.fromJS(action.configuration.meta);
    var apiResponseAffectsStatus = true;
    if (meta && meta.get('apiResponseAffectsStatus') === false) {
      apiResponseAffectsStatus = false;
    }

    if (apiResponseAffectsStatus) {
      if (isFailedAPIResponse) {
        // state = defaultState();
        console.log('got failure', action);
        var parentError;
        if (meta) {
          parentError = meta.get('parentError');
        }

        if (parentError) {
          for(var attr in action.error) {
            var error = action.error[attr];
            if (!(error instanceof Array)) {
              error = [error];
            }

            if (!action.error[parentError]) {
              action.error[parentError] = {};
            }

            action.error[parentError][attr] = error;
          }
        } else {
          for(var attr in action.error) {
            if (!(action.error[attr] instanceof Array)) {
              action.error[attr] = [action.error[attr]];
            }
          }
        }
        state = state.mergeDeep({'errors': action.error});
      }

      state = state.set('progress', {});
    }
  }


  switch(action.type) {
    case ActionTypes.RESET_STATUS:
      state = defaultState();
      return state
    case ActionTypes.SET_ERRORS:
      for(var attr in action.errors) {
        if (!(action.errors[attr] instanceof Array)) {
          action.errors[attr] = [action.errors[attr]];
        }
      }
      state = state.mergeDeep({'errors': action.errors});
      return state;
    case ActionTypes.RESET_ERROR_STATUS:
      state = state.set('errors', {});
      return state
    case ActionTypes.RESET_PROGRESS_STATUS:
      state = state.set('progress', {});
      return state
    case ActionTypes.SET_ERROR_STATUS:
      state = state.remove('errors');
      var errors = {errors: {}};
      errors['errors'][action.attr] = [action.message];
      state = state.mergeDeep(errors);
      return state;
    case ActionTypes.SET_PROGRESS_STATUS:
      state = state.remove('progress');
      var progress = {progress: {}};
      progress['progress'][action.attr] = action.message;
      state = state.mergeDeep(progress);
      return state;
  }

  return state;
}