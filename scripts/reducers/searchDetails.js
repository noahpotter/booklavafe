import * as ActionTypes from '../actions';
import Immutable from 'immutable';

export function searchDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS({
      query: '',
      type: '',
      selectedId: null,
      books: [],
      clubs: [],
      users: [],
      isSearching: false,
      hasMorePages: true,
      lastSearch: {
        query: null,
        type: null,
        page: null
      }
    });
  }

  switch(action.type) {
    case ActionTypes.SEARCH:
      state = state.set('isSearching', true);
      state = state.mergeDeep({lastSearch: {query: action.initialActionData.query, type: action.initialActionData.type, page: action.initialActionData.page}, selectedId: '0'});
      return state;
    case ActionTypes.SEARCH_FAILURE:
      state = state.set('isSearching', false);
      return state;
    case ActionTypes.SEARCH_SUCCESS:
      state = state.set('isSearching', false);
      if (action.previousState) {
        const type = action.previousState.searchDetails.get('type');
        if (action.response.entities) {
          if (type == 'clubs') {
            state = state.set('clubs', Immutable.fromJS(Object.keys(action.response.entities.clubs || {})).map(club => parseInt(club)))
          }

          if (type == 'users') {
            state = state.set('users', Immutable.fromJS(Object.keys(action.response.entities.users || {})).map(user => parseInt(user)))
          }

          if (type == 'books') {
            state = state.set('books', Immutable.fromJS(Object.keys(action.response.entities.books || {})).map(book => parseInt(book)))
          }

          state = state.set('selectedId', null);
        }
      }
      
      return state;
    case ActionTypes.CHANGE_SEARCH_TYPE:
      var query = state.get('query');

      if (action.searchType == 'books' && query) {
        if (query.indexOf('book: ') == 0) {
          query = query.slice(6);
        }
      }

      return state.merge({'type': action.searchType, query});
    case ActionTypes.EDIT_SEARCH_QUERY:
      return state.set('query', action.query);
    case ActionTypes.SELECT_SEARCH_RESULT:
      return state.set('selectedId', action.id);
    default:
      return state;
  }
}