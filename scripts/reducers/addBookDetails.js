import * as ActionTypes from '../actions';
import Immutable from 'immutable';
import moment from 'moment';
import { momentFormatDate } from '../utils';

function defaultAddBookDetails() {
  return {
    showAddBookToClub: false,
    bookId: null, // Possibly not necessary, just look at selected search book
    clubId: null,
    startDate: momentFormatDate(moment()),
    endDate: momentFormatDate(moment().add(2, 'months')),
  }
}

export function addBookDetails(state, action) {
  if (typeof state == 'undefined') {
    state = Immutable.fromJS(defaultAddBookDetails());
  }

  switch(action.type) {
    case ActionTypes.SHOW_ADD_BOOK_TO_CLUB:
      return state.merge({showAddBookToClub: true});
    case ActionTypes.CANCEL_ADD_BOOK_TO_CLUB:
      return state.merge(defaultAddBookDetails());
    case ActionTypes.SELECT_BOOK:
      return state.set('bookId', action.bookId)
    case ActionTypes.SELECT_CLUB:
      return state.set('clubId', action.clubId)
    case ActionTypes.UPDATE_END_DATE:    
      return state.set('endDate', action.endDate)
    case ActionTypes.UPDATE_START_DATE:    
      return state.set('startDate', action.startDate)
    default:
      return state;
  }
}